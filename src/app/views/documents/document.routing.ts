import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArrivedFolderComponent} from './arrived/folder/arrivedfolder.component'
import { ArrivedOfficeAgentComponent } from './arrived/office-agent/officeagent.component';
import { CreateOrEditArrivedDocumentComponent } from './arrived/office-agent/create-or-edit-arrived-document/createoreditarriveddocument.component';
import { ProcessArrivedDocumentComponent } from './arrived/process-document/processdocumentlist.component';
import { AssignDocumentUserComponent } from './arrived/assign-document-user/assign-document-user/assign-document-user.component';
import { ReleaseDocumentListComponent } from './departure/released/release-document-list/release-document-list.component';
import { CreateDepartureDocumentComponent } from './departure/create-departure-document.component';
import { UnresolvedDepartureDocument } from './departure/unresolved-departure-document/unresolved-departure-document.component';
import { ResolvedDepartureDocument } from './departure/resolved-departure-document/resolved-departure-document.component';
import { AssignDepartureDocumentUserComponent } from './departure/assign-departure-document-user/assign-departure-document-user.component';
import { ArrivedDocumentsHistoryComponent } from './arrived/arrived-documents-history/arrived-documents-history.component';
import { DepartureDocumentsHistoryComponent } from './departure/departure-documents-history/departure-documents-history.component';
import { EditDepartureDocumentComponent } from './departure/editdeparturedocument/edit-departure-document.component';

export const routes: Routes = [
    {
        path: 'arrived',
        children: [
            {
                path: 'folder',
                component: ArrivedFolderComponent,
                data: {
                    title: 'Danh mục sổ văn bản đến'
                }
            },
            {
                path: 'officeagent',
                component: ArrivedOfficeAgentComponent,
                data: {
                    title: 'Chuyển giao văn bản đến'
                }
            },
            {
                path: 'processdocument',
                component: ProcessArrivedDocumentComponent,
                data: {
                    title: 'Văn bản được chuyển tiếp đến'
                }
            },
            {
                path: 'createarriveddocument',
                component: CreateOrEditArrivedDocumentComponent,
                data: {
                    title: 'Tạo mới văn bản đến'
                }
            },
            {
                path: 'editarriveddocument',
                component: CreateOrEditArrivedDocumentComponent,
                data: {
                    title: 'Sửa thông tin văn bản đến'
                }
            },
            {
                path: 'assignuser/:id',
                component: AssignDocumentUserComponent,
                data: {
                    title: 'Gán người xử lý'
                }
            },
           {
               path: 'history',
               component: ArrivedDocumentsHistoryComponent,
               data: {
                   title: 'Lịch sử xử lý'
               }
           }
        ]
    },
    // {
    //     path:'released',
    //     children:[
    //         {
    //             path: 'releasedocumentlist',
    //             component: ReleaseDocumentListComponent,
    //             data: {
    //                 title: 'Văn bản phát hành'
    //             }
    //         }
    //     ] 
    // },
    {
        path: 'departure',
        children: [
            {
                path: 'createdeparturedocument',
                component: CreateDepartureDocumentComponent,
                data: {
                    title: 'Tạo mới văn bản đi'
                }
            },
            {
                path: 'unresolveddeparturedocument',
                component: UnresolvedDepartureDocument,
                data: {
                    title: 'Danh sách văn bản đi chờ xử lý'
                }
            },
            {
                path: 'resolveddeparturedocument',
                component: ResolvedDepartureDocument,
                data: {
                    title: 'Danh sách văn bản đi đã xử lý'
                }
            },
            {
                path: 'releasedocumentlist',
                component: ReleaseDocumentListComponent,
                data: {
                    title: 'Văn bản phát hành'
                }
            },
            {
                path: 'assignUser/:id',
                component: AssignDepartureDocumentUserComponent,
                data: {
                    title: 'Gán cán bộ chỉnh sửa'
                }
            },
            {
                path: 'history',
                component: DepartureDocumentsHistoryComponent,
                data: {
                    title: 'Lịch sử xử lý'
                }
            },
            {
                path: 'editdeparturedocument/:id',
                component: EditDepartureDocumentComponent,
                data: {
                    title: 'Chỉnh sửa văn bản đi'
                }
            }
        ]
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
  })
  export class DocumentRouting {}