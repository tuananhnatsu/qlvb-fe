import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { PagedListingComponentBase } from "@shared/paged-listing-component-base";
import { ArrivedDocumentDto } from "@app/models/documents/arrived/arriveddocument.model";
import { MatPaginator } from "@angular/material/paginator";
import { PagedRequestDto } from "../../../../shared/paged-listing-component-base";
import {
  PagedProcessDocumentRequestDto,
  ProcessDocumentDtoPagedResultDto,
  ProcessDocumentView,
} from "@app/models/documents/arrived/arriveddocument.model";
import { MatDialog } from "@angular/material/dialog";
import { finalize } from "rxjs/operators";
import { LookupTableService } from "@app/services/lookuptable.service";
import { LookupTableDto } from "@app/models/system-management/lookuptable.model";
import * as _ from "lodash";
import { ProcessDocumentService } from "@app/services/documents/arrived/process-document/process-document.service";
import { ViewDetailDocumentComponent } from "./view-detail-document/viewdetaildocument.component";
import { Router } from '@angular/router';

@Component({
  templateUrl: "./processdocumentlist.component.html",
  animations: [appModuleAnimation()]
})
export class ProcessArrivedDocumentComponent extends PagedListingComponentBase<
  ArrivedDocumentDto
> {
  @ViewChild("paginator") paginator: MatPaginator;
  level: number = 0;
  role: number = 0;
  totalDocument: number = 0;
  totalUnHandle: number = 0;
  totalProcessing: number = 0;
  totalHandlded: number = 0;
  items: any[] = [
    { label: "--Tất cả--", value: null },
    { label: "Đến hạn", value: 0 },
    { label: "Quá hạn", value: 1 },
    { label: "Không có hạn", value: 2 },
  ];
  years: any[] = [];
  rolesHandling: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  urgentLevel: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  documentFields: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  documentTypes: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  isDisableChooseYear: boolean = false;
  isDisableChooseDate: boolean = false;
  isDisableChooseFromDate: boolean = false;
  isDisableChooseToDate: boolean = false;

  private service: ProcessDocumentService;
  private lookupTableAppService: LookupTableService;
  input = new PagedProcessDocumentRequestDto();
  private _dialog: MatDialog;

  isAdvancedSearch: boolean = false;
  processDocumentView: ProcessDocumentView[];
  urgentLevels: Map<number, LookupTableDto> = new Map();
  roles: Map<number, LookupTableDto> = new Map();

  columns: Array<any> = [
    { field: "allowRoleName", header: this.l("VT") },
    { field: "urgentLevelName", header: this.l("Cấp độ") },
    { field: "documentNumber", header: this.l("Số ký hiệu") },
    { field: "shortDescription", header: this.l("Trích yếu") },
    { field: "deadLineDate", header: this.l("Hạn xử lý") },
    { field: "arrivedDate", header: this.l("Thời gian nhận văn bản") },
    { field: "publishedOrganization", header: this.l("Cơ quan ban hành") },
  ];

  constructor(
    injector: Injector,
    service: ProcessDocumentService,
    dialog: MatDialog,
    lookupTableAppService: LookupTableService,
    private router: Router,
  ) {
    super(injector);
    this.service = service;
    this._dialog = dialog;
    this.lookupTableAppService = lookupTableAppService;
  }

  ngOnInit() {
    super.ngOnInit();
    this.initialize();
  }

  private initialize(): void {
    this.initUrgentLevelsMap();
    this.getMasterData();
    this.initAllowRoleIdsMap();
  }

  protected list(
    request: PagedProcessDocumentRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    let filterRequest: PagedProcessDocumentRequestDto = {...this.input, skipCount: request.skipCount, maxResultCount: request.maxResultCount};
    this.processDocumentView = [];

    this.service
      .filterProcessDocuments(filterRequest)
      .pipe(finalize(() => finishedCallback()))
      .subscribe(
        (processDocumentPagedResult: ProcessDocumentDtoPagedResultDto) => {
          this.showPaging(processDocumentPagedResult, pageNumber);
          if (processDocumentPagedResult.totalCount === 0) {
            return;
          }
          this.totalDocument = processDocumentPagedResult.totalCount;
          this.totalUnHandle = processDocumentPagedResult.totalUnHandle;
          this.totalProcessing = processDocumentPagedResult.totalProcessing;
          this.totalHandlded = processDocumentPagedResult.totalHandlded;
          for (let processDocument of processDocumentPagedResult.items) {
            let view: ProcessDocumentView = new ProcessDocumentView(
              processDocument
            );
            view.urgentLevelName = this.getUrgentLevelName(
              processDocument.urgentLevel
            );
            view.allowRoleName = this.getAllowRoleName(
              processDocument.allowRoleId
            );
            this.processDocumentView.push(view);
          }
        }
      );
  }

  private initAllowRoleIdsMap(): void {
    this.lookupTableAppService
      .getAllRoleHandlingForLookupTable()
      .subscribe((roles) => {
        for (let role of roles) {
          this.roles.set(role.id, role);
        }
      });
  }

  getAllowRoleName(allowRoleId: number): string {
    if (
      allowRoleId === null ||
      allowRoleId === undefined ||
      this.roles === undefined ||
      this.roles.size === 0
    ) {
      return "Không xác định";
    }
    let role: LookupTableDto = this.roles.get(allowRoleId);
    if (
      role === undefined ||
      role === null ||
      _.isEmpty(role.displayName)
    ) {
      return "Không xác định";
    }

    return role.displayName;
  }

  refreshFilter() {
    this.input = new PagedProcessDocumentRequestDto();
  }

  private initUrgentLevelsMap(): void {
    this.lookupTableAppService
      .getAllUrgentLevelForLookupTable()
      .subscribe((urgentLevels) => {
        for (let urgentLevel of urgentLevels) {
          this.urgentLevels.set(urgentLevel.id, urgentLevel);
        }
      });
  }

  private getUrgentLevelName(urgentLevelId?: number): string {
    if (
      urgentLevelId === null ||
      urgentLevelId === undefined ||
      this.urgentLevels === undefined ||
      this.urgentLevels.size === 0
    ) {
      return "Không xác định";
    }
    let urgentLevel: LookupTableDto = this.urgentLevels.get(urgentLevelId);
    if (
      urgentLevel === undefined ||
      urgentLevel === null ||
      _.isEmpty(urgentLevel.displayName)
    ) {
      return "Không xác định";
    }

    return urgentLevel.displayName;
  }

  toggleAndvancedSearch() {
    this.isAdvancedSearch = !this.isAdvancedSearch;
  }

  private getMasterData() {
    let currentYear = new Date().getFullYear();
    for (var y = -5; y < 5; y++) {
      this.years.push({ label: currentYear + y, value: currentYear + y });
    }

    this.lookupTableAppService
      .getAllRoleHandlingForLookupTable()
      .subscribe((rs) => {
        _.forEach(rs, (item) => {
          this.rolesHandling.push({ label: item.displayName, value: item.id });
        });
      });

    this.lookupTableAppService
      .getAllUrgentLevelForLookupTable()
      .subscribe((rs) => {
        _.forEach(rs, (item) => {
          this.urgentLevel.push({ label: item.displayName, value: item.id });
        });
      });

    this.lookupTableAppService
      .getAllCategoryTypeForLookupTable()
      .subscribe((rs) => {
        _.forEach(rs, (item) => {
          if (item.id == 2) {
            this.documentTypes.push({
              label: item.displayName,
              value: item.id,
            });
          } else if (item.id == 1) {
            this.documentFields.push({
              label: item.displayName,
              value: item.id,
            });
          }
        });
      });
  }

  viewDetail(item): void {
    this.showDetailDocument(item.id);
  }

  checkYearIsChoose() {
    if(this.input.years.length > 0) {
      this.isDisableChooseDate = true;
    } else {
      this.isDisableChooseDate = false;
    }
  }

  checkDateIsChoose() {
    if (this.input.endDate != null || this.input.startDate != null) {
      this.isDisableChooseYear = true;
    } else {
      this.isDisableChooseYear = false;
    }
  }
  
  checkExpiredDateFromIsChoose() {
    if (this.input.expiredDateFrom != null) {
      this.isDisableChooseToDate = true;
    } else {
      this.isDisableChooseToDate = false;
    }
  }

  checkExpiredDateToIsChoose() {
    if (this.input.expiredDateTo != null) {
      this.isDisableChooseFromDate = true;
    } else {
      this.isDisableChooseFromDate = false;
    }
  }

  showDetailDocument(id?: number): void {
    this._dialog.open(ViewDetailDocumentComponent, {
      width: "1200px",
      maxWidth: "1400px",
      data: {
        allowUpload: true,
        allowAction: true,
        id: id
      }
    }).afterClosed().subscribe((result) => {
      if (result) {
        this.refresh();
      }
    });
  }
  search(value) {
    if (value === "isTotal"){
      this.input.status = null;
      this.refresh();
    }
    if (value === "isUnHandle"){
      this.input.status = 1;
      this.refresh();
    }
    if (value === "isProcessing"){
      this.input.status = 2;
      this.refresh();
    }
    if (value === "isHandled"){
      this.input.status = 3;
      this.refresh();
    }
  }

  forwardDocument(id) {
    this.router.navigate(['/document/arrived/assignuser/' + id]);
  }

  protected delete() {}

  viewDocumentHistory(id: number) {
    this.router.navigate(["/document/arrived/history"], { queryParams: { id: id } })
  }
}
