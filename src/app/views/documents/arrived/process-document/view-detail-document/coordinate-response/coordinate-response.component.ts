import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { ProcessDocumentService } from '@app/services/documents/arrived/process-document/process-document.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FileService } from '@app/services/file.service';
import { finalize } from 'rxjs/operators';
import { CordinateDocumentsDto, ArrivedDocumentDto } from '../../../../../../models/documents/arrived/arriveddocument.model';
import { UserDto } from '../../../../../../models/system-management/user.model';
import { FileDto } from '../../../../../../models/documents/file/file.model';

@Component({
    selector: 'app-coordinate-response',
    templateUrl: './coordinate-response.component.html'
})
export class CoordinateResponseComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    relatedFiles: File[] = [];
    form: CordinateDocumentsDto = new CordinateDocumentsDto();
    isLoading: boolean

    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) private _id: number,
        private _service: ProcessDocumentService,
        private _dialogRef: MatDialogRef<CoordinateResponseComponent>,
        private fileService: FileService,
    ) {
        super(injector);
    }
    ngOnInit() {
        this.getCodinateDocumentById();
    }

    columns: Array<any> = [
        { field: "name", header: this.l("Văn bản") }
      ];

    close(result: any): void {
      this._dialogRef.close(result);
    }
    chooseFiles(event) {
        const fileList: FileList = event.target.files;
        for (let i=0; i<fileList.length; i++){
            // Kiem tra neu trong tep file da them da co file thi khong them vao nua
            // if (this.relatedFiles.every(e => e.name !== fileList.item(i).name) && 
            //     this.form.attachedFiles.every(e => e.fileName !== fileList.item(i).name)) 
                this.relatedFiles.push(fileList.item(i)); 
        }
    }
    
    focusChooseFiles() {
        this.chooseFile.nativeElement.click();
    }

    download(fileUrl: string) {
        this.fileService.download(fileUrl).pipe(finalize(() => {})).subscribe(response => {
        })
    }

    // uploadFile() {
    //     this.fileService.upload(this.relatedFiles, "arriveddocument").subscribe(response => {
    //       this.formArrivedDocument.files = response.items;
    //       this._service.update(this.formArrivedDocument).subscribe(rs => {
    //           this.getCodinateDocumentById();
    //     })
    //   })
    // }
   
    getCodinateDocumentById(){
        this.isLoading = true
        this._service.getCodinateDocumentById(this._id)
        .subscribe((result: CordinateDocumentsDto) => {
          this.form = result;
        }, () => {}, () => {this.isLoading = false});
    }

    save(){
        this.fileService.upload(this.relatedFiles, "arriveddocument")
        .pipe(
            finalize(() => {
                this.form.arrivedDocumentId = this._id;
                this._service.updateDocument(this.form)
                .subscribe((result: CordinateDocumentsDto) => {
                    this.close(true);
                    this.getCodinateDocumentById();
                });
            })
        )
        .subscribe(response => {
            response.items.forEach(item => {
                this.relatedFiles.forEach(i => {
                    if(item.fileName == i.name)
                    this.form.files.push(item)
                })
            });
        })
    }
}