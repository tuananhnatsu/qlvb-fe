import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { ArrivedDocumentDto } from '@app/models/documents/arrived/arriveddocument.model';
import { ProcessDocumentService } from '@app/services/documents/arrived/process-document/process-document.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '@app/services/file.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LookupTableService } from "@app/services/lookuptable.service";
import { LookupTableDto } from "@app/models/system-management/lookuptable.model";
import {ProcessDocumentView} from "@app/models/documents/arrived/arriveddocument.model";
import { MatDialog } from "@angular/material/dialog";

import { CompleteDocumentComponent } from './completedocument/completedocument.component';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { CoordinateResponseComponent } from './coordinate-response/coordinate-response.component';
import { ResponseModel } from '@app/models/response.model';
import { ViewFileDocumentComponent } from './view-file/viewfiledocument.component';

@Component({
    selector: 'app-viewdetaildocument',
    templateUrl: './viewdetaildocument.component.html'
})

export class ViewDetailDocumentComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    relatedFiles: File[] = [];
    form: ArrivedDocumentDto = new ArrivedDocumentDto();
    private lookupTableAppService: LookupTableService;
    urgentLevels: Map<number, LookupTableDto> = new Map();
    secretLevels: Map<number, LookupTableDto> = new Map();
    fileView: File[] = []
    processDocumentView: ProcessDocumentView[];
    private _dialog: MatDialog;
    data: any
    contentCordinate: ArrivedDocumentDto[] = [];

    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) data: number,
        private _service: ProcessDocumentService,
        private _serviceDocument: ArrivedDocumentService,
        private route: ActivatedRoute,
        private fileService: FileService,
        lookupTableAppService: LookupTableService,
        private _dialogRef: MatDialogRef<ViewDetailDocumentComponent>,
        dialog: MatDialog,
        private router: Router
    ) {
        super(injector);
        this.lookupTableAppService = lookupTableAppService;
        this._dialog = dialog;
        this.data = data;
    }
    ngOnInit() {
        this.getDocumentById();
    }

    columnsPd: any[] = [
      { field: "sendedBy", header: "Người gửi phối hợp phúc đáp" },
      { field: "sendDate", header: "Ngày gửi" },
      { field: "content", header: "Nội dung phối hợp phúc đáp" },
      { field: "filesCordinate", header: "File phối hợp phúc đáp" }
    ]

    columns: Array<any> = [
        { field: "fileName", header: this.l("Văn bản") }
      ];

    columnsBp: Array<any> = [
        { field: "handlingBy", header: this.l("Người chuyển") },
        { field: "handlingDate", header: this.l("Ngày chuyển") },
        { field: "content", header: this.l("Nội dung bút phê") }
      ];

    columnsReply: Array<any> = [
        { field: "personPd", header: this.l("Người gửi phối hợp phúc đáp") },
        { field: "dateSend", header: this.l("Ngày gửi") },
        { field: "contentPd", header: this.l("Nội dung phối hợp phúc đáp") },
        { field: "filePd", header: this.l("File phối hợp phúc đáp") }
      ];

      getDocumentById(){
        const id = this.data.id ? this.data.id : 0;
        if (id <= 0) return;
            if (id && id != null) {
                this._service.getDocumentById(id, undefined)
                .subscribe((result: ArrivedDocumentDto) => {
                  this.form.files = [];
                  this.form = result;
                });
            }
            else {
                // this.form = ArrivedDocumentDto.fromJS(true);
                // this.form.year = (new Date()).getFullYear();
            }
    }

    chooseFiles(event) {
        const fileList: FileList = event.target.files;
        let broken = false;
        for (let i=0; i<fileList.length; i++){
          // Kiem tra neu trong tep file da them da co file thi khong them vao nua
          if (this.relatedFiles.every(e => e.name !== fileList.item(i).name) && 
                this.form.files.every(e => e.fileName !== fileList.item(i).name))
            { 
              this.relatedFiles.push(fileList.item(i)); console.log(this.relatedFiles);
            }
            else {
              broken = true;
              break;
            }
          }
          if (!broken) {
            this.uploadFile(this.relatedFiles, "arriveddocument");
          }
    }
    
    focusChooseFiles() {
        this.chooseFile.nativeElement.click();
    }

    download(fileUrl: string) {
        this.fileService.download(fileUrl).pipe(finalize(() => {})).subscribe(response => {
        })
    }

    restrictDoc() {
      this.form.status = 4;
      this._service.update(this.form).subscribe(response => {
        this._messageService.add({ summary: "Thu hồi thành công!" })
      })
    }

    uploadFile(files , folderName) {
        this.fileService.upload(files, folderName ).subscribe(response => {
          this.form.attachedFiles = response.items;  
       this._serviceDocument.update(this.form).subscribe(rs => {
         //this.form.id = rs.id;            
          this.getDocumentById();     
          })         
      }, () => {}, () => {this.relatedFiles = []})
    }

    addCoordinateResponse(id: number): void {
      let openModal;
      openModal = this._dialog.open(CoordinateResponseComponent, {
        width: "700px",
        maxWidth: "700px",
        data: id
      });
  
      openModal.afterClosed().subscribe((result) => {
        if (result) {
          this.getDocumentById();
        }
      });
    }

    completeDocument(id: number): void {
      this._confirmService.confirm({
        header: "Xác nhận",
        message: "Hoàn tất xử lý văn bản",
        accept: () => {
          const _form = new ArrivedDocumentDto();
          _form.id = id;
          _form.status = 3;
          this._service.completeDocument(_form).pipe(finalize(() => {
            // this._messageService.add({
            //   severity: "success",
            //   summary: "Thành công",
            //   data: "Hoàn tất xử lý văn bản"
            // });
            
          })).subscribe(response => {
            console.log(response);
          }, () => {}, () => {
            this.close(true);
          })
        }
      })
      // let openModal;
      // openModal = this._dialog.open(CompleteDocumentComponent, {
      //   width: "500px",
      //   maxWidth: "500px",
      //   data: id,
      //   autoFocus: false
      // });
  
      // openModal.afterClosed().subscribe((result) => {
      //   if (result) {
      //     this.getDocumentById();
      //   }
      // });
    }

    viewFile(url: string): void {
      let openModal;
      openModal = this._dialog.open(ViewFileDocumentComponent, {
        width: "1000px",
        maxWidth: "800px",
        data: url
      });
  
      openModal.afterClosed().subscribe((result) => {
        if (result) {
        }
      });
    }

    close(result: any): void {
      this._dialogRef.close(result);
    }

    forwardDocument(id) {
      this.router.navigate(['/document/arrived/assignuser/' + id]);
      this.close(false);
    }

}