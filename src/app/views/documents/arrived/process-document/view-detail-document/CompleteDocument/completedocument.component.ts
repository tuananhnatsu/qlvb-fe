import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { ArrivedDocumentDto } from '@app/models/documents/arrived/arriveddocument.model';
import { ProcessDocumentService } from '@app/services/documents/arrived/process-document/process-document.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-completedocument',
    templateUrl: './completedocument.component.html'
})
export class CompleteDocumentComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    relatedFiles: File[] = [];
    form: ArrivedDocumentDto = new ArrivedDocumentDto();
    valueProcessing: any;
    valueProcessed: any;
    
    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) private _id: number,
        private _service: ProcessDocumentService,
        private _dialogRef: MatDialogRef<CompleteDocumentComponent>,
    ) {
        super(injector);
    }
    ngOnInit() {
        this.form.id = this._id;
        this.form.status = 4;
    }

    // onChangeProcess(value) {
    //     console.log(value)
    //     return
    //    this.form.status = value;
    // }

    save() {
        this._service.completeDocument(this.form).subscribe(() =>{
            this.close(true);
        });
    }

    close(result: any): void {
      this._dialogRef.close(result);
    }

}