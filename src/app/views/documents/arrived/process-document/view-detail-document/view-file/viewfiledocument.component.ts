import {
  Component,
  Injector,
  OnInit,
  ViewChild,
  ElementRef,
  Optional,
  Inject,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import * as _ from "lodash";
import { ProcessDocumentService } from "@app/services/documents/arrived/process-document/process-document.service";
import { finalize } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import { FileService } from "@app/services/file.service";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-viewfiledocument",
  templateUrl: "./viewfiledocument.component.html",
})
export class ViewFileDocumentComponent extends AppComponentBase
  implements OnInit {
  private _dialog: MatDialog;
  url = '';
  constructor(
    injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: string,
    private _dialogRef: MatDialogRef<ViewFileDocumentComponent>,
    dialog: MatDialog,
    private router: Router
  ) {
    super(injector);
    this._dialog = dialog;
  }
  ngOnInit() {
    this.url = 'https://docs.google.com/gview?url=14.238.8.242:5005/ArrivedDocumentFolder/19072020/demo.docx&embedded=true;'//this._data;
    console.log(this.url);
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
