import { Component, Injector, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
import { PagedArrivedFolderRequestDto, ArrivedFolderDto } from '@app/models/documents/arrived/folder/arrivedfolder.model';
import { LookupTableDto } from '@app/models/system-management/lookuptable.model';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CommonService } from '@app/services/common.service';
import { PagedCommonValueRequestDto, CommonValueDto } from '@app/models/common/commonvalue.model';
import { CategoryService } from '@app/services/system-management/category.service';
import { CategoryDto, PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import * as _ from 'lodash';
import { ArrivedDocumentDto } from '@app/models/documents/arrived/arriveddocument.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UserSysService } from '@app/services/system-management/user.service';
import { isNgTemplate } from '@angular/compiler';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '@app/services/file.service';
import { Observable, forkJoin } from 'rxjs';

@Component({
    selector: 'app-createoreditarriveddocument',
    templateUrl: './createoreditarriveddocument.component.html',
    styles: [
        `
          mat-form-field {
            padding: 10px;
          }
        `
    ]
})
export class CreateOrEditArrivedDocumentComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    saving = false;
    years: any[] = [];
    arrivedFolders: any[] = [];
    secretLevels: any[] = [];
    urgentLevels: any[] = [];
    documentNumbers: any[] = [];
    filteredDocumentNumbers: any[] = [];
    searchDocumentNumber: any;
    publishedOrganizations: any[] = [];
    filteredPublishedOrganizations: any[] = [];
    selectedPublishedOrganization: any;
    searchPublishedOrganization: any;
    documentFields: any[] = [];
    documentTypes: any[] = [];
    leaderShipApprovedUsers: any[] = [];
    formGroup: FormGroup;
    form: ArrivedDocumentDto = new ArrivedDocumentDto();
    relatedFiles: File[] = [];
    arrivedFolderId: number
   
    constructor(
        injector: Injector,
        private arrivedFolderService: ArrivedFolderService,
        private lookupTableService: LookupTableService,
        private commonService: CommonService,
        private categoryService: CategoryService,
        private userService: UserSysService,
        private _service: ArrivedDocumentService,
        private route: ActivatedRoute,
        private router: Router,
        private fileService: FileService
    ) {
        super(injector)
        this.initlizeFormGroup();
    }
    ngOnInit() {
        this.getMasterData();        
    }

    getMasterData() {
        let currentYear = (new Date()).getFullYear();
        for (var y = -5; y < 5; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
        let catRequest1 = new PagedCategoriesRequestDto();
        catRequest1.skipCount = 0;
        catRequest1.maxResultCount = 10000;
        catRequest1.type = [1];
        let catRequest2 = new PagedCategoriesRequestDto();
        catRequest2.skipCount = 0;
        catRequest2.maxResultCount = 10000;
        catRequest2.type = [2];
        let request1 = new PagedCommonValueRequestDto();
        request1.maxResultCount = 10000;
        request1.type = 1;
        let request2 = new PagedCommonValueRequestDto();
        request2.maxResultCount = 10000;
        request2.type = 2;

        // Sử dụng khi gọi nhiều API trong 1 function => xử lý bất đồng bộ khi cần lấy dữ liệu masterdata trước khi lấy dữ liệu chính
        forkJoin([
            this.arrivedFolderService.getAllArrivedFolders(new PagedArrivedFolderRequestDto()), 
            this.lookupTableService.getAllSecretLevelForLookupTable(), 
            this.lookupTableService.getAllUrgentLevelForLookupTable(),
            this.categoryService.getAllCategories(catRequest1),
            this.categoryService.getAllCategories(catRequest2),
            this.lookupTableService.getAllCommonValueByType(request1),
            this.lookupTableService.getAllCommonValueByType(request2),
            this.userService.getAllLeadershipApprovedPermission()
        ]).subscribe(response => {            
            _.forEach(response[0]['items'], item => {
                if(item.statusName = item.status === 0) {
                    this.arrivedFolders.push({ label: item.name, value: item.id, currentNumber: (this.form.id && this.form.id > 0 && this.form.arrivedFolderId === item.id) ? Number.parseInt(item.currentNumber) : Number.parseInt(item.currentNumber) + 1 });
                }             
            });
            _.forEach(response[1], item => {
                this.secretLevels.push({ label: item['displayName'], value: item['id'] });
            });
            _.forEach(response[2], item => {
                this.urgentLevels.push({ label: item['displayName'], value: item['id'] });
            });
            _.forEach(response[3]["items"], item => {
                this.documentFields.push({ label: item.name, value: item.id });
            });
            _.forEach(response[4]["items"], item => {
                this.documentTypes.push({ label: item.name, value: item.id });
            });
            _.forEach(response[5]["items"], item => {
                this.documentNumbers.push(item.value);
                this.filteredDocumentNumbers.push(item.value);
            });
            _.forEach(response[6]["items"], item => {
                this.publishedOrganizations.push(item.value);
                this.filteredPublishedOrganizations.push(item.value);
            });
            _.forEach(response[7]["items"], user => {
                this.leaderShipApprovedUsers.push({ label: `${user.userName} - ${user.fullName}`, value: user.id });
            });
        }, () => {}, () => {this.getArrivedDocumentForEdit()})
    }

    initlizeFormGroup() {
        this.formGroup = new FormGroup({
            shortDescriptionCtrl: new FormControl('', Validators.required),
            documentFieldCtrl: new FormControl('', Validators.required),
            documentTypeCtrl: new FormControl('', Validators.required),
            publishedDateCtrl: new FormControl('', Validators.required),
            arrivedDateCtrl: new FormControl('', Validators.required),
            yearCtrl: new FormControl('', Validators.required),
            arrivedNumberCtrl: new FormControl('', Validators.required),
            selectedDocumentNumberCtrl: new FormControl('', Validators.required),
            arrivedFolderCtrl: new FormControl(),
            signedByNameCtrl: new FormControl(),
            numberOfPublishCtrl: new FormControl(),
            numberOfPagesCtrl: new FormControl(),
            storedPlaceCtrl: new FormControl(),
            selectedPublishedOrganizationCtrl: new FormControl('', Validators.required),
            approvedByIdCtrl: new FormControl('', Validators.required),
            urgentLevelCtrl: new FormControl('', Validators.required),
            secretLevelCtrl: new FormControl('', Validators.required),
            descriptionCtrl: new FormControl()
        });
    }
    getArrivedDocumentForEdit(){
        const id = this.route.snapshot.queryParams['id'];
        if (id && id != null && id != '') {
            this._service.getArrivedDocumentForEdit(parseInt(id)).subscribe(rs => {
                this.form = ArrivedDocumentDto.fromJS(rs);
                this.arrivedFolderId = this.form.arrivedFolderId;
                if (this.form.status !== 0) {
                    this.formGroup.disable();
                }
            });
        }
        else 
        {
            this.form = ArrivedDocumentDto.fromJS(true);
                this.form.year = (new Date()).getFullYear();
        }
    }

    createCommonValue(type: number, value: string) {
        var dto = new CommonValueDto();
        dto.type = type;
        dto.value = value;
        this.commonService.create(dto).subscribe(rs => {
            if (type === 1) {
                this.filteredDocumentNumbers = [];
                this.filteredDocumentNumbers.push(rs.value);
                this.documentNumbers.push(rs.value);
            } else {
                this.filteredPublishedOrganizations = [];
                this.filteredPublishedOrganizations.push(rs.value);
                this.publishedOrganizations.push(rs.value);
            }
        });
    }
    filterDocumentNumbers(event) {
        this.filteredDocumentNumbers = [];
        this.filteredDocumentNumbers = this.documentNumbers.filter(item => item.toLowerCase().indexOf(event.query.toLowerCase()) >= 0);
        this.searchDocumentNumber = event.query;
    }
    filterPublishedOrganizations(event) {
        this.filteredPublishedOrganizations = [];
        this.filteredPublishedOrganizations = this.publishedOrganizations.filter(item => item.toLowerCase().indexOf(event.query.toLowerCase()) >= 0);
        this.searchPublishedOrganization = event.query;
    }
    createNewDocumentNumber(): void {
        if (this.searchDocumentNumber && this.searchDocumentNumber !== '') {
            this.createCommonValue(1, this.searchDocumentNumber);
        }
    }
    createNewPublishedOrganization(): void {
        if (this.searchPublishedOrganization && this.searchPublishedOrganization !== '') {
            this.createCommonValue(2, this.searchPublishedOrganization);
        }
    }
    save(): void {
        this.formGroup.markAllAsTouched();
        if (this.formGroup.invalid) return;
        if (this.relatedFiles.length <= 0 && this.form.files.length <= 0)
        {
            this.message.error("File văn bản không được để trống.");
            return;
        }
        this.saving = true;
        if (this.form.id && this.form.id != null) {
            if (this.relatedFiles.length <= 0) {
                this._service
                    .update(this.form)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                            if (this.form.status !== 1) {
                                this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Sửa văn bản đến!')});
                            } 
                            else 
                            {
                                this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Chuyển lãnh đạo duyệt!')});
                            }
                            this.router.navigate(['/document/arrived/officeagent'], { queryParams: { status: this.form.status } })
                        })
                    ).subscribe();
            } else {
                this.fileService.upload(this.relatedFiles, "arriveddocument").pipe(
                    finalize(() => {
                        this._service.update(this.form).pipe(
                            finalize(() => {
                                this.saving = false;
                                if (this.form.status !== 1) {
                                    this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Sửa văn bản đến!')});
                                } 
                                else 
                                {
                                    this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Chuyển lãnh đạo duyệt!')});
                                }
                                this.router.navigate(['/document/arrived/officeagent'], { queryParams: { status: this.form.status } })
                            })
                        ).subscribe(rs => {
                            this.form.id = rs.id;
                        })
                    })
                )
                .subscribe(response => {
                    this.form.attachedFiles = response.items;
                })
            }
        }
        else {
            this.fileService.upload(this.relatedFiles, "arriveddocument")
            .pipe(
                finalize(() => {
                    this._service.create(this.form).pipe(
                        finalize(() => {
                            this.saving = false;
                            this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới văn bản đến!')});
                        })
                    ).subscribe(rs => {
                        this.form.id = rs.id;
                    })
                })
            )
            .subscribe(response => {
                this.form.attachedFiles = response.items;
            })
        }
    }
    chooseFiles(event) {
        const fileList: FileList = event.target.files;
        for (let i=0; i<fileList.length; i++){
            // Kiem tra neu trong tep file da them da co file thi khong them vao nua
            if (this.relatedFiles.every(e => e.name !== fileList.item(i).name) && this.form.files.every(e => e.fileName !== fileList.item(i).name)) this.relatedFiles.push(fileList.item(i));
        }
    }
    focusChooseFiles() {
        this.chooseFile.nativeElement.click();
    }
    removeFile(index: number) {
        this.relatedFiles.splice(index, 1);
    }
    changeArrivedFolders(event) {        
        const arrivefolder = this.arrivedFolders.find(f => f.value === event.value);
        if (arrivefolder) {
            this.form.arrivedNumber = arrivefolder.currentNumber;            
        }
        if (arrivefolder && arrivefolder.value === this.arrivedFolderId && this.form.id && this.form.id > 0) {
            this.form.arrivedNumber = this.form.arrivedNumber - 1;        
        }        
    }
    removeAttachedFile(index: number) {
        this.form.files.splice(index, 1);
    }
    download(fileUrl: string) {
        this.fileService.download(fileUrl).pipe(finalize(() => {})).subscribe(response => {
        })
    }
    backtrack() {
        this.router.navigate(['/document/arrived/officeagent'], { queryParams: { status: this.form.status } });
    }
}