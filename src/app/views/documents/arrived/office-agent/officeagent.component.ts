import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { ArrivedDocumentDto } from '@app/models/documents/arrived/arriveddocument.model';
import { MatPaginator } from '@angular/material/paginator';
import { PagedRequestDto } from '../../../../shared/paged-listing-component-base';
import { OfficeAgentService } from '@app/services/documents/arrived/office-agent/office-agent.service';
import { PagedOfficeAgentRequestDto, OfficeAgentDtoPagedResultDto, OfficeAgentView, OfficeAgentDto } from '@app/models/documents/arrived/office-agent/office-agent.model';
import { MatDialog } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { LookupTableService } from '@app/services/lookuptable.service';
import { DocumentTypeService } from '@app/services/documents/document-types/document-type.service';
import { LookupTableDto } from '@app/models/system-management/lookuptable.model';
import * as _ from 'lodash';
import { DocumentTypeDto } from '@app/models/documents/document-types/document-type.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
import { ViewDetailDocumentComponent } from '../process-document/view-detail-document/viewdetaildocument.component';
import { PermissionCheckerService } from 'abp-ng2-module/dist/src/auth/permission-checker.service';
import { AppSessionService } from '@shared/session/app-session.service';

export class PagedRequestArrivedOfficeAgentDto extends PagedRequestDto{
    keyword: string
    shortDescription: string
    documentNumber: string
    status: number = 0
    urgentLevel: number
    arrivedFolders: Array<number>
    createFromDate: moment.Moment
    createToDate: moment.Moment
    createYears: number[] = []
    sendFromDate: moment.Moment
    sendToDate: moment.Moment
    sendYears: number[] = []
}

@Component({
    templateUrl: './officeagent.component.html',
    animations: [appModuleAnimation()]
})
export class ArrivedOfficeAgentComponent extends PagedListingComponentBase<ArrivedDocumentDto>  {

    public paginator: MatPaginator;
    private officeAgentService: OfficeAgentService;
    private lookupTableAppService: LookupTableService;
    private documentTypeService: DocumentTypeService;
    input = new PagedRequestArrivedOfficeAgentDto;
    private dialog: MatDialog

    isAdvancedSearch: boolean = true /*false*/;
    arrivedDocuments: OfficeAgentView[];
    selectedArrivedDocuments : OfficeAgentView[] = [];
    urgentLevels: Array<any> = [{label: this.l("-- Cấp độ --"), value: null}];
    arrivedFolders: Array<any> = [];
    documentTypeIds: number[];
    years: any[] = [];
    isCreateYearSearch = false;
    isSendYearSearch = false;

    numberOfDraftDocuments: number = 0
    numberOfForwardedDocuments: number = 0
    numberOfRevokedDocuments: number = 0
    isStasticLoading: boolean = false;

    // permission: PermissionCheckerService;
    // sessionService: AppSessionService;

    // Check quyen của user
    checkPermissionCreate: boolean

    columns: Array<any> = [
        { field: "urgentLevelName", header: this.l("Cấp độ"), align: "left" },
        { field: "arrivedNumber", header: this.l("Số đến"), align: "center" },
        { field: "arrivedFolderName", header: this.l("Sổ VB đến"), align: "left" },
        { field: "shortDescription", header: this.l("Trích yếu"), align: "left" },
        { field: "documentNumber", header: this.l("Số ký hiệu"), align: "left" },
        { field: "publishedOrganization", header: this.l("Cơ quan ban hành"), align: "left" },
        // { field: "creationTime", header: this.l("Ngày lưu") },
        // { field: "sentDate", header: this.l("Ngày chuyển") },
        // { field: "approvedName", header: this.l("Người xử lý chính") },
    ];

    constructor(
        injector: Injector, officeAgentService: OfficeAgentService,
        dialog: MatDialog, lookupTableAppService: LookupTableService, private arrivedFolderService: ArrivedFolderService, 
        private router: Router,
        private route: ActivatedRoute,
        private matDialog: MatDialog) {
        super(injector);
        this.officeAgentService = officeAgentService;
        this.dialog = dialog;
        this.lookupTableAppService = lookupTableAppService;
        this.route.queryParams.subscribe(param => {
            const status = param['status'];
            if (status) {
                this.input.status = status
            } else {
                // Start with DraftDocuments
                this.input.status = 0;
            }
        }) 
        // this.permission = injector.get(PermissionCheckerService);  
        // this.sessionService = s.get(AppSessionService); 
    }

    ngOnInit() {
        super.ngOnInit();
        this.initialize();
        this.checkPermissions();
    }

    private checkPermissions() {
        this.checkPermissionCreate = this.permission.isGranted("Pages.Documents.Received.CreateArrivedDocument")
        this.appSession.init().then(res => {
            if (res) {
                console.log(this.appSession.user);
            }
        })
    }

    private initialize(): void {
        this.initUrgentLevelsMap();
        this.initArrivedFolder();
        this.initYears();
        this.getStastic();
        
    }

    private initYears() {
        let currentYear = (new Date()).getFullYear();
        for (var y = -5; y < 5; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
    }

    private getStastic() {
        this.isStasticLoading = true;
        this.officeAgentService.getStasticArrivedDocument().subscribe(response => {
            if (response.success){
                this.numberOfDraftDocuments = response.result.numberOfDraftDocuments;
                this.numberOfForwardedDocuments = response.result.numberOfForwardedDocuments;
                this.numberOfRevokedDocuments = response.result.numberOfRevokedDocuments;
                this.isStasticLoading = false;
            }
        })
    }

    protected list(request: PagedRequestArrivedOfficeAgentDto, pageNumber: number, finishedCallback: Function): void {
        this.arrivedDocuments = [];
        request.keyword = this.input.keyword;
        request.shortDescription = this.input.shortDescription;
        request.urgentLevel = this.input.urgentLevel;
        request.documentNumber = this.input.documentNumber;
        request.status = this.input.status;
        request.arrivedFolders = this.input.arrivedFolders;
        request.createFromDate = this.isCreateYearSearch || request.status != 0 ? null : this.input.createFromDate;
        request.createToDate = this.isCreateYearSearch || request.status != 0 ? null : this.input.createToDate;
        request.createYears = this.isCreateYearSearch && request.status == 0 ? this.input.createYears : []
        request.sendFromDate = this.isSendYearSearch || request.status == 0 ? null : this.input.sendFromDate;
        request.sendToDate = this.isSendYearSearch || request.status == 0 ? null : this.input.sendToDate;
        request.sendYears = this.isSendYearSearch && request.status != 0 ? this.input.sendYears : []

        this.officeAgentService
            .filterOfficeAgents(request)
            .pipe(finalize(() => finishedCallback()))
            .subscribe((officeAgentPagedResult: OfficeAgentDtoPagedResultDto) => {
                this.showPaging(officeAgentPagedResult, pageNumber);
                if (officeAgentPagedResult.totalCount === 0) {
                    return;
                }
                for (let officeAgent of officeAgentPagedResult.items) {
                    let officeAgentView: OfficeAgentDto = OfficeAgentDto.fromJS(officeAgent);
                    // officeAgentView.urgentLevelName = officeAgent['urgentLevelName']
                    this.arrivedDocuments.push(officeAgentView);
                }
            });
    }
    protected delete(entity: ArrivedDocumentDto): void {
        this._confirmService.confirm({
            header: "Xác nhận",
            message: "Xác nhận xóa bản ghi này",
            accept: () => {
                this.officeAgentService.delete(entity.id).subscribe(response => {
                    if (response.success) {
                        this.getStastic();
                        this.getDataPage(0);
                        this._messageService.add({
                            severity: "success",
                            summary: "Thành công",
                            detail: "Xóa văn bản lưu nháp!"
                        })
                    }
                })
            }
        })
    }

    private initUrgentLevelsMap(): void {
        this.lookupTableAppService.getAllUrgentLevelForLookupTable()
            .subscribe(urgentLevels => {
                _.forEach(urgentLevels, level => {
                    this.urgentLevels.push({value: level.id, label: level.displayName});
                })
            });
    }

    private toggleAndvancedSearch() {
        this.isAdvancedSearch = !this.isAdvancedSearch;
    }

    private initArrivedFolder(): void {
        this.arrivedFolderService.getAll("id", 0, 10000).subscribe(response => {
            if (response.success) {
                _.forEach(response.result.items , item => {
                    this.arrivedFolders.push({value: item.id, label: item.name})
                })
            }
        })
    }

    editArrivedDocument(id: number) {
        this.router.navigate(['/document/arrived/editarriveddocument'], { queryParams: {
            id: id
        } })
    }
    viewArrivedDocument(id: number, allowRestrict: boolean = true) {
        this.matDialog.open(ViewDetailDocumentComponent, {
            width: "1200px",
            maxWidth: "1400px",
            data: {
                allowUpload: false,
                allowAction: false,
                allowRestrict: allowRestrict,
                id: id
            }
        })
    }
    createArrivedDocument() {
        this.router.navigate(['/document/arrived/createarriveddocument'])
    }
    changeCreateYears(event: any) {
        this.isCreateYearSearch = event.value.length > 0;       
    }
    changeSendYears(event: any) {
        this.isSendYearSearch = event.value.length > 0;       
    }
    refreshFilter() {
        this.input = new PagedRequestArrivedOfficeAgentDto;
    }
    updateStatusDocs(status: number) {
        // console.log(this.selectedArrivedDocuments)
        this._confirmService.confirm({
            header: "Xác nhận",
            message: "Xác nhận chuyển tiếp văn bản ?",
            accept: () => {
                this.officeAgentService.changeStatusArrivedDocuments({
                    status: status,
                    listArrivedDocumentsIdDto : this.selectedArrivedDocuments.map(m => m.id)
                }).subscribe(response => {
                    if (response.success) {
                        this.getStastic();
                        this.getDataPage(0);
                        this.selectedArrivedDocuments = [];
                        this._messageService.add({
                            severity: "success",
                            summary: "Thành công",
                            detail: "Chuyển tiếp văn bản đến!"
                        })
                    }
                })
            }
        })
    } 
    deleteMany() {
        this._confirmService.confirm({
            header: "Xác nhận",
            message: "Xác nhận xóa các bản ghi đã chọn ?",
            accept: () => {
                this.officeAgentService.removeManyArrivedDocument(this.selectedArrivedDocuments.map(m => m.id)).subscribe(response => {
                    if (response.success) {
                        this.getStastic();
                        this.getDataPage(0);
                        this.selectedArrivedDocuments = [];
                        this._messageService.add({
                            severity: "success",
                            summary: "Thành công",
                            detail: "Xóa văn bản lưu nháp!"
                        })
                    }
                })
            }
        })
    }
    viewDocumentHistory(id: number) {
        this.router.navigate(["/document/arrived/history"], { queryParams: { id: id } })
      }
}