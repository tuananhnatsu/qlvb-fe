import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmAssignDocumentComponent } from './confirm-assign-document.component';

describe('ConfirmAssignDocumentComponent', () => {
  let component: ConfirmAssignDocumentComponent;
  let fixture: ComponentFixture<ConfirmAssignDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmAssignDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmAssignDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
