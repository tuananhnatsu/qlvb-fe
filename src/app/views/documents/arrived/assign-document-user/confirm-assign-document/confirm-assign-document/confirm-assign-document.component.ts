import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { ProcessingDocumentGroupDto, ProcessingDocumentGroupDetailDto, ConfirmAssignDocumentRequestDto } from '@app/models/documents/arrived/arriveddocument.model';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-confirm-assign-document',
  templateUrl: './confirm-assign-document.component.html',
  styleUrls: ['./confirm-assign-document.component.css']
})
export class ConfirmAssignDocumentComponent extends AppComponentBase implements OnInit {

  formGroupCreate: FormGroup;
  confirmDto =  new ConfirmAssignDocumentRequestDto();

  constructor(  injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any,
    private dialogRef: MatDialogRef<ConfirmAssignDocumentComponent>,
    private _service: ArrivedDocumentService) { 
      super(injector);
      this.formGroupCreate = new FormGroup({
        contentCtrl: new FormControl('', Validators.required),
        statusCtrl: new FormControl(''),

      })

      if(data.arriveddocuments){
        this.confirmDto.users =[];
        this.confirmDto.users = data.arriveddocuments;
        this.confirmDto.status = 1;
        console.log(this.confirmDto);
      }
    }
  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.formGroupCreate.markAllAsTouched();
    this.formGroupCreate.markAsDirty();
    if (this.formGroupCreate.invalid) return;
      this._service
    .assignDocumentUser(this.confirmDto)
    .pipe(
        finalize(() => {
           this.dialogRef.close(true);
            this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Gán người xử lý thành công!')});
        })
    ).subscribe();
  }

}
