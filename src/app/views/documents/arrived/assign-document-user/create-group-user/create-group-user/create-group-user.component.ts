
import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { ProcessingDocumentGroupDto, ProcessingDocumentGroupDetailDto } from '@app/models/documents/arrived/arriveddocument.model';

@Component({
  selector: 'app-create-group-user',
  templateUrl: './create-group-user.component.html',
  styleUrls: ['./create-group-user.component.css']
})
export class CreateGroupUserComponent extends AppComponentBase implements OnInit {

  formGroupCreate: FormGroup;
  processingDocumentGroup = new ProcessingDocumentGroupDto();
  groupId: number|0;

  constructor(  injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any,
    private dialogRef: MatDialogRef<CreateGroupUserComponent>,
    private _service: ArrivedDocumentService) { 
      super(injector);
      this.formGroupCreate = new FormGroup({
        nameCtrl: new FormControl('', Validators.required),
      })

      if(data.arriveddocuments){
        this.processingDocumentGroup.users=[];i
        for(var  i=0 ; i<data.arriveddocuments.length ; i++){
          let processingDocumentGroupDetailDto = new ProcessingDocumentGroupDetailDto();
          processingDocumentGroupDetailDto.userId = data.arriveddocuments[i].userId;
          this.processingDocumentGroup.users.push(processingDocumentGroupDetailDto);
        }
      }
    }

  ngOnInit(): void {
   
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.formGroupCreate.markAllAsTouched();
    this.formGroupCreate.markAsDirty();
    if (this.formGroupCreate.invalid) return;
    this._service.createOrUpdateProcessingDocumentGroup(this.processingDocumentGroup).subscribe(response => {
      if (response) {
        this.dialogRef.close({data:response});
      }
    })
  }

}
