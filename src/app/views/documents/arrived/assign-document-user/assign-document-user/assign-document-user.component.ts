import { Component, OnInit, Injector } from '@angular/core';
import { CommonService } from '@app/services/common.service';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CategoryService } from '@app/services/system-management/category.service';
import { UserSysService } from '@app/services/system-management/user.service';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { TreeNode } from 'primeng/api/treenode';
import { DepartmentDto } from '@app/views/system-management/departments/departments.component';
import { DepartmentService } from '@app/services/system-management/department.service';
import { CategoryDto } from '@app/models/system-management/category.model';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CreateGroupUserComponent } from '../create-group-user/create-group-user/create-group-user.component';
import { ConfirmAssignDocumentComponent } from '../confirm-assign-document/confirm-assign-document/confirm-assign-document.component';
import { ArrivedDocumentUserDto, ProcessingDocumentGroupDto, ProcessingDocumentGroupDetailDto } from '@app/models/documents/arrived/arriveddocument.model';
import * as _ from 'lodash';
import * as moment from 'moment';
import { FormGroup, FormControl, FormArray } from '@angular/forms';


@Component({
  selector: 'app-assign-document-user',
  templateUrl: './assign-document-user.component.html',
  styleUrls: ['./assign-document-user.component.css']
})
export class AssignDocumentUserComponent extends AppComponentBase implements OnInit {

  isTableLoading = false;
  listTreeNodeDepartments: TreeNode[];
  departmentInfo = new DepartmentDto;
  arrrivedDocumentUsers: ArrivedDocumentUserDto[] = [];
  allowRoleIds = [];
  processingReasonIds = [];
  arrivedDocumentId : number| undefined;
  defaultProcessingId : number|undefined;
  defautAllowRoleId : number | undefined;
  processingDocumentGroupId:number|0;
  processingDocumentGroup = new ProcessingDocumentGroupDto;
  processingDocumentGroups : ProcessingDocumentGroupDto []=[];
  processingDocumentGroupDetails : ProcessingDocumentGroupDetailDto []=[];
  formGroup: FormGroup;
  selectTabIndex = 0 ;//first SelectTab is 0 (tab cans bo)
  cols = [
      {
          field: "userFullName",
          header: "Cán bộ"
          ,width:'15%'
      },
      {
          field: "allowRoleId",
          header: this.l("Vai trò"),
          width:'23%'
      },
      {
          field: "processingReasonId",
          header: this.l("Lý do")
          ,width:'23%'
      },
      {
          field: "nonExpired",
          header: this.l("Không thời hạn")
          ,width:'10%'
      },
      {
          field: "expiredDate",
          header: this.l("Hạn xử lý")
          ,width:'24%'
      }
    ];
    colProcessingDocumentGroup = [
      {
        field: "name",
        header: "Tên nhóm"
        ,width:'50%'
      },
      {
          field: "creationDate",
          header: this.l("Ngày tạo"),
          width:'50%'
      },
    ]
  colProcessingDocumentGroupDetail=[
      {
          field: "departmentName",
          header: "Phòng ban"
          ,width:'15%'
      },
      {
          field: "departmentId",
          header: this.l("Chức vụ"),
          width:'23%'
      },
      {
          field: "fullName",
          header: this.l("Tên cán bộ")
          ,width:'23%'
      }
    ];
  constructor(
    injector: Injector,
    private departmentService: DepartmentService,
    private dialog: MatDialog,
    private arrivedFolderService: ArrivedFolderService,
    private lookupTableService: LookupTableService,
    private commonService: CommonService,
    private categoryService: CategoryService,
    private userService: UserSysService,
    private _service: ArrivedDocumentService,
    private route: ActivatedRoute,
    private _router: Router,
) {
    super(injector);
}
  getMasterData():void{
    this.lookupTableService.getAllAllowRoleIdForLookupTable().subscribe(rs => {
      _.forEach(rs, item => {
          this.allowRoleIds.push({ label: item.displayName, value: item.id });
      }

      );
      this.defautAllowRoleId =  this.allowRoleIds[0].value;
    });
    this.lookupTableService.getProcessingReasonForLookupTable().subscribe(rs => {
      _.forEach(rs, item => {
          this.processingReasonIds.push({ label: item.displayName, value: item.id });
      }

      );
      this.defaultProcessingId = this.processingReasonIds[0].value;
    });
    this.filterUserDocument();
  }
  

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.arrivedDocumentId = +id;
    // this.formGroup = new FormGroup({
    //   arrayName: new FormArray([])
    // });
    this.getMasterData();

  }
  getArrivedDocumentArray(): FormArray {
    var formArray =  this.formGroup.get("arrayName") as FormArray;
    return formArray;
  }
  // addCalendarCtrl(index:number){
  //   var f : FormGroup =new FormGroup({
  //     "expiredDateCtrl":new FormControl("",[])
  //     }
  //   )
  //   this.getArrivedDocumentArray().push(f);
  // }


  filterUserDocument(keyword?: string, dependentDepartmentId?: number) {
    this.isTableLoading = true;
    this.departmentService.getTreeUserDepartment(keyword, dependentDepartmentId).subscribe(response => {
      if (response.success) {
        this.listTreeNodeDepartments = response.result.items;
      }
    },() => {}, () => {
      this.isTableLoading = false;
    })
  }
  onChangeTab(event){
    var index = event.index;
    this.selectTabIndex=index;
    if(index == 1){
      this.reloadProcessingGroup();
    }
  }
  reloadProcessingGroup(){
    this.isTableLoading = true;
    this._service.getAllProcessingDocumentGroup().subscribe(response => {
      if (response.success) {
        this.processingDocumentGroups = response.result.items;
      }
    },() => {}, () => {
      this.isTableLoading = false;
    })
  }
  formatDateTime(ngayHienThi){
    return moment(ngayHienThi).format('DD/MM/YYYY');
  }

  onSelectProcessingGroup (rowData){
    this.isTableLoading = true;
    this._service.getAllProcessingDocumentDetailGroup(rowData.id).subscribe(response => {
      if (response.success) {
        this.processingDocumentGroupDetails = response.result.items;
      }
    },() => {}, () => {
      this.isTableLoading = false;
    })
  }
  nodeSelect(event) {
    this.processSelectNode(event.node);
  }
  processSelectNode(node){
    if(node.data.type == 0){
      this.addUser(node.data);
    }else{
      for(let i=0;i<node.children.length;i++ ){
        this.processSelectNode(node.children[i]);
      }
    }
  }

  nodeUnSelect(event) {
    this.processUnSelectNode(event.node);
  }


  applyProcessingGroup(){
    if(this.processingDocumentGroupDetails.length==0){
      this._messageService.add({severity:'warn', summary: this.l('Thất bại'), detail: this.l('Cần chọn nhóm có dữ liệu')});
      return;
    }
    this.arrrivedDocumentUsers=[];
    _.forEach(this.processingDocumentGroupDetails, item => {
      let arriveDOcument = new ArrivedDocumentUserDto (
        { userId: item.userId,
           userFullName: item.username,
           allowRoleId:this.defautAllowRoleId,
           processingReasonId :this.defaultProcessingId,
           nonExpired:true,
           arrivedDocumentId:this.arrivedDocumentId
        }
      );
      this.arrrivedDocumentUsers.push(arriveDOcument);
    });
    this.selectTabIndex = 0;
  }


  processUnSelectNode(node){
    if(node.data.type == 0){
      this.removeUser(node.data);
    }else{
      for(let i=0;i<node.children.length;i++ ){
        this.processUnSelectNode(node.children[i]);
      }
    }
  }
  removeUser(data){
    let index = this.arrrivedDocumentUsers.findIndex(x=>x.userId == data.id);

    if(index >= 0){
      this.arrrivedDocumentUsers.splice(index,1);
    }
  }
  changeNonExpired(event,rowIndex){
    console.log(event);
    console.log(rowIndex);
  }
  addUser(data){
    let index = this.arrrivedDocumentUsers.findIndex(x=>x.userId == data.id);
    if(index < 0){
      let arrrivedDocumentUser : ArrivedDocumentUserDto = new ArrivedDocumentUserDto({
        userId : data.id,
        userFullName : data.displayName,
        nonExpired:true,
        arrivedDocumentId:this.arrivedDocumentId,
        allowRoleId: this.defautAllowRoleId,
        processingReasonId: this.defaultProcessingId,
      })
      this.arrrivedDocumentUsers.push(arrrivedDocumentUser);
      //this.addCalendarCtrl(this.arrrivedDocumentUsers.length-1);
    }
  }
  checkExpireDate(rowData:ArrivedDocumentUserDto):boolean{
    if(rowData.allowRoleId ==this.defautAllowRoleId ){
      if(!rowData.nonExpired){
        if(!rowData.expiredDate){
          return true;
        }
      }
    }
    return false;
  }

  save(){
    if(this.arrrivedDocumentUsers.length==0){
      this._messageService.add({severity:'warn', summary: this.l('Thất bại'), detail: this.l('Cần gán người xử lý')});
      return;
    }
    for(let x of this.arrrivedDocumentUsers ){
      if(this.checkExpireDate(x)){
        return;
      }
    }
    this.onConfirmAssignDocument();
  }

  onConfirmAssignDocument() {
    this.dialog.open(ConfirmAssignDocumentComponent, {
      data: {
        arriveddocuments: this.arrrivedDocumentUsers
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        //redirect to document
        this._router.navigate(["/document/arrived/processdocument"]);
      }
    })
  }

  /**
   * tao nhom can bo
   */
  onCreateUserGroup() {
    if(this.arrrivedDocumentUsers.length==0){
      this._messageService.add({severity:'warn', summary: this.l('Cảnh báo'), detail: this.l('Nhóm cần có cán bộ')});
      return;
    }
    this.dialog.open(CreateGroupUserComponent, {
      data: {
        arriveddocuments: this.arrrivedDocumentUsers
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        if(response.data){
          this.processingDocumentGroupId = response.data.id;
          this.processingDocumentGroup = response.data;
        }
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới nhóm thành công')});
      }
    })
  }
  updateCreateUserGroup(){
    this.processingDocumentGroup.users=[];
    if(this.arrrivedDocumentUsers.length==0){
      this._messageService.add({severity:'warn', summary: this.l('Cảnh báo'), detail: this.l('Nhóm cần có cán bộ')});
      return;
    }
    for(var  i=0 ; i<this.arrrivedDocumentUsers.length ; i++){
      let processingDocumentGroupDetailDto = new ProcessingDocumentGroupDetailDto();
      processingDocumentGroupDetailDto.userId = this.arrrivedDocumentUsers[i].userId;
      this.processingDocumentGroup.users.push(processingDocumentGroupDetailDto);
    }
    this._service.createOrUpdateProcessingDocumentGroup(this.processingDocumentGroup).subscribe(response => {
      if (response) {
        this.processingDocumentGroupId = response.id;
        this.processingDocumentGroup=response;
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Update nhóm thành công')});

      }
    })
  }

  close () {}

}
