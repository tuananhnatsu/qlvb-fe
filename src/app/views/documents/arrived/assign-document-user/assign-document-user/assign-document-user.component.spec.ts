import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDocumentUserComponent } from './assign-document-user.component';

describe('AssignDocumentUserComponent', () => {
  let component: AssignDocumentUserComponent;
  let fixture: ComponentFixture<AssignDocumentUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignDocumentUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDocumentUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
