import { AppComponentBase } from '@shared/app-component-base';
import { Component, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ArrivedFolderDto } from '@app/models/documents/arrived/folder/arrivedfolder.model';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
@Component({
    templateUrl: 'create-arrived-folder-dialog.component.html'
})
export class CreateArrivedFolderDialogComponent extends AppComponentBase {
    statuses: any[] = [{label: this.l('-- Trạng thái --'), value: null},{label:this.l('Mở'),value:0},{label:this.l('Khóa'),value:1}];
    formGroup: FormGroup;
    saving = false;
    model: ArrivedFolderDto = new ArrivedFolderDto();
    years: any[] = [];
    constructor(
        injector: Injector,
        private _service: ArrivedFolderService,        
        private _dialogRef: MatDialogRef<CreateArrivedFolderDialogComponent>
    ) {
        super(injector);
        let currentYear = (new Date()).getFullYear();
        this.years = [{ label : this.l('-- Năm --'), value: null }];
        for (var y = -10; y < 10; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
        this.formGroup = new FormGroup({
            nameCtrl: new FormControl('', Validators.required),
            codeCtrl: new FormControl(),
            yearCtrl: new FormControl(),
            priorityCtrl: new FormControl(),
            statusCtrl: new FormControl(0),            
        })
    }
    save(): void {    
      if (this.formGroup.invalid) return;    
        this.saving = true;   
    
        this._service
          .create(this.model)
          .pipe(
            finalize(() => {
              this.saving = false;
            })
          )
          .subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close(true);
          });
      }
    
      close(result: any): void {
        this._dialogRef.close(result);
      }
}