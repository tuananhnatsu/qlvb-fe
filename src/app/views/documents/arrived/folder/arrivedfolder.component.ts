import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { ArrivedFolderDto, PagedArrivedFolderRequestDto, ArrivedFolderDtoPagedResultDto } from '@app/models/documents/arrived/folder/arrivedfolder.model';
import { MatDialog } from '@angular/material/dialog';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { CreateArrivedFolderDialogComponent } from './modals/create-arrived-folder/create-arrived-folder-dialog.component';
import { EditArrivedFolderDialogComponent } from './modals/edit-arrived-folder/edit-arrived-folder-dialog.component';

@Component({
    templateUrl: './arrivedfolder.component.html',
    animations: [appModuleAnimation()]
})
export class ArrivedFolderComponent extends PagedListingComponentBase<ArrivedFolderDto>{
    paginator
    input = new PagedArrivedFolderRequestDto;
    arrivedFolders: ArrivedFolderDto[] = [];
    years: any[] = [];
    statuses: any[] = [{label: this.l('-- Trạng thái --'), value: null},{label:this.l('Mở'),value:0},{label:this.l('Đã khóa'),value:1}];
    cols = [
        {
            field: "name",
            header: this.l("Tên sổ")
        },
        {
            field: "currentNumber",
            header: this.l("Số đến hiện tại")
        },
        {
            field: "statusName",
            header: this.l("Trạng thái")
        },
        {
            field: "year",
            header:this.l("Năm")
        },
        {
            field: "act",
            header: this.l("Actions")
        },
        
    ];
    keyword = '';
    constructor(
        injector: Injector,
        private _service: ArrivedFolderService,
        private _dialog: MatDialog
    ) {
        super(injector);        
        // let currentyear = new Date().getFullYear();
        // this.years = [{ label: this.l('-- Năm --'), value: null }];
        // for (var y = -5; y < 5; y++) {            
        //     this.years.push({ label: currentyear + y, value: currentyear + y });
        // }
    }
    protected list(request: PagedArrivedFolderRequestDto,
        pageNumber: number,
        finishedCallback: Function): void {

        request.keyword = this.input.keyword;
        request.status = this.input.status;
        request.year = this.input.year;
        
        this._service
            .getAllArrivedFolders(request)
            .pipe(
                finalize(() => {
                    finishedCallback();
                })
            )
            .subscribe((result: ArrivedFolderDtoPagedResultDto) => {
                this.arrivedFolders = result.items; 
                _.map(this.arrivedFolders, item => {
                    item.statusName = item.status === 0 ? this.l('Mở'):this.l('Đã khóa');
                });
                this.showPaging(result, pageNumber);                
            });
    }
    protected delete(entity: ArrivedFolderDto): void {
        abp.message.confirm(
            this.l('ArrivedFolderDeleteWarningMessage', entity.name),
            undefined,
            (result: boolean) => {
                if (result) {
                    this._service
                        .delete(entity.id)
                        .pipe(
                            finalize(() => {
                                abp.notify.success(this.l('Xóa thành công'));
                                this.refresh();
                            })
                        )
                        .subscribe(() => {
                        }, err => {
                            if(err.error.error.message === "EXIST_DOCUMENTS") {
                              this.message.error(this.l("Không thể xóa sổ khi có văn bản trong sổ!"), this.l("Tồn tại văn bản"))
                            };
                          });
                }
            }
        );
    }    
    create(): void {
        this.showCreateOrEditArrivedFolderDialog();
    }

    edit(cat: ArrivedFolderDto): void {
        this.showCreateOrEditArrivedFolderDialog(cat.id);
    }

    showCreateOrEditArrivedFolderDialog(id?: number): void {
        let createOrEditArrivedFolderDialog;
        if (id === undefined || id <= 0) {
            createOrEditArrivedFolderDialog = this._dialog.open(CreateArrivedFolderDialogComponent, {
                width: '700px',
                maxWidth: "800px",
                minHeight: '400px'
            });
        } else {
            createOrEditArrivedFolderDialog = this._dialog.open(EditArrivedFolderDialogComponent, {
                width: '700px',
                maxWidth: "800px",
                minHeight: '400px',
                data: id
            });
        }

        createOrEditArrivedFolderDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh();
            }
        });
    }
}