import { Component, OnInit } from '@angular/core';
import { ArrivedDocumentHistoryService, PagedRequestArrivedDocumentHistoryDto } from '@app/services/documents/arrived/history/arriveddocumenthistory.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'arrived-documents-history',
  templateUrl: './arrived-documents-history.component.html',
  styleUrls: ['./arrived-documents-history.component.css']
})
export class ArrivedDocumentsHistoryComponent implements OnInit {

  input = new PagedRequestArrivedDocumentHistoryDto
  listHistory = []
  selectedHistory: any

  constructor(
    private arrivedDocumentHistoryService: ArrivedDocumentHistoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.input.arrivedDocumentId = this.route.snapshot.queryParams["id"];
    this.getArrivedDocumentHistory();
  }

  getArrivedDocumentHistory() {
    this.arrivedDocumentHistoryService.searchHistoryByArrivedDocumentId(this.input).subscribe(response => {
      if (response.success) {
        this.listHistory = response.result;
      }
    })
  }

  selectHistoryToView(history: any) {
    this.selectedHistory = this.selectedHistory == history ? null : history;
  }

}
