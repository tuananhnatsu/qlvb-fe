import { Component, OnInit } from '@angular/core';
import { PagedRequestDepartureDocumentHistoryDto } from '@app/services/documents/departure/history/departuredocumenthistory.service';

@Component({
  selector: 'departure-documents-history',
  templateUrl: './departure-documents-history.component.html',
  styleUrls: ['./departure-documents-history.component.css']
})
export class DepartureDocumentsHistoryComponent implements OnInit {

  input = new PagedRequestDepartureDocumentHistoryDto()
  listHistory = []
  selectedHistory: any
  constructor() { }

  ngOnInit(): void {
  }

}
