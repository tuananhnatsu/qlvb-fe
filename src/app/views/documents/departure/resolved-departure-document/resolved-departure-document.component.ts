import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { DepartmentDto } from '@app/views/system-management/departments/departments.component';
import { DepartureDocumentService } from '@app/services/documents/departure/departure-document.service';
import { DepartureDocumentForTableDto, DepartureDocumentPagedRequestDto } from '@app/models/documents/departure/departure-document/departure-document.model';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from '@app/services/system-management/category.service';
import { finalize } from 'rxjs/operators';
import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import { ViewOrEditDepartureDocumentComponent } from '../view-or-edit-departure-document/vieworeditdeparturedocument.component';
import * as _ from 'lodash';

@Component({
    selector: 'app-resolveddeparturedocument',
    templateUrl: './resolved-departure-document.component.html'
})
export class ResolvedDepartureDocument extends PagedListingComponentBase<DepartureDocumentForTableDto>{
    statusDisplay = 2
    paginator;
    columns = [
        { header: this.l("Cấp độ khẩn"), field: "urgentLevelName" },
        { header: this.l("Trích yếu"), field: "shortDescription" },
        { header: this.l("Ngày trình"), field: "sentDate" }
    ]
    isStatisticLoading = false;
    numberOfSentDocuments = 0;
    numberOfConfirmedDocuments = 0;
    numberOfCancelDocuments = 0;
    numberOfProcessedDocuments = 0;
    isYearSearch = false;
    listdepartureDocuments: DepartureDocumentForTableDto[]
    input = new DepartureDocumentPagedRequestDto
    documentFields : any[] = [{label: "-- Lĩnh vực chuyên môn --", value: null}]
    documentTypes: any[] = [{label: "-- Loại văn bản --", value: null}]
    years: any[] = []
    private _dialog: MatDialog;


    constructor( 
        injector: Injector, 
        private _departureDocumentService: DepartureDocumentService,
        private _categoryService: CategoryService,
        dialog: MatDialog,) 
        {
        super(injector);
        this._dialog = dialog;
        this.input.status = 1;
    }
    ngOnInit() {
        super.ngOnInit();
        this.getStatistic();
        this.getMasterData();
    }
    
    protected delete() {}

    protected list(
        request: DepartureDocumentPagedRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ) {
        request.keyword = this.input.keyword;
        request.status = this.input.status == 0 ? 1 : this.input.status;
        request.year = this.isYearSearch ? this.input.year : []; 
        request.documentFieldId = this.input.documentFieldId;
        request.documentTypeId = this.input.documentTypeId;
        request.fromDate = this.isYearSearch ? null : this.input.fromDate;
        request.toDate = this.isYearSearch ? null : this.input.toDate;
        request.isProcessed = true;
        this._departureDocumentService
          .searchDocuments(request)
          .pipe(
              finalize(() => {
                  finishedCallback();
              })
          )
          .subscribe(response => {
            this.listdepartureDocuments = response.items;
            this.showPaging(response.items, pageNumber);
          })
    }

    protected getStatistic() {
        this.isStatisticLoading = true;
        this._departureDocumentService.getResolvedDepartureDocumentStatistic().pipe(finalize(() => {
            this.isStatisticLoading = false;
        })).subscribe(response => {
            if (response) {
                this.numberOfSentDocuments = response.result.numberOfSentDocuments
                this.numberOfConfirmedDocuments = response.result.numberOfConfirmedDocuments
                this.numberOfCancelDocuments = response.result.numberOfCancelDocuments
                this.numberOfProcessedDocuments = response.result.numberOfProcessedDocuments
            }
        })
    }

    private getMasterData() {
        let currentYear = (new Date()).getFullYear();
        for (var y = -5; y < 5; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
        let catRequest = new PagedCategoriesRequestDto();
        catRequest.skipCount = 0;
        catRequest.maxResultCount = 10000;
        catRequest.type = [1];
        this._categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentFields.push({ label: item.name, value: item.id });
            });

        });
        catRequest.type = [2];
        this._categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentTypes.push({ label: item.name, value: item.id });
            });
        });
    }
    refreshFilter(status: number | undefined) {
        this.input = new DepartureDocumentPagedRequestDto();
        this.input.status = status
    }
    changeYears(event: any) {
        this.isYearSearch = event.value.length > 0;       
    }
    
    viewArrivedDocument(id?: number): void {
        let openModal;
        openModal = this._dialog.open(ViewOrEditDepartureDocumentComponent, {
          width: "1200px",
          maxWidth: "1400px",
          data: {
            allowUpload: true,
            allowAction: this.isGranted("Pages.Administration"),
            id: id
          }
        });
    
        openModal.afterClosed().subscribe((result) => {
          if (result) {
            this.refresh();
          }
        });
      }
}