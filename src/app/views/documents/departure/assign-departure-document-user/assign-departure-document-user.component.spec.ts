import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDepartureDocumentUserComponent } from './assign-departure-document-user.component';

describe('AssignDepartureDocumentUserComponent', () => {
  let component: AssignDepartureDocumentUserComponent;
  let fixture: ComponentFixture<AssignDepartureDocumentUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignDepartureDocumentUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDepartureDocumentUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
