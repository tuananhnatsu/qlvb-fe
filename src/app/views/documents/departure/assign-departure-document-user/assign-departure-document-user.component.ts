import { Component, OnInit, Injector } from '@angular/core';
import { CommonService } from '@app/services/common.service';
import { ArrivedFolderService } from '@app/services/documents/arrived/folder/arrivedfolder.service';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CategoryService } from '@app/services/system-management/category.service';
import { UserSysService } from '@app/services/system-management/user.service';
import { ArrivedDocumentService } from '@app/services/documents/arrived/arriveddocument.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { TreeNode } from 'primeng/api/treenode';
import { DepartmentDto } from '@app/views/system-management/departments/departments.component';
import { DepartmentService } from '@app/services/system-management/department.service';
import { CategoryDto } from '@app/models/system-management/category.model';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ArrivedDocumentUserDto, ProcessingDocumentGroupDto, ProcessingDocumentGroupDetailDto } from '@app/models/documents/arrived/arriveddocument.model';
import * as _ from 'lodash';
import * as moment from 'moment';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import {  DepartureDocumentUserDto, AssignDepartureDocumentUserDto } from '@app/models/documents/departure/departure-document/departure-document.model';
import { DepartureDocumentService } from '@app/services/documents/departure/departure-document.service';


@Component({
  selector: 'assign-departure-document-user',
  templateUrl: './assign-departure-document-user.component.html',
  styleUrls: ['./assign-departure-document-user.component.css']
})
export class AssignDepartureDocumentUserComponent extends AppComponentBase implements OnInit {

  
  isTableLoading = false;
  listTreeNodeDepartments: TreeNode[];
  selectedTreeNode:TreeNode[];
  departmentInfo = new DepartmentDto;
  departureDocumentUsers: DepartureDocumentUserDto[] = [];
  allowRoleIds = [];
  processingReasonIds = [];
  departureDocumentId : number| undefined;
  assignDepartureDocumentUserDto = new AssignDepartureDocumentUserDto;
  
  // processingDocumentGroup = new ProcessingDocumentGroupDto;
  // processingDocumentGroups : ProcessingDocumentGroupDto []=[];
  // processingDocumentGroupDetails : ProcessingDocumentGroupDetailDto []=[];
  formGroup: FormGroup;
  selectTabIndex = 0 ;//first SelectTab is 0 (tab cans bo)
  cols = [
      {
          field: "userFullName",
          header: "Cán bộ"
          
      },
      {
          field: "nonExpired",
          header: this.l("Không thời hạn")
         
      },
      {
          field: "expiredDate",
          header: this.l("Hạn xử lý")
         
      }
    ];
  constructor(
    injector: Injector,
    private _departmentService: DepartmentService,
    private dialog: MatDialog,
    private categoryService: CategoryService,
    private _departureDocumentService: DepartureDocumentService,
    private route: ActivatedRoute,
    private _router: Router,
) {
    super(injector);
}
  getMasterData():void{
    this.filterUserDocument();
  }
  

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.departureDocumentId = +id;
    this.getMasterData();

  }
  getArrivedDocumentArray(): FormArray {
    var formArray =  this.formGroup.get("arrayName") as FormArray;
    return formArray;
  }

  filterUserDocument(keyword?: string, dependentDepartmentId?: number) {
    this.isTableLoading = true;
    this._departmentService.getTreeUserDepartment(keyword, dependentDepartmentId).subscribe(response => {
      if (response.success) {
        this.listTreeNodeDepartments = response.result.items;
      }
    },() => {}, () => {
      this.isTableLoading = false;
    })
  }


  formatDateTime(ngayHienThi){
    return moment(ngayHienThi).format('DD/MM/YYYY');
  }

 
  nodeSelect(event) {
    var node=event.node;
    if(node.data.type == 0){
      for(let item of this.selectedTreeNode){
        this.processUnSelectNode(item);
      }
      this.selectedTreeNode=[];
      this.selectedTreeNode.push(node);
      this.addUser(node.data);
    }else{
      for(let item of this.selectedTreeNode){
        this.processUnSelectNode(item);
      }
      this.selectedTreeNode=[];
      this.selectedTreeNode.push(node);
    }
  }
 
  nodeUnSelect(event) {
    this.processUnSelectNode(event.node);
   
  }

  processUnSelectNode(node){
    if(node.data.type == 0){
      this.removeUser(node.data);
    }
  }
  removeUser(data){
    let index = this.departureDocumentUsers.findIndex(x=>x.userId == data.id);

    if(index >= 0){
      this.departureDocumentUsers.splice(index,1);
    }
  }
  changeNonExpired(event,rowIndex){
    console.log(event);
    console.log(rowIndex);
  }
  addUser(data){
    let index = this.departureDocumentUsers.findIndex(x=>x.userId == data.id);
    if(index < 0){
      let departureDocumentUser : DepartureDocumentUserDto = new DepartureDocumentUserDto({
        userId : data.id,
        userFullName : data.displayName,
        nonExpired:true,
        departureDocumentId:this.departureDocumentId
      })
      this.departureDocumentUsers.push(departureDocumentUser);
    }
  }
 

  save(){
    if(this.departureDocumentUsers.length==0){
      this._messageService.add({severity:'warn', summary: this.l('Thất bại'), detail: this.l('Cần gán người xử lý')});
      return;
    }
    this.onConfirmAssignDocument();
  }

  onConfirmAssignDocument() {
    this.assignDepartureDocumentUserDto.departureDocumentUserDto =  this.departureDocumentUsers[0];
    this._departureDocumentService.assignDepartureDocumentUser(this.assignDepartureDocumentUserDto).subscribe(response => {
      if (response) {
        this._router.navigate(["document/departure/unresolveddeparturedocument"]);
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Chuyển yêu cầu thành công')});
      }
    })
  
  }

  close () {}

}
