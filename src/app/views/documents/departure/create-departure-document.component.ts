import { Component, Injector, OnInit, ViewChild, ElementRef,  Input, Output, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CommonService } from '@app/services/common.service';
import { PagedCommonValueRequestDto, CommonValueDto } from '@app/models/common/commonvalue.model';
import { CategoryService } from '@app/services/system-management/category.service';
import { CategoryDto, PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import * as _ from 'lodash';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserSysService } from '@app/services/system-management/user.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '@app/services/file.service';
import { TreeNode } from 'primeng/api/treenode';
import { DepartmentService } from '../../../services/system-management/department.service';
import { EventEmitter } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { SendRequestDocumentComponent } from './sendrequest/sendrequestdocument.component';
import { DepartureDocumentForTableDto } from '../../../models/documents/departure/departure-document/departure-document.model';
import { DepartureDocumentService } from '../../../services/documents/departure/departure-document.service';
import { UserDto } from '../../../models/system-management/user.model';
import { AppSessionService } from '../../../shared/session/app-session.service';

@Component({
    selector: 'app-createdeparturedocument',
    templateUrl: './create-departure-document.component.html'
})
export class CreateDepartureDocumentComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    @ViewChild('chooseFileAttach') chooseFileAttach: ElementRef;
    
    saving = false;
    years: any[] = [{label:'-Chọn năm-', value: null}];
    arrivedFolders: any[] = [];
    secretLevels: any[] = [{label:'-Chọn cấp độ mật-', value: null}];
    urgentLevels: any[] = [{label:'-Chọn cấp độ khẩn-', value: null}];
    documentNumbers: any[] = [];
    filteredDocumentNumbers: any[] = [];
    searchDocumentNumber: any;
    publishedOrganizations: any[] = [];
    filteredPublishedOrganizations: any[] = [];
    selectedPublishedOrganization: any;
    searchPublishedOrganization: any;
    documentFields: any[] = [{label:'-Chọn lĩnh vực văn bản-', value: null}];
    documentTypes: any[] = [];
    departments: TreeNode[] = [];
    leaderShipApprovedUsers: any[] = [];
    formGroup: FormGroup;
    form: DepartureDocumentForTableDto = new DepartureDocumentForTableDto();
    relatedFiles: File[] = [];
    attachFiles: File[] = [];
    arrivedFolderId: number;
    values = [{label:'-Chọn phòng ban soạn thảo-', value: null}]
    users: any[] = [];
    isAttachFile = false;
    isSuccess = false;
    fileUpload: any[] = [];
    data = {Files:[], form: {}, isFirstStep: true, isDraft: false};

    //for tree dropdown 
    @ViewChild('filter', {static: true})
    targetElement: ElementRef;
    @Input()
    dropDownHeight = 300;
    @Input()
    treeData;
    @Input()
    selection: TreeNode;
    @Output()
    selectionChange = new EventEmitter();
    visible: boolean = false;
    id: any = Math.floor(Math.random() * 1000);
    firstTimeLoad = true;
    dropDownEl;
    //end

    private _dialog: MatDialog;
    isCanApproved: boolean;
    isCanRejected: boolean;

    constructor(
        injector: Injector,
        private lookupTableService: LookupTableService,
        private commonService: CommonService,
        private categoryService: CategoryService,
        private userService: UserSysService,
        private _service: DepartureDocumentService,
        private route: ActivatedRoute,
        private router: Router,
        private fileService: FileService,
        private departmentService: DepartmentService,
        dialog: MatDialog,
        private sessionService: AppSessionService,
        private _router: Router,
    ) {
        super(injector)
        this.initlizeFormGroup();
        this._dialog = dialog;

    }
    ngOnInit() {
        this.getMasterData();
        this.getAllEmployees();
        this.sessionService.init().then(rs => {
            this.form.createdById = this.sessionService.userId;
        })
    }

    // private checkPermissions() {
    //     this.isCanApproved = this.permission.isGranted("Pages.Documents.Departure.CanApproved")
    //     this.isCanRejected = this.permission.isGranted("Pages.Documents.Departure.CanRejected")
    // }

    getMasterData() {
        let currentYear = (new Date()).getFullYear();
        for (var y = -5; y < 5; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
       
        this.lookupTableService.getAllSecretLevelForLookupTable().subscribe(rs => {
            _.forEach(rs, item => {
                this.secretLevels.push({ label: item.displayName, value: item.id });
            });
        });
        this.lookupTableService.getAllUrgentLevelForLookupTable().subscribe(rs => {
            _.forEach(rs, item => {
                this.urgentLevels.push({ label: item.displayName, value: item.id });
            });
        });
        let catRequest = new PagedCategoriesRequestDto();
        catRequest.skipCount = 0;
        catRequest.maxResultCount = 10000;
        catRequest.type = [1];
        this.categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentFields.push({ label: item.name, value: item.id });
            });

        });
        catRequest.type = [2];
        this.categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentTypes.push({ label: item.name, value: item.id });
            });
        });
        let request = new PagedCommonValueRequestDto();
        request.maxResultCount = 10000;
        request.type = 1;
        this.lookupTableService.getAllCommonValueByType(request).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentNumbers.push(item.value);
                this.filteredDocumentNumbers.push(item.value);
            });

        });
        request.type = 2;
        this.lookupTableService.getAllCommonValueByType(request).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.publishedOrganizations.push(item.value);
                this.filteredPublishedOrganizations.push(item.value);
            });
        });
        this.userService.getAllLeadershipApprovedPermission().subscribe(rs => {
            _.forEach(rs.items, user => {
                this.leaderShipApprovedUsers.push({ label: user.fullName, value: user.id });
            });
        });
        this.departmentService.getTreeDepartments(null, null).subscribe(response => {
          if (response.success) {
            this.departments = response.result.items;
          }
        });
    }

    initlizeFormGroup() {
        this.formGroup = new FormGroup({
            shortDescriptionCtrl: new FormControl('', Validators.required),
            documentFieldCtrl: new FormControl('', Validators.required),
            documentTypeCtrl: new FormControl('', Validators.required),
            yearCtrl: new FormControl('', Validators.required),
            storedPlaceCtrl: new FormControl(),
            selectedPublishedOrganizationCtrl: new FormControl('', Validators.required),
            createdByNameCtrl: new FormControl(this.form.createdById, Validators.required),
            urgentLevelCtrl: new FormControl('', Validators.required),
            secretLevelCtrl: new FormControl('', Validators.required),
            descriptionCtrl: new FormControl(),
            numberOfPagesCtrl: new FormControl(),
            departmentsCtrl: new FormControl()
        });
    }

    createCommonValue(type: number, value: string) {
        var dto = new CommonValueDto();
        dto.type = type;
        dto.value = value;
        this.commonService.create(dto).subscribe(rs => {
            if (type === 1) {
                this.filteredDocumentNumbers = [];
                this.filteredDocumentNumbers.push(rs.value);
                this.documentNumbers.push(rs.value);
            } else {
                this.filteredPublishedOrganizations = [];
                this.filteredPublishedOrganizations.push(rs.value);
                this.publishedOrganizations.push(rs.value);
            }
        });
    }
    filterDocumentNumbers(event) {
        this.filteredDocumentNumbers = [];
        this.filteredDocumentNumbers = this.documentNumbers.filter(item => item.toLowerCase().indexOf(event.query.toLowerCase()) >= 0);
        this.searchDocumentNumber = event.query;
    }
    filterPublishedOrganizations(event) {
        this.filteredPublishedOrganizations = [];
        this.filteredPublishedOrganizations = this.publishedOrganizations.filter(item => item.toLowerCase().indexOf(event.query.toLowerCase()) >= 0);
        this.searchPublishedOrganization = event.query;
    }
    createNewDocumentNumber(): void {
        if (this.searchDocumentNumber && this.searchDocumentNumber !== '') {
            this.createCommonValue(1, this.searchDocumentNumber);
        }
    }
    createNewPublishedOrganization(): void {
        if (this.searchPublishedOrganization && this.searchPublishedOrganization !== '') {
            this.createCommonValue(2, this.searchPublishedOrganization);
        }
    }
    save(): void {
        this.formGroup.markAllAsTouched();
        if (this.formGroup.invalid) return;
        if (this.relatedFiles.length <= 0) 
        { this.message.error("Chưa thêm file đính kèm!"); return;}

        this.saving = true;
        this.fileUpload = [...this.relatedFiles, ...this.attachFiles];
        this.fileService.upload(this.fileUpload, "departuredocument")
        .pipe(
            finalize(() => {
                console.log("form", this.form);
                
                this._service.create(this.form).pipe(
                    finalize(() => {
                        this.saving = false;
                        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới văn bản đi!')});
                    })
                ).subscribe(rs => {
                    this.form.id = rs.id;
                    this._router.navigate(["document/departure/unresolveddeparturedocument"]);
                })
            })
        )
        .subscribe(response => {
            response.items.forEach(item => {
                this.attachFiles.forEach(i => {
                    if(item.fileName == i.name)
                    this.form.attachedFiles.push(item)
                })
                this.relatedFiles.forEach(j => {
                    if(item.fileName == j.name)
                    this.form.files.push(item);
                })
            });
            this.clearSelectedFiles();
        })
    }
    chooseFiles(event) {
        const fileList: FileList = event.target.files;
        for (let i=0; i<fileList.length; i++){
            // Kiem tra neu trong tep file da them da co file thi khong them vao nua
            if(this.isAttachFile) {
                if (this.attachFiles.every(e => e.name !== fileList.item(i).name) && 
                    this.form.attachedFiles.every(e => e.fileName !== fileList.item(i).name)) 
                    { this.attachFiles.push(fileList.item(i)); console.log(this.attachFiles);}
            }
            else {
                if (this.relatedFiles.every(e => e.name !== fileList.item(i).name) && 
                this.form.files.every(e => e.fileName !== fileList.item(i).name))
                { this.relatedFiles.push(fileList.item(i)); console.log(this.relatedFiles); } 
            }
        }
    }
    
    focusChooseFiles() {
        this.isAttachFile = false;
        
        this.chooseFile.nativeElement.click();
    }
    focusChooseFilesAttach() {
        this.isAttachFile = true;
        this.chooseFileAttach.nativeElement.click();
    }

    clearSelectedFiles() {
        this.chooseFile.nativeElement.value = null;
        this.chooseFileAttach.nativeElement = null;
    }

    removeFile(index: number) {
        this.relatedFiles.splice(index, 1);
    }
    removeFileAttach(index: number) {
        this.attachFiles.splice(index, 1);
    }
    getAllEmployees() {
        this._service.getAllEmployees().subscribe(response => {
            _.forEach(response.result.items, user => {
                this.users.push({ label: user.fullName + '-' + user.positionName + '-' + user.departmentName, value: user.id });
        })});
    }
    removeAttachedFile(index: number) {
        this.form.files.splice(index, 1);
    }
    download(fileUrl: string) {
        this.fileService.download(fileUrl).pipe(finalize(() => {})).subscribe(response => {
        })
    }

    //for tree dropdown
    click(event) {
        if(this.firstTimeLoad) {
         this.dropDownEl = document.getElementById(this.id);
         document.body.appendChild(this.dropDownEl);
         this.firstTimeLoad = false;
        }
        this.showDropDown();
      }
      setPosition() {
        const el: Element = this.targetElement.nativeElement;
        const position = el.getBoundingClientRect();
        this.dropDownEl.style.top = position.height + position.top + 'px';
        this.dropDownEl.style.left = position.left + 'px';
        this.dropDownEl.style.width = position.width + 'px';
        this.dropDownEl.style.height = this.dropDownHeight + 'px';
      }
       showDropDown() {
         this.visible = !this.visible;
         this.setPosition();
       }
       nodeSelect(event) {
           console.log(event);
         this.visible = false;
         this.selection = event.node;
         this.selectionChange.emit(event.node);
         this.form.departmentId = event.node.data.id;
       }
       //end
        // NOte
       sendRequestDocument(): void {
        this.formGroup.markAllAsTouched();
        if (this.formGroup.invalid) return;
        // if (this.relatedFiles.length <= 0 && this.attachFiles.length <= 0) 
        // { this.message.error("Chưa thêm file đính kèm!"); return;}
        this.form.status = 0;
        this.fileUpload = [...this.relatedFiles, ...this.attachFiles];
        this.data.Files = this.fileUpload;
        this.data.form = this.form;

        let openModal;
        openModal = this._dialog.open(SendRequestDocumentComponent, {
            width: '700px',
            maxWidth: "800px",
            minHeight: '300px',
            data: this.data
        });
    
        openModal.afterClosed().subscribe((result) => {
          if (result) {
            this._router.navigate(["document/departure/unresolveddeparturedocument"]);
          }
        });
      }

}