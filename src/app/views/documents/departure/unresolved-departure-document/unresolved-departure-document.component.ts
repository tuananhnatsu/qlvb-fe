import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { DepartureDocumentForTableDto, DepartureDocumentPagedRequestDto } from '@app/models/documents/departure/departure-document/departure-document.model';
import { MatPaginator } from '@angular/material/paginator';
import { DepartureDocumentService } from '@app/services/documents/departure/departure-document.service';
import { finalize } from 'rxjs/operators';
import { CategoryService } from '@app/services/system-management/category.service';
import { PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { CreateDepartureDocumentComponent } from '../create-departure-document.component';
import { Router } from '@angular/router';
import { ViewOrEditDepartureDocumentComponent } from '../view-or-edit-departure-document/vieworeditdeparturedocument.component';
import { isNgTemplate } from '@angular/compiler';

@Component({
    selector: 'app-unresolveddeparturedocument',
    templateUrl: './unresolved-departure-document.component.html'
})
export class UnresolvedDepartureDocument extends PagedListingComponentBase<DepartureDocumentForTableDto>{
    @ViewChild("paginator") paginator: MatPaginator;
    columns = [
        { header: this.l("Cấp độ khẩn"), field: "urgentLevelName" },
        { header: this.l("Trích yếu"), field: "shortDescription" },
        // header: this.l("Nơi nhận văn bản"), field: "recievedDocument" }
    ]
    isStatisticLoading = false;
    numberOfSentDocuments = 0
    numberOfTextingDocuments = 0
    numberOfWaitingDocuments = 0
    isYearSearch = false;
    listdepartureDocuments: DepartureDocumentForTableDto[]
    input = new DepartureDocumentPagedRequestDto
    documentFields : any[] = [{label: "-- Lĩnh vực chuyên môn --", value: null}]
    documentTypes: any[] = [{label: "-- Loại văn bản --", value: null}]
    years: any[] = []
    private _dialog: MatDialog;
    
    constructor(
        injector: Injector, 
        private _departureDocumentService: DepartureDocumentService,
        private _categoryService: CategoryService,
        dialog: MatDialog,
        private router: Router,

    ) {
        super(injector);
        this._dialog = dialog;
        this.input.status = 0;
    }
    ngOnInit() {
        super.ngOnInit();
        this.getStatistic();
        this.getMasterData();    
    }
    protected list(
        request: DepartureDocumentPagedRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ) {
        request.keyword = this.input.keyword;
      request.status = this.input.status;
      request.year = this.isYearSearch ? this.input.year : []; 
      request.documentFieldId = this.input.documentFieldId;
      request.documentTypeId = this.input.documentTypeId;
      request.fromDate = this.isYearSearch ? null : this.input.fromDate;
      request.toDate = this.isYearSearch ? null : this.input.toDate;
      this._departureDocumentService
          .searchDocuments(request)
          .pipe(
              finalize(() => {
                  finishedCallback();
              })
          )
          .subscribe(response => {
            this.listdepartureDocuments = response.items;
            this.showPaging(response.items, pageNumber);
          })
    }
    protected delete() {}
    protected getStatistic() {
        this.isStatisticLoading = true;
        this._departureDocumentService.getUnresolvedDepartureDocumentsStatistic().pipe(finalize(() => {
            this.isStatisticLoading = false;
        })).subscribe(response => {
            if (response) {
                this.numberOfSentDocuments = response.result.numberOfSentDocuments
                this.numberOfTextingDocuments = response.result.numberOfTextingDocuments
                this.numberOfWaitingDocuments = response.result.numberOfWaitingDocuments
            }
        })
    }
    private getMasterData() {
        let currentYear = (new Date()).getFullYear();
        for (var y = -5; y < 5; y++) {
            this.years.push({ label: currentYear + y, value: currentYear + y });
        }
        let catRequest = new PagedCategoriesRequestDto();
        catRequest.skipCount = 0;
        catRequest.maxResultCount = 10000;
        catRequest.type = [1];
        this._categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentFields.push({ label: item.name, value: item.id });
            });

        });
        catRequest.type = [2];
        this._categoryService.getAllCategories(catRequest).subscribe(rs => {
            _.forEach(rs.items, item => {
                this.documentTypes.push({ label: item.name, value: item.id });
            });
        });
    }
    public checkPermissionToChinhSua(item) {   
        return item.creatorUserId == abp.session.userId || this.isGranted("Pages.Administration")
    }
    public checkPermission(item) {
        return item.createdById == abp.session.userId || this.isGranted("Pages.Administration")
    }
    refreshFilter(status: number | undefined) {
        this.input = new DepartureDocumentPagedRequestDto();
        this.input.status = status
    }
    changeYears(event: any) {
        this.isYearSearch = event.value.length > 0;       
    }
    
    showDetailDocumentToEdit(id?: number): void {
        this.router.navigate(['/document/departure/editdeparturedocument/' + id]);
    }

    showDetailDocument(id?: number, isEdit?: boolean, allowAction: boolean = true ): void {
        let openModal;
        openModal = this._dialog.open(ViewOrEditDepartureDocumentComponent, {
          width: "1200px",
          maxWidth: "1400px",
          data: {
            allowUpload: true,
            allowAction: allowAction,
            id: id,
            isEdit: isEdit
          }
        });
    
        openModal.afterClosed().subscribe(() => {
            this.refresh();
            this.getStatistic();
        });
      }
    goToCreateDepartureDocument() {
        this.router.navigate(['/document/departure/createdeparturedocument']);
    }
}