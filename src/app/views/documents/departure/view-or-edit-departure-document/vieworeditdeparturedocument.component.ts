import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '@app/services/file.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LookupTableService } from "@app/services/lookuptable.service";
import { LookupTableDto } from "@app/models/system-management/lookuptable.model";
import {ProcessDocumentView} from "@app/models/documents/arrived/arriveddocument.model";
import { MatDialog } from "@angular/material/dialog";
import { ConfirmReleaseDepartureDocumentComponent } from '../released/release-document-list/confirm-release-departure-document/confirm-release-departure-document.component';
import { DepartureDocumentService } from '../../../../services/documents/departure/departure-document.service';
import { DepartureDocumentForTableDto } from '../../../../models/documents/departure/departure-document/departure-document.model';
import { SendRequestDocumentComponent } from '../sendrequest/sendrequestdocument.component';
import { ApproveRequestDocumentComponent } from '../approve-request/approverequestdocument.component';
import { RejectRequestDocumentComponent } from '../reject-request/rejectrequestdocument.component';

@Component({
    selector: 'app-vieworeditdeparturedocument',
    templateUrl: './vieworeditdeparturedocument.component.html'
})
export class ViewOrEditDepartureDocumentComponent extends AppComponentBase implements OnInit {
    @ViewChild('chooseFile') chooseFile: ElementRef;
    relatedFiles: File[] = [];
    form: DepartureDocumentForTableDto = new DepartureDocumentForTableDto();
    private lookupTableAppService: LookupTableService;
    urgentLevels: Map<number, LookupTableDto> = new Map();
    secretLevels: Map<number, LookupTableDto> = new Map();
    fileView: File[] = []
    processDocumentView: ProcessDocumentView[];
    private _dialog: MatDialog;
    data: any
    isAttachFile : boolean;
    attachFiles: File[] = [];
    isFirstStep = true;

    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) _data: any,
        private _service: DepartureDocumentService,
        private route: ActivatedRoute,
        private fileService: FileService,
        private _dialogRef: MatDialogRef<ConfirmReleaseDepartureDocumentComponent>,
        lookupTableAppService: LookupTableService,
        dialog: MatDialog,
        private _router: Router,
    ) {
        super(injector);
        this.lookupTableAppService = lookupTableAppService;
        this._dialog = dialog;
        this.data = _data;
    }
    ngOnInit() {
        this.getDepartureDocumentById();
        if (!this.data.isEdit) {
            this.isFirstStep = false;
        }
    }

    columns: Array<any> = [
        { field: "fileName", header: this.l("Document") }
      ];

    columnsBp: Array<any> = [
        { field: "personForward", header: this.l("PersonForward") },
        { field: "dateForward", header: this.l("DateForward") },
        { field: "contentBp", header: this.l("ContentBp") }
      ];

    columnsReply: Array<any> = [
        { field: "personPd", header: this.l("PersonPd") },
        { field: "dateSend", header: this.l("DateSend") },
        { field: "contentPd", header: this.l("ContentPd") },
        { field: "filePd", header: this.l("FilePd") }
      ];

      chooseFiles(event) {
        const fileList: FileList = event.target.files;
        for (let i=0; i<fileList.length; i++){
            // Kiem tra neu trong tep file da them da co file thi khong them vao nua
            if(this.isAttachFile == true ) {
                if (this.attachFiles.every(e => e.name !== fileList.item(i).name)) 
                    { this.attachFiles.push(fileList.item(i)); 
                    console.log(this.attachFiles);}
                    this.uploadFile(this.attachFiles, "departuredocument");
            }
            else {
                if(this.isAttachFile == false) {
                if (this.relatedFiles.every(e => e.name !== fileList.item(i).name))
                { this.relatedFiles.push(fileList.item(i)); 
                console.log(this.relatedFiles); } 
                this.uploadFile(this.relatedFiles, "relateddeparturedocument");
                }                
            }
        }
    }
    
    focusChooseFiles() {
        this.isAttachFile = true;
        this.chooseFile.nativeElement.click();
    }
    focusChooseFilesAttach() {
        this.isAttachFile = false;
        this.chooseFile.nativeElement.click();
    }

    download(fileUrl: string) {
        this.fileService.download(fileUrl).pipe(finalize(() => {})).subscribe(response => {
        })
    }

    uploadFile(files, folderName) {
        if ( folderName == "departuredocument")
        {this.fileService.upload(files, folderName).subscribe(response => {
            this.form.files = response.items;
            this.getDepartureDocumentById();      
        })}
        if ( folderName == "relateddeparturedocument")
        {
        this.fileService.upload(files, folderName).subscribe(response => {
                this.form.attachedFiles = response.items;
                this.getDepartureDocumentById();      
            })
        }     
    }

    assignDepartureDocumentUser(){
        this.close();
        this._router.navigate(["/document/departure/assignUser/"+ this.data.id]);

    }
    releaseDocument(id:number){
        let openModal;
        openModal = this._dialog.open(ConfirmReleaseDepartureDocumentComponent, {
            width: "800px",
            maxWidth: "1000px",
            data: {
            // allowUpload: true,
            // allowAction: true,
            // allowRelease:true,
                id: id
            }
        });
    
        openModal.afterClosed().subscribe((result) => {
            if (result) {
            //this.refresh();
            this.data.allowRelease=false;
            this._dialogRef.close(this.data);
            }
        });
          
    }
    closeReleaseDocument(){
        this._dialogRef.close();
    }
    
    close(){
        this._dialogRef.close();
    }
    
    getDepartureDocumentById() {
        this._service.getDepartureDocumentById(this.data.id).subscribe(response => {
            if (response.success) {
                this.form = response.result;
            }
        });
    }

    sendRequestDocument(): void {
        let openModal;
        openModal = this._dialog.open(SendRequestDocumentComponent, {
            width: '700px',
            maxWidth: "800px",
            minHeight: '300px',
            data: {
                id: this.data.id,
                isFirstStep: this.isFirstStep,
                isDraft: true
            }
        });
    
        openModal.afterClosed().subscribe(() => {
              this.close();
              this._messageService.add({
                severity: "success",
                summary: "Thành công",
                detail: "Gửi phê duyệt thành công!"
            })
        });
      }
      
    approveDocument(): void {
        let openModal;
        openModal = this._dialog.open(ApproveRequestDocumentComponent, {
            width: '700px',
            maxWidth: "800px",
            minHeight: '300px',
            data: { id: this.data.id }
        });
    
        openModal.afterClosed().subscribe(() => {
            this.close();
            this._messageService.add({
                severity: "success",
                summary: "Thành công",
                detail: "Phê duyệt thành công!"
            })
        });
      }
      
    rejecttDocument(): void {
        let openModal;
        openModal = this._dialog.open(RejectRequestDocumentComponent, {
            width: '700px',
            maxWidth: "800px",
            minHeight: '300px',
            data: { id: this.data.id }
        });
    
        openModal.afterClosed().subscribe(() => {
            this.close();
            this._messageService.add({
                severity: "success",
                summary: "Thành công",
                detail: "Từ chối phê duyệt thành công!"
            })
        });
      }
      
}