import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { ArrivedDocumentDto } from '@app/models/documents/arrived/arriveddocument.model';
import { ProcessDocumentService } from '@app/services/documents/arrived/process-document/process-document.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '@app/services/file.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LookupTableService } from "@app/services/lookuptable.service";
import { LookupTableDto } from "@app/models/system-management/lookuptable.model";
import {ProcessDocumentView} from "@app/models/documents/arrived/arriveddocument.model";
import { MatDialog } from "@angular/material/dialog";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReleasedDepartureDocumentDto } from '@app/models/documents/departure/departure-document/departure-document.model';
import * as moment from 'moment';
import { DepartureDocumentService } from '@app/services/documents/departure/departure-document.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'confirm-release-departure-document',
  templateUrl: './confirm-release-departure-document.component.html',
  styleUrls: ['./confirm-release-departure-document.component.css']
})
export class ConfirmReleaseDepartureDocumentComponent extends AppComponentBase implements OnInit {
  formGroup: FormGroup;
  saving = false;
  data: any;
  model = new ReleasedDepartureDocumentDto;

  currentDate = new Date();
  //For inputMask
  maskPrefix: string;
  mask: string;
  DEFAULT_NUM_RELEASED : number =3;

  constructor(
    injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) data: number,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
   private _departureDocumentService : DepartureDocumentService,
    
    private _dialogRef: MatDialogRef<ConfirmReleaseDepartureDocumentComponent>,

    dialog: MatDialog,
) {
    super(injector);
    this.data = data;

}
  ngOnInit(): void {
    this.formGroup = new FormGroup({
      releasedSymbolCtrl: new FormControl('', Validators.required),
      releasedNumberCtrl: new FormControl(),
      releasedDateCtrl: new FormControl(),
      releasedReceiveOrgCtrl: new FormControl(),
      releasedSignedUserCtrl: new FormControl(),
      releasedNumOrgCtrl: new FormControl(),
  })
    this.initialize();
  }

  private initialize(): void {
   

  this._departureDocumentService.getNextReleasedNumber(this.data.id).
    subscribe(response => {
      if (response) {
        this.maskPrefix = response.result.prefix;
        this.mask = response.result.prefix + this.formatPrefixMask(this.DEFAULT_NUM_RELEASED);
        this.model = new ReleasedDepartureDocumentDto({
          releasedDate : moment(new Date()),
          departureDocumentId : this.data.id,
          //releasedSymbol : response.result.prefix + this.formatSurfix( response.result.number, this.DEFAULT_NUM_RELEASED)
        });
        setTimeout(() => {
          const maskedInputControl = this.formGroup.get('releasedSymbolCtrl') as FormControl;
          this.model.releasedSymbol=response.result.prefix + this.formatSurfix( response.result.number, this.DEFAULT_NUM_RELEASED);
          maskedInputControl.setValue(this.model.releasedSymbol);
          // const releasedDateCtrl = this.formGroup.get('releasedDateCtrl') as FormControl;
          // releasedDateCtrl.setValue(this.model.releasedDate);
          console.log(this.model);
          });      
      }
       // console.log(this.model);
    });

  }

  formatPrefixMask(numberSymbol: number){
      var result="";
      for(let i=0;i<numberSymbol;i++){
        result +="9";
      }
    return result;
  }
  formatSurfix(nextNumber:number, numberSymbol: number){
    var result = nextNumber.toString();
    for(let i = result.length ;i<numberSymbol;i++){
      result = "0" + result;
    }
  return result;
}

  save(): void {
    if (this.formGroup.invalid) return;

    this.saving = true;
    this.model.symbolValue = parseInt(this.model.releasedSymbol.substr(this.maskPrefix.length));

    this._departureDocumentService
      .confirmReleaseDeparture(this.model)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      
      .subscribe(response => {
        if(response=="confirmed"){
          //need to confirm
          this.notify.info(this.l('SavedSuccessfully'));
          this.close(true);
        }else if(response =="needToConfirm"){
          this.confirmationService.confirm({
            message: 'Có số kí hiệu trùng! Bạn có muốn tiếp tục?',
            accept: () => {
              this.processForceConfirm();
            }
        });
           
        }
      
      });
  }

  processForceConfirm(): void{
    this.saving = true;
     
    this._departureDocumentService
      .confirmForcedReleaseDeparture(this.model)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      
      .subscribe(response => {
        if(response=="confirmed"){
          //need to confirm
          this.notify.info(this.l('SavedSuccessfully'));
          this.close(true);
        }
      
      });
  }
  close(result: any): void {
    this._dialogRef.close(result);
  }

}
