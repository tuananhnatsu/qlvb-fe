import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmReleaseDepartureDocumentComponent } from './confirm-release-departure-document.component';

describe('ConfirmReleaseDepartureDocumentComponent', () => {
  let component: ConfirmReleaseDepartureDocumentComponent;
  let fixture: ComponentFixture<ConfirmReleaseDepartureDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmReleaseDepartureDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmReleaseDepartureDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
