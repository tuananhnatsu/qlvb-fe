import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseDocumentListComponent } from './release-document-list.component';

describe('ReleaseDocumentListComponent', () => {
  let component: ReleaseDocumentListComponent;
  let fixture: ComponentFixture<ReleaseDocumentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleaseDocumentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseDocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
