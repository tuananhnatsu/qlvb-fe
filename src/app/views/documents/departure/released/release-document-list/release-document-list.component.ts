import { Component, Injector, OnInit } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { PagedListingComponentBase, PagedRequestDto } from "@shared/paged-listing-component-base";
import { MatPaginator } from "@angular/material/paginator";
import { MatDialog } from "@angular/material/dialog";
import { finalize } from "rxjs/operators";
import { LookupTableService } from "@app/services/lookuptable.service";
import { LookupTableDto } from "@app/models/system-management/lookuptable.model";
import * as _ from "lodash";
import { Router } from '@angular/router';
import { ProcessDocumentService } from '@app/services/documents/arrived/process-document/process-document.service';
import { DepartureDocumentService } from '@app/services/documents/departure/departure-document.service';
import {  DepartureDocumentForTableDto, DepartureDocumentPagedRequestDto, DepartureDocumentStatusEnum } from '@app/models/documents/departure/departure-document/departure-document.model';
import { isBuffer } from 'lodash';
import { CategoryService } from '@app/services/system-management/category.service';
import { PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import { ViewOrEditDepartureDocumentComponent } from '../../view-or-edit-departure-document/vieworeditdeparturedocument.component';
@Component({
  selector: 'release-document-list',
  templateUrl: './release-document-list.component.html',
  styleUrls: ['./release-document-list.component.css']
})
export class ReleaseDocumentListComponent  extends PagedListingComponentBase<DepartureDocumentForTableDto> implements OnInit{
  isStatisticLoading : boolean = true;
  totalWaitingForRelease: number = 0;
  totalReleased: number = 0;
  isYearSearch = false;

  years: any[] = [];

  documentFields: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  documentTypes: any[] = [
    { label: "--Tất cả--", value: 0 },
  ];
  isDisableChooseYear: boolean = false;
  isDisableChooseDate: boolean = false;
  isDisableChooseFromDate: boolean = false;
  isDisableChooseToDate: boolean = false;

  public paginator: MatPaginator;
  private service: ProcessDocumentService;
  private lookupTableAppService: LookupTableService;
  input = new DepartureDocumentPagedRequestDto;


  private _dialog: MatDialog;

  isAdvancedSearch: boolean = false;
  urgentLevels: Map<number, LookupTableDto> = new Map();
  roles: Map<number, LookupTableDto> = new Map();

  columns = [
    { header: this.l("Cấp độ khẩn"), field: "urgentLevelName" },
    { header: this.l("Trích yếu"), field: "shortDescription" },
    { header: this.l("Ngày phê duyệt"), field: "appliedDate" }
  ];
  listdepartureDocuments: DepartureDocumentForTableDto[];

  constructor(injector: Injector,
      private _departureDocumentService: DepartureDocumentService,
      dialog: MatDialog,
      private _categoryService: CategoryService,
      lookupTableAppService: LookupTableService,
      private router: Router,) { 
      super(injector);
      this.input.status = DepartureDocumentStatusEnum.Confirmed;
      this._dialog = dialog;
      this.lookupTableAppService = lookupTableAppService;
    }

  ngOnInit(): void {
    super.ngOnInit();
    this.initialize();
    this.getStatistic();
  }
  private initialize(): void {
    this.getMasterData();
  }

  protected getStatistic() {
    this.isTableLoading = true;
    this._departureDocumentService.getReleaseDepartureDocumentStatistic().pipe(finalize(() => {
        this.isStatisticLoading = false;
    })).subscribe(response => {
        if (response) {
            this.totalReleased = response.result.numberReleased;
            this.totalWaitingForRelease = response.result.numberWaitingForRelease;
        }
    })
}
  private getMasterData() {
    let currentYear = new Date().getFullYear();
    for (var y = -5; y < 5; y++) {
      this.years.push({ label: currentYear + y, value: currentYear + y });
    }
    let catRequest = new PagedCategoriesRequestDto();
    catRequest.skipCount = 0;
    catRequest.maxResultCount = 10000;
    catRequest.type = [1];
    this._categoryService.getAllCategories(catRequest).subscribe(rs => {
        _.forEach(rs.items, item => {
            this.documentFields.push({ label: item.name, value: item.id });
        });

    });
    catRequest.type = [2];
    this._categoryService.getAllCategories(catRequest).subscribe(rs => {
        _.forEach(rs.items, item => {
            this.documentTypes.push({ label: item.name, value: item.id });
        });
    });
  }

  protected list(request: DepartureDocumentPagedRequestDto, pageNumber: number, finishedCallback: Function): void {
      //request.keyword = this.input.keyword;
      request.status = this.input.status;
      request.year = this.isYearSearch ? this.input.year : []; 
      request.documentFieldId = this.input.documentFieldId;
      request.documentTypeId = this.input.documentTypeId;
      request.fromDate = this.isYearSearch ? null : this.input.fromDate;
      request.toDate = this.isYearSearch ? null : this.input.toDate;
      this._departureDocumentService
          .searchReleasedDepartureDocument(request)
          .pipe(
              finalize(() => {
                  finishedCallback();
              })
          )
          .subscribe(response => {
            this.listdepartureDocuments = response.items;
            this.showPaging(response, pageNumber);
          })
  }
  protected delete(entity: DepartureDocumentForTableDto): void {
    //throw new Error("Method not implemented.");
  }
  changeYears(event: any) {
    this.isYearSearch = event.value.length > 0;       
  }
  refreshFilter(status: number | undefined) {
    this.input = new DepartureDocumentPagedRequestDto();
    this.input.status = status
  }

  search(value){
    if (value === "totalWaitingForRelease"){
      this.input.status = DepartureDocumentStatusEnum.Confirmed;
      this.refresh();
    }else if(value =="totalReleased"){
      this.input.status = DepartureDocumentStatusEnum.Release;
      this.refresh();
    }
  }

  showDetailDocument(id?: number): void {
    let openModal;
    openModal = this._dialog.open(ViewOrEditDepartureDocumentComponent, {
      width: "1200px",
      maxWidth: "1400px",
      data: {
        showReleaseDetail:true,
        allowRelease:this.input.status == DepartureDocumentStatusEnum.Confirmed,
        id: id
      }
    });

    openModal.afterClosed().subscribe((result) => {
      if (result) {
        this.refresh();
      }
    });
  }


}
