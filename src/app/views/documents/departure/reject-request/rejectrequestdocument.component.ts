import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartureDocumentForTableDto, ProcessApproveDepartureDocumentDto } from '../../../../models/documents/departure/departure-document/departure-document.model';
import { DepartureDocumentService } from '../../../../services/documents/departure/departure-document.service';
import { FileService } from '@app/services/file.service';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './rejectrequestdocument.component.html',
    styles: [
        `
        mat-form-field {
          width: 100%;
        }
        mat-checkbox {
          padding-bottom: 5px;
        }
        .actionButton {
            float: right;
        }
        .btn-action {
            background-color: #3e4e62;
            color: #fff;
            font-size: 13px;
            float: right;
            margin-right: 10px;
        }
      `
    ]
})
export class RejectRequestDocumentComponent extends AppComponentBase implements OnInit {
    form: ProcessApproveDepartureDocumentDto = new ProcessApproveDepartureDocumentDto();

    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) private data,
        private _service: DepartureDocumentService,
        private _dialogRef: MatDialogRef<RejectRequestDocumentComponent>,
        private fileService: FileService

        ) {
        super(injector);
    }

    ngOnInit() {
        this.form.departureDocumentId = this.data.id;
    }

    save() {
        this._service.rejectRequest(this.form).subscribe(rs => {
            this.close();
        })
    }

    close() {
      this._dialogRef.close();
    }

}