import { Component, Injector, OnInit, ViewChild, ElementRef, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartureDocumentForTableDto, ProcessApproveDepartureDocumentDto } from '../../../../models/documents/departure/departure-document/departure-document.model';
import { DepartureDocumentService } from '../../../../services/documents/departure/departure-document.service';
import { FileService } from '@app/services/file.service';
import { finalize } from 'rxjs/operators';
import { UserSysService } from '@app/services/system-management/user.service';

@Component({
    templateUrl: './sendrequestdocument.component.html',
    styles: [
        `
        mat-form-field {
          width: 100%;
        }
        mat-checkbox {
          padding-bottom: 5px;
        }
        .actionButton {
            float: right;
        }
        .btn-action {
            background-color: #3e4e62;
            color: #fff;
            font-size: 13px;
            float: right;
            margin-right: 10px;
        }
      `
    ]
})
export class SendRequestDocumentComponent extends AppComponentBase implements OnInit {
    form: ProcessApproveDepartureDocumentDto = new ProcessApproveDepartureDocumentDto();
    formGroup: FormGroup;
    dateNow: Date = new Date();
    managers: any[] = [];
    users: any[] = [];
    data = {Files: [], form: new DepartureDocumentForTableDto()};
    isExpired = false;
    isFirstStep = true;
    isDraft = true;
    
    managerDepartments: any[] = [
        {label:"Phòng", value: 1},
        {label:"Đơn vị", value: 2}
    ];

    constructor(
        injector: Injector,
        @Optional() @Inject(MAT_DIALOG_DATA) private _data,
        private _service: DepartureDocumentService,
        private _userService: UserSysService, 
        private _dialogRef: MatDialogRef<SendRequestDocumentComponent>,
        private fileService: FileService

        ) {
        super(injector);
        this.formGroup = new FormGroup({
            departmentCtrl: new FormControl(),
            managerCtrl: new FormControl(),
            unlimitedCtrl: new FormControl(),
            deadlineCtrl: new FormControl(),
            contentCtrl: new FormControl(),            
        })
    }

    ngOnInit() {
        this.getAllEmployees();
        this.data = this._data;
        this.isFirstStep = this._data.isFirstStep;
        this.isDraft = this._data.isDraft;
        this.form.isExpried = false;
    }

    handleChangeCheckbox(event) {
        this.form.isExpried = event.checked;
        if(event.checked) {
            this.isExpired = true;
        } else {
            this.isExpired = false;
        }
    }
    
    getAllEmployees() {
        this._userService.getAllLeadershipApprovedPermission().subscribe(response => {
            _.forEach(response.items, user => {                
                this.users.push({ label: user.fullName, value: user.id });
        })});
    }

    save(): void {
        if (this.data.Files.length > 0) {
            this.fileService.upload(this.data.Files, "departuredocument")
            .pipe(
                finalize(() => {
                    this.data.form.status = 1;
                    this._service.create(this.data.form).pipe(
                    ).subscribe(rs => {
                        this.form.departureDocumentId = rs.id;
                        this.sendRequest();
                        this.close(true);
                    })
                })
            )
            .subscribe(response => {
                response.items.forEach(item => {
                    this.data.Files.forEach(i => {
                        if(item.fileName == i.name)
                        this.data.form.attachedFiles.push(item)
                    })
                    this.data.Files.forEach(j => {
                        if(item.fileName == j.name)
                        this.data.form.files.push(item);
                    })
                });
            })
        } else{
            this.data.form.status = 1;
                    this._service.create(this.data.form).pipe(
                    ).subscribe(rs => {
                        this.form.departureDocumentId = rs.id;
                        this.sendRequest();
                        this.close(true);
                    })
        }
        
    }
    
    sendRequest() {
        this.form.status = 1;
        if (this.isDraft) {
            this.form.departureDocumentId = this._data.form.id;
        }
        this._service.sendRequest(this.form).subscribe(rs => {
            this.close(true);
        })
    }

    close(result: boolean) {
      this._dialogRef.close(result);
    }

}