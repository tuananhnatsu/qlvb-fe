import { NgModule } from '@angular/core';
import { DocumentRouting } from './document.routing';
import { SharedModule } from '@shared/shared.module';
import {ArrivedFolderComponent} from './arrived/folder/arrivedfolder.component'
import {CreateArrivedFolderDialogComponent} from './arrived/folder/modals/create-arrived-folder/create-arrived-folder-dialog.component'
import {EditArrivedFolderDialogComponent} from './arrived/folder/modals/edit-arrived-folder/edit-arrived-folder-dialog.component'
import {ArrivedOfficeAgentComponent} from './arrived/office-agent/officeagent.component';
import {CreateOrEditArrivedDocumentComponent} from './arrived/office-agent/create-or-edit-arrived-document/createoreditarriveddocument.component';
import {ProcessArrivedDocumentComponent} from './arrived/process-document/processdocumentlist.component';
import { AssignDocumentUserComponent } from './arrived/assign-document-user/assign-document-user/assign-document-user.component';
import { CreateGroupUserComponent } from './arrived/assign-document-user/create-group-user/create-group-user/create-group-user.component';
import { ConfirmAssignDocumentComponent } from './arrived/assign-document-user/confirm-assign-document/confirm-assign-document/confirm-assign-document.component';
import { ReleaseDocumentListComponent } from './departure/released/release-document-list/release-document-list.component';
import { ViewDetailDocumentComponent} from './arrived/process-document/view-detail-document/viewdetaildocument.component';
import { CompleteDocumentComponent } from './arrived/process-document/view-detail-document/completedocument/completedocument.component';

import { CreateDepartureDocumentComponent } from './departure/create-departure-document.component';
import { SendRequestDocumentComponent } from './departure/sendrequest/sendrequestdocument.component';
import { UnresolvedDepartureDocument } from './departure/unresolved-departure-document/unresolved-departure-document.component';
import { ResolvedDepartureDocument } from './departure/resolved-departure-document/resolved-departure-document.component';
import { AssignDepartureDocumentUserComponent } from './departure/assign-departure-document-user/assign-departure-document-user.component';
import { ViewOrEditDepartureDocumentComponent } from './departure/view-or-edit-departure-document/vieworeditdeparturedocument.component';
import { ConfirmReleaseDepartureDocumentComponent } from './departure/released/release-document-list/confirm-release-departure-document/confirm-release-departure-document.component';
import { CoordinateResponseComponent } from './arrived/process-document/view-detail-document/coordinate-response/coordinate-response.component';
import { ApproveRequestDocumentComponent } from './departure/approve-request/approverequestdocument.component';
import { RejectRequestDocumentComponent } from './departure/reject-request/rejectrequestdocument.component';
import { ArrivedDocumentsHistoryComponent } from './arrived/arrived-documents-history/arrived-documents-history.component';
import { DepartureDocumentsHistoryComponent } from './departure/departure-documents-history/departure-documents-history.component';
import { EditDepartureDocumentComponent } from './departure/editdeparturedocument/edit-departure-document.component';
import { ViewFileDocumentComponent } from './arrived/process-document/view-detail-document/view-file/viewfiledocument.component';

@NgModule({
    declarations: [
        ArrivedFolderComponent,
        CreateArrivedFolderDialogComponent,
        EditArrivedFolderDialogComponent,
        ArrivedOfficeAgentComponent,
        CreateOrEditArrivedDocumentComponent,
        ProcessArrivedDocumentComponent,
        AssignDocumentUserComponent,
        CreateGroupUserComponent,
        ConfirmAssignDocumentComponent,
        ViewDetailDocumentComponent,
        ReleaseDocumentListComponent,
        CreateDepartureDocumentComponent,
        SendRequestDocumentComponent,
        CompleteDocumentComponent,
        CoordinateResponseComponent,
        UnresolvedDepartureDocument,
        ResolvedDepartureDocument,
        AssignDepartureDocumentUserComponent,
        ViewOrEditDepartureDocumentComponent,
        EditDepartureDocumentComponent,
        ConfirmReleaseDepartureDocumentComponent,
        ApproveRequestDocumentComponent,
        RejectRequestDocumentComponent,
        ArrivedDocumentsHistoryComponent,
        EditDepartureDocumentComponent,
        ViewFileDocumentComponent,
        DepartureDocumentsHistoryComponent
    ],
    imports : [
        DocumentRouting,
        SharedModule
    ],
    bootstrap: [
        
    ]
  })
  export class DocumentModule {}