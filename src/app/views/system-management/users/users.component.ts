import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { PagedListingComponentBase, PagedRequestDto } from '@shared/paged-listing-component-base';
import { UserDto, UserServiceProxy, RoleServiceProxy } from '@shared/service-proxies/service-proxies';
import { UserSysService } from '../../../services/system-management/user.service';
import { finalize } from 'rxjs/operators';
import { DepartmentService } from '@app/services/system-management/department.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateUsersComponent } from './modals/create-users/create-users.component';
import { EditUsersComponent } from './modals/edit-users/edit-users.component';
import { MessageService, ConfirmationService, TreeNode } from 'primeng/api';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { ResetPasswordComponent } from './modals/reset-password/reset-password.component';
import { MatPaginator } from '@angular/material/paginator';
import { CategoryService } from '@app/services/system-management/category.service';
import { PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import * as _ from 'lodash';

export class UserPagedRequestDto extends PagedRequestDto{
  keyword: string = ""
  isActive: boolean
  listDepartmentId: number[]
  listRoleId: number[]
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent extends PagedListingComponentBase<UserDto>{
  @ViewChild("paginator") paginator: MatPaginator;
  input = new UserPagedRequestDto;
  cols = [
    {
      field: "fullName",
      header: this.l("Họ và tên")
    },
    {
      field: "userName",
      header: this.l("Tài khoản")
    },
    {
      field: "gender",
      header: this.l("Giới tính")
    },
    {
      field: "emailAddress",
      header: this.l("Email")
    },
    {
      field: "phoneNumber",
      header: this.l("Điện thoại")
    },
    {
      field: "departmentName",
      header: this.l("Phòng ban")
    },
    {
      field: "roleNames",
      header: this.l("Vai trò")
    },
    {
      field: "isActive",
      header: this.l("Trạng thái")
    },
    {
      field: "lastLoginTime",
      header: this.l("Lần đăng nhập cuối cùng")
    },
    {
      field: "act",
      header: this.l("Thao tác")
    }
  ];

  values = [
    { label: '-- Phòng ban --', value: null },
    { label: '-- Cay phong ban --', value: 0 }
  ]
  isOpenRow: any
  users = [];
  departmentOptions = [];
  roleOptions = [];
  positionOptions = [];
  statusOptions = [{
    label: this.l("-- Trạng thái --"),
    value: null
  }, {
    label: this.l("Đã kích hoạt"),
    value: true
  }, {
    label: this.l("Đã khóa"),
    value: false
  }];
  selection: TreeNode

  constructor(
    injector: Injector,
    private _userService: UserSysService,
    private _userServiceProxy: UserServiceProxy,
    private _departmentService: DepartmentService,
    private categoryService: CategoryService,
    private _roleService: RoleServiceProxy,
    private dialog: MatDialog
  ) {
    super(injector)
    this.getMasterData()
   }

  protected list(
    request: UserPagedRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
      request.keyword = this.input.keyword;
      request.listDepartmentId = this.input.listDepartmentId;
      request.isActive = this.input.isActive;
      this._userService
          .searchUsers(request)
          .pipe(
              finalize(() => {
                  finishedCallback();
              })
          )
          .subscribe(response => {
              if (response.success) {
                this.users = response.result['items']
                this.showPaging(response.result, pageNumber);
              }
          })
  }

  protected delete(): void {
  }

  private getMasterData() {
    this._departmentService.getTreeDepartments().subscribe(response => {
      if (response.success) {
        this.departmentOptions = response.result.items;
      }
    })
    this._roleService.getAll("user", 0, 1000).subscribe(response => {
      if (response) {
        this.roleOptions = response.items.map(m => {return {
          label: this.l(m.name),
          value: m.normalizedName        }
        })
      }
    })
    let catRequest = new PagedCategoriesRequestDto();
    catRequest.skipCount = 0;
    catRequest.maxResultCount = 10000;
    catRequest.type = [4];
    this.categoryService.getAllCategories(catRequest).subscribe(rs => {
      _.forEach(rs.items, item => {
        this.positionOptions.push({ label: item.name, value: item.id });
      });

    });
  }

  openModalCreateUsers() {
    this.dialog.open(CreateUsersComponent, {
      maxWidth: "800px",
      data: {
        departmentOptions: this.departmentOptions,
        roleOptions: this.roleOptions,
        positionOptions: this.positionOptions
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this.refresh()
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới người dùng')});
      }
    })
  }

  openModalEditUsers(rowData) {
    this.dialog.open(EditUsersComponent, {
      maxWidth: "800px",
      data: {
        user: rowData,
        departmentOptions: this.departmentOptions,
        roleOptions: this.roleOptions,
        positionOptions: this.positionOptions
      }
      // disableClose: true
    }).afterClosed().subscribe(response => {
      if (response) {
        this.refresh()
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Thay đổi thông tin người dùng')});
      }
    })
  }

  changeActive(isActive: boolean, rowData: UserDto) {
    this._confirmService.confirm({
      message: this.l(`Xác nhận ${isActive ? 'kích hoạt' : 'khóa'} tài khoản ${rowData.userName}?`),
      header: this.l("Xác nhận"),
      accept: () => {
        let obj = <UserDto>{...rowData};
        obj.isActive = isActive;
        this._userServiceProxy.update(obj).subscribe(response => {
          if (response) {
            this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l(`${isActive ? 'Kích hoạt' : 'Khóa'} tài khoản`)});
            this.refresh()
          }
        })
      }
    })
  }

  openModalChangePassword(rowData: UserDto) {
    this.dialog.open(ChangePasswordComponent, {
      maxWidth: "400px",
      data: {
        userId : rowData.id
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l(`Đổi mật khẩu`)});
      }
    })
  }

  openModalResetPassword(rowData: UserDto) {
    this.dialog.open(ResetPasswordComponent, {
      maxWidth: "400px",
      data: {
        userId : rowData.id
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l(`Reset mật khẩu`)});
      }
    })
  }

  nodeSelect(event) {
    this.input.listDepartmentId = [event.node.data.id]
    this.getDataPage(0);
  }
  changeDepartment(event) {
    if (event.value == null) {
      this.selection = undefined;
      this.input.listDepartmentId = []
      this.getDataPage(0);
    }
  }
}
