import { Component, OnInit, Injector, Inject, Optional, ɵEMPTY_ARRAY } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { TreeNode } from 'primeng/api';
import * as _ from 'lodash';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent extends AppComponentBase implements OnInit {

  formGroup: FormGroup

  user: UserDto
  departmentOptions: TreeNode[] = [];
  positionOptions = [];
  roleOptions = [];
  roleValue: string;
  values = [
    { label: '-- Phòng ban --', value: null },
    { label: '-- Cay phong ban --', value: 0 }
  ]
  selection: TreeNode;
  isHavingDepartment = null

  constructor(
    injector: Injector,
    private dialogRef: MatDialogRef<EditUsersComponent>,
    private _userService: UserServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any
  ) {
    super(injector);
    if (data.user.departmentId && data.user.departmentId > 0) {
      this.isHavingDepartment = 0;
      const findSelection = (arr: TreeNode[]) => {
        if (this.selection) return;
        arr.forEach(item => {
          if (item.data.id == data.user.departmentId) {
            this.selection = item;
            return
          } else {
            findSelection(item.children);
          }
        })
      }
      findSelection(data.departmentOptions);
    }
    this.user = <UserDto>{...data.user}
    this.user.roleNames = this.user.roleNames.map(m => m.toLocaleUpperCase());
    // this.roleValue = this.user.roleNames[0] ? this.user.roleNames[0].toLocaleUpperCase() : null;
    // console.log(this.roleValue)
    this.departmentOptions = [...data.departmentOptions]
    this.roleOptions = [...data.roleOptions]
    this.positionOptions = [{
      label: this.l("-- Cấp bậc --"),
      value: null
    }, ...data.positionOptions]
    this.formGroup = new FormGroup({
      nameCtrl: new FormControl('',Validators.required),
      surnameCtrl: new FormControl('',Validators.required),
      departmentCtrl: new FormControl(),
      genderMaleCtrl: new FormControl(),
      genderFemaleCtrl: new FormControl(),
      emailCtrl: new FormControl(),
      phoneCtrl: new FormControl(),
      roleCtrl: new FormControl(),
      positionCtrl: new FormControl(),
      userCtrl: new FormControl({value: '', disabled: true}, Validators.required)
    })
   }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) return;
    this._userService.update(this.user).subscribe(response => {
      if (response) { this.dialogRef.close(true); }
    })
  }
  nodeSelect(event) {
    this.user.departmentId = event.node.data.id;
  }
  changeDepartment(event) {
    if (event.value == null) {
      this.selection = undefined;
      this.user.departmentId = null
    }
  }

}
