import { Component, OnInit, Injector, Inject, Optional } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppComponentBase } from '@shared/app-component-base';
import { ResetPasswordDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent extends AppComponentBase implements OnInit {

  isLoading: boolean
  resetPassword = new ResetPasswordDto
  formGroup: FormGroup
  constructor(
    injector: Injector,
    private dialogRef: MatDialogRef<ResetPasswordComponent>,
    private _userService: UserServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) data
  ) {
    super(injector)
    if (data) {
      if (data.userId) {
        this.resetPassword.userId = data.userId
      }
    }
   }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      adminPasswordCtrl: new FormControl('', Validators.required),
      newPasswordCtrl: new FormControl({value: '', disabled: true}, Validators.required)
    })
    this.resetPassword.newPassword = Math.random()
    .toString(36)
    .substr(2, 10);
  }

  save(): void {
    this.isLoading = true;
    this._userService
      .resetPassword(this.resetPassword)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(() => {
        this.dialogRef.close(true);
      });
  }
  cancel() {
    this.dialogRef.close()
  }

}
