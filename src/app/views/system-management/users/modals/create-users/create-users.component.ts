import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CreateUserDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { TreeNode } from 'primeng/api';

@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.css']
})
export class CreateUsersComponent extends AppComponentBase implements OnInit {

  formGroup: FormGroup
  departmentOptions: TreeNode[] = [];
  positionOptions = [];
  roleOptions = [];
  user = new CreateUserDto
  values = [
    { label: '-- Phòng ban --', value: null },
    { label: '-- Cay phong ban --', value: 0 }
  ]
  selection: TreeNode;

  roleValue: string;

  constructor(
    injector: Injector,
    private dialogRef: MatDialogRef<CreateUsersComponent>,
    private _userService: UserServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any
  ) {
    super(injector)
    this.departmentOptions = [...data.departmentOptions]
    this.roleOptions = [...data.roleOptions]
    this.positionOptions = [{
      label: this.l("-- Cấp bậc --"),
      value: null
    }, ...data.positionOptions]
    this.formGroup = new FormGroup({
      nameCtrl: new FormControl('',Validators.required),
      surnameCtrl: new FormControl('',Validators.required),
      departmentCtrl: new FormControl(),
      genderMaleCtrl: new FormControl(),
      genderFemaleCtrl: new FormControl(),
      emailCtrl: new FormControl('',Validators.required),
      phoneCtrl: new FormControl(),
      roleCtrl: new FormControl(),
      positionCtrl: new FormControl(),
      userCtrl: new FormControl('',Validators.required),
      passwordCtrl: new FormControl('', Validators.required)
    })
   }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }
  save() {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) return;
    this.user.isActive = true;
    this._userService.create(this.user).subscribe(response => {
      if (response) this.dialogRef.close(true);
    })
  }
  nodeSelect(event) {
    this.user.departmentId = event.node.data.id;
  }
  changeDepartment(event) {
    if (event.value == null) {
      this.selection = undefined;
      this.user.departmentId = null
    }
  }

}
