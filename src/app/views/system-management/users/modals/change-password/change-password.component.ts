import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserServiceProxy, ChangePasswordDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends AppComponentBase implements OnInit {

  formGroup: FormGroup
  changePassUser = new ChangePasswordDto
  isLoading: boolean
  verifyPassword: string

  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any
  ) {
    super(injector)
    if (data) {
      if (data.userId) {
        this.changePassUser.userId = data.userId;
      }
    }
   }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      oldPasswordCtrl: new FormControl('', Validators.required),
      newPasswordCtrl: new FormControl('', Validators.required),
      verifyPasswordCtrl: new FormControl('', Validators.required)
    })
  }

  save() {
    this.isLoading = true;
    this._userService.changePassword(this.changePassUser).pipe(
      finalize(() => {
          this.doneLoading();
      })
  )
  .subscribe(success => {
      if (success) {
          this.dialogRef.close(true);
      }
  });
  }

  private doneLoading(): void {
    this.isLoading = false;
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
