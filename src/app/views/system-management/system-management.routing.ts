import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { DepartmentsComponent } from './departments/departments.component';
import { CategoryComponent } from './categories/categories.component';
export const routes: Routes = [{
    path: 'users',
    component: UsersComponent
},
{
    path: 'roles',
    component: RolesComponent
},
{
    path: 'departments',
    component: DepartmentsComponent
},
{
    path: 'categories',
    component: CategoryComponent
}]

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
  })
  export class SystemManagementRouting {}