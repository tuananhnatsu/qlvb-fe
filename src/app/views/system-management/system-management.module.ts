import { NgModule } from '@angular/core';
import { UsersComponent } from './users/users.component';
import { SystemManagementRouting } from './system-management.routing';
import { DepartmentsComponent } from './departments/departments.component';
import { CreateDepartmentComponent } from './departments/modals/create-department/create-department.component';
import { AddUserToDepartmentComponent } from './departments/modals/add-user-to-department/add-user-to-department.component';
import { CreateUsersComponent } from './users/modals/create-users/create-users.component';
import { EditUsersComponent } from './users/modals/edit-users/edit-users.component';
import { SharedModule } from '../../shared/shared.module';
import { RolesComponent } from './roles/roles.component';
import { CreateRoleDialogComponent } from './roles/modals/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/modals/edit-role/edit-role-dialog.component';
import { CategoryComponent } from './categories/categories.component';
import {CreateCategoryDialogComponent} from './categories/modals/create-category/create-category-dialog.component'
import {EditCategoryDialogComponent} from './categories/modals/edit-category/edit-category-dialog.component';
import { ChangePasswordComponent } from './users/modals/change-password/change-password.component';
import { ResetPasswordComponent } from './users/modals/reset-password/reset-password.component'
@NgModule({
    declarations: [
        UsersComponent,
        RolesComponent,
        CreateRoleDialogComponent,
        EditRoleDialogComponent,
        DepartmentsComponent,
        CreateDepartmentComponent,
        AddUserToDepartmentComponent,
        CreateUsersComponent,
        EditUsersComponent,
        CategoryComponent,
        CreateCategoryDialogComponent,
        EditCategoryDialogComponent,
        ChangePasswordComponent,
        ResetPasswordComponent
    ],
    imports : [
        SystemManagementRouting,
        SharedModule
    ],
    bootstrap: [
        CreateDepartmentComponent,
        AddUserToDepartmentComponent,
        CreateUsersComponent,
        EditUsersComponent,
        ChangePasswordComponent,
        ResetPasswordComponent
    ]
  })
  export class SystemManagementModule {}