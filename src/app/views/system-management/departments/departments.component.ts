import { Component, OnInit, Injector } from '@angular/core';
import { DepartmentService } from '../../../services/system-management/department.service';
import { TreeNode } from 'primeng/api/treenode';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialog } from '@angular/material/dialog';
import { CreateDepartmentComponent } from './modals/create-department/create-department.component';
import { AddUserToDepartmentComponent } from './modals/add-user-to-department/add-user-to-department.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService, ConfirmationService } from 'primeng/api';

export class DepartmentDto {
  departmentName: string
  level: number
  dependentDepartment: string
  id: number
  dependentDepartmentId: number
  hasChild: boolean
}

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent extends AppComponentBase implements OnInit {

  isTableLoading = false
  listTreeNodeDepartments: TreeNode[];
  formGroup: FormGroup;
  departmentInfo = new DepartmentDto;
  departmentName : string;
  keyword: string

  constructor(
    private departmentService: DepartmentService,
    injector: Injector,
    private dialog: MatDialog
  ) {
    super(injector)
   }

  ngOnInit(): void {
    this.filterDepartments();
    this.formGroup = new FormGroup({
      departmentNameCtrl: new FormControl({ value: '', disabled: true }, Validators.required),
      levelCtrl: new FormControl({ value: '', disabled: true }),
      dependentDepartmentCtrl: new FormControl({ value: '', disabled: true }),
    })
  }

  filterDepartments(keyword?: string, dependentDepartmentId?: number) {
    this.isTableLoading = true;
    this.departmentService.getTreeDepartments(keyword, dependentDepartmentId).subscribe(response => {
      if (response.success) {
        this.listTreeNodeDepartments = response.result.items;
      }
    },() => {}, () => {
      this.isTableLoading = false;
    })
  }

  openCreateRoot() {
    this.dialog.open(CreateDepartmentComponent, {
      data: {
        root: true
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới phòng ban cấp cao')});
        this.filterDepartments();
      }
    })
  }

  openCreateChild() {
    this.dialog.open(CreateDepartmentComponent, {
      data: {
        root: false,
        item: {
          parentName: this.departmentName,
          parentId: this.departmentInfo.id,
          parentLevel: this.departmentInfo.level
        }
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới phòng ban con')});
        this.filterDepartments();
      }
    })
  }

  openAddUserToDepartment() {
    this.dialog.open(AddUserToDepartmentComponent, {
      // maxWidth: '500px'
      data: this.departmentInfo.id
    }).afterClosed().subscribe(response => {
      if (response) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Thêm người dùng vào phòng ban')});
      }
    })
  }

  nodeSelect(event) {
    this.departmentName = event.node.label;
    this.formGroup.controls.departmentNameCtrl.enable();
    this.departmentInfo = {
      departmentName: event.node.label,
      dependentDepartment: event.node.parent ? event.node.parent.label : this.l("Không có"),
      level: event.node.data.level,
      id: event.node.data.id,
      dependentDepartmentId: event.node.parent ? event.node.parent.data.id : null,
      hasChild: event.node.children && event.node.children.length > 0
    }
  }

  save() {
    if (this.formGroup.invalid) return;
    this.departmentService.put(this.departmentInfo).subscribe(response => {
      if (response.success) {
        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Thay đổi thông tin phòng ban')});
        this.filterDepartments();
        this.departmentName = this.departmentInfo.departmentName
      }
    })
  }

  delete() {
    this._confirmService.confirm({
      message: this.l(`Xác nhận xóa "${this.departmentName}"?`),
      header: this.l("Xác nhận"),
      accept: () => {
        this.departmentService.deleteDepartments(this.departmentInfo.id).subscribe(response => {
          if (response.success) {
            this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Xóa phòng ban')});
            this.filterDepartments();
          }
        }, err => {
          if(err.error.error.message === "EXIST_USERS") {
            this.message.error(this.l("Hãy chắc chắn rằng phòng ban muốn xóa vào các phòng ban con (nếu có) là các phòng trống!"), this.l("Tồn tại người dùng"))
          };
        })
      }
    })
  }
}
