import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartmentService } from '@app/services/system-management/department.service';

export class CreateDepartmentDto {
  departmentName: string
  level: number
  dependentDepartmentId: number
}

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.css']
})
export class CreateDepartmentComponent extends AppComponentBase implements OnInit {

  department = new CreateDepartmentDto
  formGroupCreate: FormGroup;
  dependentDepartmentName: string

  constructor(
    injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) data: any,
    private dialogRef: MatDialogRef<CreateDepartmentComponent>,
    private _departmentService: DepartmentService
  ) {
    super(injector)
    this.formGroupCreate = new FormGroup({
      departmentNameCtrl: new FormControl('', Validators.required),
      levelCtrl: new FormControl({ value: '', disabled: true }),
      dependentDepartmentCtrl: new FormControl({ value: '', disabled: true }),
    })
    if (!data.root) {
      this.department.dependentDepartmentId = data.item.parentId
      this.department.level = data.item.parentLevel + 1
      this.dependentDepartmentName = data.item.parentName
    } else {
      this.department.level = 0;
      this.dependentDepartmentName = this.l("Không có")
    }
   }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.formGroupCreate.markAllAsTouched();
    this.formGroupCreate.markAsDirty();
    if (this.formGroupCreate.invalid) return;
    this._departmentService.post(this.department).subscribe(response => {
      if (response.success) {
        this.dialogRef.close(true);
      }
    })
  }

}
