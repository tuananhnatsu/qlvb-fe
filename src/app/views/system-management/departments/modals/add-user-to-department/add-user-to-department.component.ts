import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { PagedListingComponentBase, PagedRequestDto } from '@shared/paged-listing-component-base';
import { AddUserToDepartmentModel } from '@app/models/system-management/add-user-to-department.model';
import { UserSysService } from '@app/services/system-management/user.service';
import { UserPagedRequestDto } from '@app/views/system-management/users/users.component';
import { UserDtoPagedResultDto } from '@shared/service-proxies/service-proxies';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppComponentBase } from '@shared/app-component-base';
import { DepartmentService } from '@app/services/system-management/department.service';
import { forkJoin } from 'rxjs';

export class RequestDto extends PagedRequestDto{
  keyword: string
}

@Component({
  selector: 'app-add-user-to-department',
  templateUrl: './add-user-to-department.component.html',
  styleUrls: ['./add-user-to-department.component.css']
})
export class AddUserToDepartmentComponent extends AppComponentBase implements OnInit {

  listUserSelect = [];
  listUserSelected = [];
  userPageRequest = new UserPagedRequestDto

  constructor(
    injector: Injector,
    private _userService: UserSysService,
    private _departmentService: DepartmentService,
    private dialogRef: MatDialogRef<AddUserToDepartmentComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
    super(injector);
    this.userPageRequest.listDepartmentId = [0, null]
   }

  ngOnInit(): void {
    forkJoin([this._userService.searchUsers(this.userPageRequest), this._departmentService.getAllUserByDepartmentId(this.data)]).subscribe(
      response =>  {
        if (response[0].success) {
          this.listUserSelect = response[0].result.items
        }
        if (response[1].success) {
          this.listUserSelected = response[1].result.items
        }
      })
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this._userService.updateManyUsers([...this.listUserSelected.map(m => {
      const obj = m;
      obj.departmentId = this.data;
      return obj;
    }), ...this.listUserSelect.map(m => {
      const obj = m;
      obj.departmentId = null;
      return obj;
    })]).subscribe(response => {
      if (response.success) {
        this.dialogRef.close(true);
      }
    })
  }

}
