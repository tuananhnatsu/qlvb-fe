import { Component, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { AppComponentBase } from '@app/shared/app-component-base';
import { RoleDto, PermissionDto, RoleServiceProxy, PermissionDtoListResultDto, CreateRoleDto, RoleDtoPagedResultDto } from '@app/shared/service-proxies/service-proxies';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  templateUrl: 'create-role-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
      ::ng-deep .mat-tab-body-content {
        padding: 1rem;
      }
    `
  ]
})
export class CreateRoleDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  role: RoleDto = new RoleDto();
  permissions: PermissionDto[] = [];
  grantedPermissionNames: string[] = [];
  checkedPermissionsMap: { [key: string]: boolean } = {};
  defaultPermissionCheckedStatus = false;
  formGroup: FormGroup;
  roles: RoleDto[] = [];
  constructor(
    injector: Injector,
    private _roleService: RoleServiceProxy,
    private _dialogRef: MatDialogRef<CreateRoleDialogComponent>
  ) {
    super(injector);
    this.formGroup = new FormGroup({
      nameCtrl: new FormControl('', Validators.required),
      displayNameCtrl: new FormControl('', Validators.required),
      descriptionCtrl: new FormControl()
    })
  }

  ngOnInit(): void {
    this._roleService
      .getAllPermissions()
      .subscribe((result: PermissionDtoListResultDto) => {
        this.permissions = result.items;
        this.setInitialPermissionsStatus(this.permissions);
      });
    this._roleService
      .getAll('', 0, 1000)
      .pipe(
        finalize(() => {

        })
      )
      .subscribe((result: RoleDtoPagedResultDto) => {
        this.roles = result.items;

      });
  }

  setInitialPermissionsStatus(permissions: PermissionDto[]): void {
    _.map(permissions, item => {
      this.checkedPermissionsMap[item.name] = this.isPermissionChecked(item.name);
      if (item.children.length > 0) this.setInitialPermissionsStatus(item.children);
    });
  }

  isPermissionChecked(permissionName: string): boolean {
    // just return default permission checked status
    // it's better to use a setting
    return this.defaultPermissionCheckedStatus;
  }

  onPermissionChange(permission: PermissionDto, $event) {
    this.checkedPermissionsMap[permission.name] = $event.checked;
  }

  getCheckedPermissions(): string[] {
    const permissions: string[] = [];
    _.forEach(this.checkedPermissionsMap, function (value, key) {
      if (value) {
        permissions.push(key);
      }
    });
    return permissions;
  }

  save(): void {
    this.formGroup.markAllAsTouched();    
    if (this.formGroup.invalid) return;
    var exist = _.find(this.roles, r => {
      return r.name == this.role.name;
    });
    if(exist && exist != null){      
      this.message.error('Vai trò có mã '+this.role.name+' đã tồn tại.', this.l("Vai trò đã tồn tại"))
      return;
    }
    this.saving = true;

    this.role.grantedPermissions = this.getCheckedPermissions();

    const role_ = new CreateRoleDto();
    role_.init(this.role);

    this._roleService
      .create(role_)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
