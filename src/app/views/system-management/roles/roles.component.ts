import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { CreateRoleDialogComponent } from './modals/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './modals/edit-role/edit-role-dialog.component';
import { PagedRequestDto, PagedListingComponentBase } from '@app/shared/paged-listing-component-base';
import { appModuleAnimation } from '@app/shared/animations/routerTransition';
import { RoleDto, RoleServiceProxy, RoleDtoPagedResultDto } from '@app/shared/service-proxies/service-proxies';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';

class PagedRolesRequestDto extends PagedRequestDto {
    keyword: string;
}

@Component({
    templateUrl: './roles.component.html',
    animations: [appModuleAnimation()],
    styles: [
        `
          mat-form-field {
            padding: 10px;
          }
        `
    ]
})
export class RolesComponent extends PagedListingComponentBase<RoleDto> {
    @ViewChild("paginator") paginator: MatPaginator
    input = new PagedRolesRequestDto;
    roles: RoleDto[] = [];
    cols = [
        {
            field: "name",
            header: this.l("Mã vai trò")
        },
        {
            field: "displayName",
            header: this.l("Tên vai trò")
        },
        {
            field: "description",
            header: this.l("Mô tả")
        },
        {
            field: "act",
            header: this.l("Actions")
        }
    ];
    keyword = '';
    isOpenRow: any
    constructor(
        injector: Injector,
        private _rolesService: RoleServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    list(
        request: PagedRolesRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ): void {
        request.keyword = this.keyword;

        this._rolesService
            .getAll(request.keyword, request.skipCount, request.maxResultCount)
            .pipe(
                finalize(() => {
                    finishedCallback();
                })
            )
            .subscribe((result: RoleDtoPagedResultDto) => {
                this.roles = result.items;
                this.showPaging(result, pageNumber);
            });
    }

    delete(role: RoleDto): void {
        this._confirmService.confirm({
            message: this.l(`Xác nhận xóa "${role.displayName}" và tất cả người dùng sẽ được bỏ chỉ định khỏi vai trò này ?`),
            header: this.l("Xác nhận"),
            accept: () => {
                this._rolesService
                .delete(role.id)
                .pipe(
                    finalize(() => {
                        this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Xóa vai trò')});
                        this.refresh();
                    })
                )
                .subscribe(() => { });
            }
          })
    }

    create(): void {
        this.showCreateOrEditRoleDialog();
    }

    edit(role: RoleDto): void {
        this.showCreateOrEditRoleDialog(role.id);
    }

    showCreateOrEditRoleDialog(id?: number): void {
        let createOrEditRoleDialog;
        if (id === undefined || id <= 0) {
            createOrEditRoleDialog = this._dialog.open(CreateRoleDialogComponent, {
                minWidth: "500px"
            });
        } else {
            createOrEditRoleDialog = this._dialog.open(EditRoleDialogComponent, {
                minWidth: "500px",
                data: id
            });
        }

        createOrEditRoleDialog.afterClosed().subscribe(result => {
            if (result) {
                if (id === undefined || id <= 0) {
                    this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Tạo mới vai trò!')});
                }
                else 
                {
                    this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Sửa vai trò!')});
                }
                this.refresh();
            }
        });
    }
}
