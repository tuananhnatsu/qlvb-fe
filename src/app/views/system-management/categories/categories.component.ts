import { Component, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { CreateCategoryDialogComponent } from './modals/create-category/create-category-dialog.component';
// import { EditRoleDialogComponent } from './modals/edit-role/edit-role-dialog.component';
import { PagedRequestDto, PagedListingComponentBase } from '@app/shared/paged-listing-component-base';
import { appModuleAnimation } from '@app/shared/animations/routerTransition';
import { MatDialog } from '@angular/material/dialog';
import { CategoryDto, CategoryDtoPagedResultDto, PagedCategoriesRequestDto } from '@app/models/system-management/category.model';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CategoryService } from '@app/services/system-management/category.service';
import { EditCategoryDialogComponent } from './modals/edit-category/edit-category-dialog.component';
import * as _ from 'lodash';

@Component({
    templateUrl: './categories.component.html',
    animations: [appModuleAnimation()]
})
export class CategoryComponent extends PagedListingComponentBase<CategoryDto>{
    paginator
    input = new PagedCategoriesRequestDto;
    categories: CategoryDto[] = [];
    cols = [
        {
            field: "name",
            header: this.l("Tên danh mục")
        },
        {
            field: "typeName",
            header: this.l("Loại danh mục")
        },
        {
            field: "description",
            header: this.l("Mô tả")
        },
        {
            field: "creationTime",
            header: this.l("Ngày tạo")
        },
        {
            field: "act",
            header: this.l("Actions")
        }
    ];
    keyword = '';
    isOpenRow: any;
    categoryTypes: any[] = [];
    constructor(
        injector: Injector,
        private _lookuptableservice: LookupTableService,
        private _service: CategoryService,
        private _dialog: MatDialog
    ) {
        super(injector);
        this._lookuptableservice.getAllCategoryTypeForLookupTable().subscribe(rs => {
            this.categoryTypes = [];
            _.map(rs, type => {
                this.categoryTypes.push({ label: this.l(type.displayName), value: type.id });
            });
        });
    }
    protected delete(category: CategoryDto): void {
        this._confirmService.confirm({
            message: this.l(`Mọi dữ liệu được gán với danh mục này đều sẽ được bỏ gán! Xác nhận xóa danh mục "${category.displayName}"?`),
            header: this.l("Xác nhận"),
            accept: () => {
              this._service.delete(category.id)
              .pipe(
                  finalize(() => {
                    this._messageService.add({severity:'success', summary: this.l('Thành công'), detail: this.l('Xóa danh mục')});
                    this.refresh();
                  })
              )
              .subscribe(() => { })
            }
          })
    }
    list(
        request: PagedCategoriesRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ): void {
        request.type = this.input.type
        request.keyword = this.input.keyword       

        this._service
            .getAllCategories(request)
            .pipe(
                finalize(() => {
                    finishedCallback();
                })
            )
            .subscribe((result: CategoryDtoPagedResultDto) => {
                this.categories = result.items;
                _.map(this.categories, cat => {
                    var type = this.categoryTypes.filter(t => t.value === cat.type);
                    if (type !== undefined && type != null && type.length > 0) {
                        cat.typeName = type[0].label;
                    }
                });
                this.showPaging(result, pageNumber);
            });
    }

    create(): void {
        this.showCreateOrEditCategoryDialog();
    }

    edit(cat: CategoryDto): void {
        this.showCreateOrEditCategoryDialog(cat.id);
    }

    showCreateOrEditCategoryDialog(id?: number): void {
        let createOrEditCategoryDialog;
        if (id === undefined || id <= 0) {
            createOrEditCategoryDialog = this._dialog.open(CreateCategoryDialogComponent, {
                width: '600px',
                maxWidth: "800px"
            });
        } else {
            createOrEditCategoryDialog = this._dialog.open(EditCategoryDialogComponent, {
                width: '700px',
                maxWidth: "800px",
                data: id
            });
        }

        createOrEditCategoryDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh();
            }
        });
    }
}
