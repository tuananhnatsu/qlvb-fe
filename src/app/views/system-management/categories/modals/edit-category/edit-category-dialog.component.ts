import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryDto, CategoryDtoPagedResultDto } from '@app/models/system-management/category.model';
import { LookupTableService } from '@app/services/lookuptable.service';
import { CategoryService } from '@app/services/system-management/category.service';
import { LookupTableDto } from '@app/models/system-management/lookuptable.model';

@Component({
  templateUrl: 'edit-category-dialog.component.html',
  styles: [
    `
        mat-form-field {
          width: 100%;
        }
        mat-checkbox {
          padding-bottom: 5px;
        }
      `
  ]
})
export class EditCategoryDialogComponent extends AppComponentBase implements OnInit {
  saving = false;
  categogy: CategoryDto = new CategoryDto();
  formGroup: FormGroup;
  categoryTypes: any[] = [];
  constructor(
    injector: Injector,
    @Optional() @Inject(MAT_DIALOG_DATA) private _id: number,
    private _service: CategoryService,
    private _lookuptableservice: LookupTableService,
    private _dialogRef: MatDialogRef<EditCategoryDialogComponent>
  ) {
    super(injector);
    this.formGroup = new FormGroup({
      nameCtrl: new FormControl('', Validators.required),
      typeCtrl: new FormControl(1, Validators.required),
      descriptionCtrl: new FormControl()
    })
  }
  ngOnInit(): void {
    this._lookuptableservice.getAllCategoryTypeForLookupTable().subscribe(rs => {
      this.categoryTypes = [];      
      _.map(rs, type => {
        this.categoryTypes.push({ label: this.l(type.displayName), value: type.id });
      });
    });
    this._service
      .getCategoryForEdit(this._id)
      .subscribe((result: CategoryDto) => {
        this.categogy = result;
      });
  }

  save(): void {
    this.saving = true;

    this._service
      .update(this.categogy)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}