// import { Injectable } from '@angular/core';
// import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
// import { Observable } from 'rxjs';
// // import { AuthenticationService } from 'app/services/authentication/auth.service';
// // import { environment } from '../../../environments/environment.setting';

// @Injectable()
// export class DefaultInterceptor implements HttpInterceptor {
//     constructor(private authenticationService: AuthenticationService) { }

//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         // add authorization header with basic auth credentials if available
//         if (!request['method'] || request['method'] === 'GET')
//         {
//             if (request['url'].includes('https://maps.googleapis.com')) return next.handle(request);
//         }
//         const currentUser = this.authenticationService.currentUserValue;
//         if (currentUser) {
//             request = request.clone({
//                 setHeaders: { 
//                     'Authorization': `Bearer ${currentUser.token}`
//                 }
//             });
//         }

//         return next.handle(request);
//     }
// }