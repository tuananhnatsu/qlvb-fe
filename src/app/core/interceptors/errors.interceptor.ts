// import { Injectable } from '@angular/core';
// import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
// import { Observable, throwError } from 'rxjs';
// import { tap } from 'rxjs/operators';

// import { AuthenticationService } from 'app/services/authentication/auth.service';
// import { Router } from '@angular/router';

// @Injectable()
// export class ErrorInterceptor implements HttpInterceptor {
//     constructor(
//         private authenticationService: AuthenticationService,
//         private router: Router
//         ) { }

//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         return next.handle(request).pipe(
//             tap(response => {
//                 if (response.type !== 0){
//                     let _res = new Object(response);
//                     if (_res.hasOwnProperty("body")){
//                         if (_res["body"]["httpStatusCode"] === 403){
//                             // auto logout if 403(TIMEOUT) response returned from api
//                             this.authenticationService.logout();
//                             this.router.navigate(['/login'], { queryParams: { error: 403 }});
//                         }
//                     }    
//                 }
//             }),
//         )
//     }
// }