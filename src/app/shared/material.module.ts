import { NgModule } from '@angular/core';
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatDialogModule } from "@angular/material/dialog";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
    imports: [
        MatPaginatorModule,
        MatDialogModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatMenuModule,
        MatIconModule,
        MatCheckboxModule,
        MatTabsModule
    ],
    exports: [
        MatPaginatorModule,
        MatDialogModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatMenuModule,
        MatIconModule,
        MatCheckboxModule,
        MatTabsModule
    ],
  })
  export class MaterialModule {}