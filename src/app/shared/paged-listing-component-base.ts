
import { Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from './app-component-base';
import { MatPaginator } from '@angular/material/paginator';

export class PagedResultDto {
    items: any[];
    totalCount: number;
}

export class EntityDto {
    id: number;
}

export class PagedRequestDto {
    skipCount: number = 0;
    maxResultCount: number;
}

export abstract class PagedListingComponentBase<TEntityDto> extends AppComponentBase implements OnInit {

    public pageSize = 10;
    public pageNumber = 0;
    public totalPages = 1;
    public totalItems: number;
    public isTableLoading = false;
    public openFilter = false;
    public startIndex = 0;

    public abstract paginator: MatPaginator;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        this.refresh();
    }

    refresh(): void {
        this.getDataPage(this.pageNumber, this.pageSize);
    }

    toggleFilter() {
        this.openFilter = !this.openFilter;
    }

    public showPaging(result: any, pageNumber: number): void {
        this.totalPages = ((result.totalCount - (result.totalCount % this.pageSize)) / this.pageSize) + 1;

        this.totalItems = result.totalCount;
        this.pageNumber = pageNumber;
    }

    public getDataPage(page: number, pageSize?: number): void {
        const req = new PagedRequestDto();
        this.pageSize = pageSize ? pageSize : this.pageSize;
        req.maxResultCount = this.pageSize;
        req.skipCount = page * this.pageSize;
        this.startIndex = req.skipCount;

        this.isTableLoading = true;
        this.list(req, page, () => {
            this.isTableLoading = false;
        });
    }

    protected abstract list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void;
    protected abstract delete(entity: TEntityDto): void;
}
