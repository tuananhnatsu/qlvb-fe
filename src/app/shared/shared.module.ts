import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AbpModule } from '@abp/abp.module';
import { RouterModule } from '@angular/router';
//import { NgxPaginationModule } from 'ngx-pagination';

import { AppSessionService } from './session/app-session.service';
import { AppRouteGuard } from '@core/auth/auth-route-guard';
import { PrimengModule } from './primeng.module';
import { MaterialModule } from './material.module';
import { AppAuthService } from '@core/auth/app-auth.service';

import { BlockDirective } from './directives/block.directive';
import { BusyDirective } from './directives/busy.directive';
import { EqualValidator } from './directives/equal-validator.directive';
import { LocalizePipe } from './pipes/localize.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MessageService, ConfirmationService } from 'primeng/api';
import { NgxDocViewerModule } from 'ngx-doc-viewer';

@NgModule({
    imports: [
        CommonModule,
        AbpModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        PrimengModule,
        MaterialModule,
        NgxDocViewerModule
    ],
    declarations: [
        LocalizePipe,
        BlockDirective,
        BusyDirective,
        EqualValidator
    ],
    providers: [
        AppSessionService,
        AppRouteGuard,
        AppAuthService,
        MessageService,
        ConfirmationService
    ],
    exports: [
        CommonModule,
        AbpModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        PrimengModule,
        MaterialModule,
        LocalizePipe,
        BlockDirective,
        BusyDirective,
        EqualValidator,
        NgxDocViewerModule
    ]
})
export class SharedModule {
}
