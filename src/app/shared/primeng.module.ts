import { NgModule } from '@angular/core';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToastModule } from 'primeng/toast';
import { TreeModule } from 'primeng/tree';
import { ContextMenuModule } from 'primeng/contextmenu';
import { PickListModule } from 'primeng/picklist';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PasswordModule } from 'primeng/password';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {TabViewModule} from 'primeng/tabview';
import {InputMaskModule} from 'primeng/inputmask';



@NgModule({
    imports: [
        CalendarModule,
        InputTextModule,
        TableModule,
        CheckboxModule,
        DropdownModule,
        MultiSelectModule,
        RadioButtonModule,
        ToastModule,
        TreeModule,
        ContextMenuModule,
        PickListModule,
        ConfirmDialogModule,
        InputTextareaModule,
        PasswordModule,
        AutoCompleteModule,
        TabViewModule,
        InputMaskModule,
    ],
    exports: [
        CalendarModule,
        InputTextModule,
        TableModule,
        CheckboxModule,
        DropdownModule,
        MultiSelectModule,
        RadioButtonModule,
        ToastModule,
        TreeModule,
        ContextMenuModule,
        PickListModule,
        ConfirmDialogModule,
        InputTextareaModule,
        PasswordModule,
        AutoCompleteModule,
        TabViewModule,
        InputMaskModule
    ],
  })
  export class PrimengModule {}