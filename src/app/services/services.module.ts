import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';
import { DepartmentService } from './system-management/department.service';
import { UserSysService } from './system-management/user.service';
import { LookupTableService } from './lookuptable.service';
import {CategoryService} from './system-management/category.service';
import {ArrivedFolderService} from './documents/arrived/folder/arrivedfolder.service';
import { OfficeAgentService }  from './documents/arrived/office-agent/office-agent.service';
import { DocumentTypeService }  from './documents/document-types/document-type.service';
import {CommonService} from './common.service';
import {ArrivedDocumentService} from './documents/arrived/arriveddocument.service';
import {ProcessDocumentService} from './documents/arrived/process-document/process-document.service';
import { FileService } from './file.service';
import { DepartureDocumentService } from './documents/departure/departure-document.service';
import { ArrivedDocumentHistoryService } from './documents/arrived/history/arriveddocumenthistory.service';
@NgModule({
    providers: [
        DepartmentService,
        UserSysService,
        LookupTableService,
        CategoryService,
        ArrivedFolderService,
        OfficeAgentService,
        DocumentTypeService,
        CommonService,
        ArrivedDocumentService,
        ProcessDocumentService,
        DepartureDocumentService,
        FileService,
        ArrivedDocumentHistoryService,
        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceModule { }