import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { PagedOfficeAgentRequestDto, OfficeAgentDtoPagedResultDto } from '@app/models/documents/arrived/office-agent/office-agent.model';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { PagedRequestArrivedOfficeAgentDto } from '@app/views/documents/arrived/office-agent/officeagent.component';
import { ResponseModel } from '@app/models/response.model';

@Injectable()
export class OfficeAgentService extends BaseService {


  constructor(httpClient: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
    super(httpClient, injector, `${serviceUri}/api/services/app/ArrivedDocument`);
  }

  filterOfficeAgents(input: PagedRequestArrivedOfficeAgentDto): Observable<OfficeAgentDtoPagedResultDto> {
    let url_ = this.serviceUri + "/FilterOfficeAgents";
    url_ = url_.replace(/[?&]$/, "");

    const requestBody: string = JSON.stringify(input);

    let options_: any = {
      body: requestBody,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        "Accept": "text/plain"
      })
    };

    return this._http.request("post", url_, options_)
      .pipe(_observableMergeMap((_response: any) => {
        if (_response instanceof HttpResponseBase) {
          try {
            return this.processFilterOfficeAgents(_response);
          } catch (e) {
            return <Observable<OfficeAgentDtoPagedResultDto>>_observableThrow(e);
          }
        } else {
          return <Observable<OfficeAgentDtoPagedResultDto>>_observableThrow(_response);
        }
      }));
  }

  protected processFilterOfficeAgents(response: HttpResponseBase): Observable<OfficeAgentDtoPagedResultDto> {
    const status = response.status;
    const responseBlob = response instanceof HttpResponse
      ? response.body
      : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = OfficeAgentDtoPagedResultDto.fromJS(resultData200);
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }

    return _observableOf<OfficeAgentDtoPagedResultDto>(<any>null);
  }

  getStasticArrivedDocument() {
    const url_ = `${this.serviceUri}/GetStastic`;
    return this.defaultGet(url_)
  }

  changeStatusArrivedDocuments(body: any): Observable<ResponseModel> {
    const url_ = `${this.serviceUri}/ChangeStatusArrivedDocuments`
    return <Observable<ResponseModel>>this._http.post(url_, body)
  }
  removeManyArrivedDocument(body: number[]): Observable<ResponseModel> {
    let url_ = `${this.serviceUri}/DeleteMany`;
    if (body) {
      url_ = url_ + `?listArrivedDocumentsId=${body[0]}`
      for(let i=1; i<body.length; i++){
        url_ = url_ + `&listArrivedDocumentsId=${body[i]}`
      }
    }
    return <Observable<ResponseModel>>this._http.delete(url_)
  }

}