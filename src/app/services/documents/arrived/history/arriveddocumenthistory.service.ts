import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { PagedOfficeAgentRequestDto, OfficeAgentDtoPagedResultDto } from '@app/models/documents/arrived/office-agent/office-agent.model';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { ResponseModel } from '@app/models/response.model';
import { PagedRequestDto } from '@shared/paged-listing-component-base';

export class PagedRequestArrivedDocumentHistoryDto extends PagedRequestDto{
    keyword: string
    arrivedDocumentId: number
}

@Injectable()
export class ArrivedDocumentHistoryService extends BaseService {


  constructor(httpClient: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
    super(httpClient, injector, `${serviceUri}/api/services/app/ArrivedDocumentHistory`);
  }

  searchHistoryByArrivedDocumentId(input: PagedRequestArrivedDocumentHistoryDto): Observable<ResponseModel> {
      const url_ = `${this.serviceUri}/SearchHistoryByArrivedDocumentId`
      return this.defaultPost(url_, input)
  }
}