import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { Observable, throwError as _observableThrow, of as _observableOf  } from 'rxjs';
import * as _ from 'lodash';
import { Header } from 'primeng/api/shared';
import { PagedProcessDocumentRequestDto, ArrivedDocumentDto, CordinateDocumentsDto } from '../../../../models/documents/arrived/arriveddocument.model';
import { ProcessDocumentDtoPagedResultDto } from '../../../../models/documents/arrived/arriveddocument.model';
import { UserDto } from '../../../../models/system-management/user.model';

@Injectable()
export class ProcessDocumentService extends BaseService {
    private _httpClient: HttpClient;

    constructor(httpClient: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
      super(httpClient, injector, `${serviceUri}/api/services/app/ProcessDocument`);
      this._httpClient = httpClient;
    }
    
    filterProcessDocuments(input: PagedProcessDocumentRequestDto): Observable<ProcessDocumentDtoPagedResultDto> {
        let url_ = this.serviceUri + "/FilterProcessDocumentList";
        url_ = url_.replace(/[?&]$/, "");
    
        const requestBody: string = JSON.stringify(input);
    
        let options_: any = {
          body: requestBody,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
          })
        };
    
        return this._httpClient.request("post", url_, options_)
          .pipe(_observableMergeMap((_response: any) => {
            if (_response instanceof HttpResponseBase) {
              try {
                return this.processFilterProcessDocuments(_response);
              } catch (e) {
                return <Observable<ProcessDocumentDtoPagedResultDto>>_observableThrow(e);
              }
            } else {
              return <Observable<ProcessDocumentDtoPagedResultDto>>_observableThrow(_response);
            }
          }));
      }
    
      protected processFilterProcessDocuments(response: HttpResponseBase): Observable<ProcessDocumentDtoPagedResultDto> {
        const status = response.status;
        const responseBlob = response instanceof HttpResponse
          ? response.body
          : (<any>response).error instanceof Blob ? (<any>response).error : undefined;
    
        let _headers: any = {};
        if (response.headers) {
          for (let key of response.headers.keys()) {
            _headers[key] = response.headers.get(key);
          }
        }
        if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ProcessDocumentDtoPagedResultDto.fromJS(resultData200);
            return _observableOf(result200);
          }));
        } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
        }
    
        return _observableOf<ProcessDocumentDtoPagedResultDto>(<any>null);
      }

      /**
     * @param id (optional) 
     * @return Success
     */
    getDocumentById(id: number | undefined, typeDocument: string | undefined): Observable<ArrivedDocumentDto> {
      let url_ = this.serviceUri + "/GetDocumentById?";
      if (id === null)
          throw new Error("The parameter 'id' cannot be null.");
      else if (id !== undefined)
          url_ += "Id=" + encodeURIComponent("" + id) + "&typeDocument=" + typeDocument;
      url_ = url_.replace(/[?&]$/, "");

      let options_: any = {
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Accept": "text/plain"
          })
      };

      return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processGet(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processGet(<any>response_);
              } catch (e) {
                  return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
              }
          } else
              return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
      }));
  }

  protected processGet(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = ArrivedDocumentDto.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<ArrivedDocumentDto>(<any>null);
  }

  completeDocument(body: ArrivedDocumentDto) :Observable<ArrivedDocumentDto>{
    let url_ = this.serviceUri + "/CompleteDocument";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
        body: content_,
        observe: "response",
        responseType: "blob",
        headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
        })
    };

    return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
        return this.processcompleteDocument(response_);
    })).pipe(_observableCatch((response_: any) => {
        if (response_ instanceof HttpResponseBase) {
            try {
                return this.processcompleteDocument(<any>response_);
            } catch (e) {
                return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
            }
        } else
            return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
    }));
  }

  protected processcompleteDocument(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
    const status = response.status;
    const responseBlob =
        response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ArrivedDocumentDto.fromJS(resultData200);
            return _observableOf(result200);
        }));
    } else if (status !== 200 && status !== 204) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }));
    }
    return _observableOf<ArrivedDocumentDto>(<any>null);
  }

/**
     * @param body (optional) 
     * @return Success
     */
    update(body: ArrivedDocumentDto | undefined): Observable<ArrivedDocumentDto> {
      let url_ = this.serviceUri + "/Update";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_: any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this._http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processUpdate(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processUpdate(<any>response_);
              } catch (e) {
                  return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
              }
          } else
              return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
      }));
  }

  protected processUpdate(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = ArrivedDocumentDto.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<ArrivedDocumentDto>(<any>null);
  }

  updateDocument(body: CordinateDocumentsDto) :Observable<CordinateDocumentsDto>{
    let url_ = this.serviceUri + "/UpdateDocument";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
        body: content_,
        observe: "response",
        responseType: "blob",
        headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
        })
    };

    return this._http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
        return this.processUpdateDocument(response_);
    })).pipe(_observableCatch((response_: any) => {
        if (response_ instanceof HttpResponseBase) {
            try {
                return this.processUpdateDocument(<any>response_);
            } catch (e) {
                return <Observable<CordinateDocumentsDto>><any>_observableThrow(e);
            }
        } else
            return <Observable<CordinateDocumentsDto>><any>_observableThrow(response_);
    }));
  }

  protected processUpdateDocument(response: HttpResponseBase): Observable<CordinateDocumentsDto> {
    const status = response.status;
    const responseBlob =
        response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = CordinateDocumentsDto.fromJS(resultData200);
            return _observableOf(result200);
        }));
    } else if (status !== 200 && status !== 204) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }));
    }
    return _observableOf<CordinateDocumentsDto>(<any>null);
  }

        /**
     * @param id (optional) 
     * @return Success
     */
    getCodinateDocumentById(id: number | undefined): Observable<CordinateDocumentsDto> {
        let url_ = this.serviceUri + "/GetCordinateDocumentById?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");
  
        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };
  
        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetCodinateDocumentById(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetCodinateDocumentById(<any>response_);
                } catch (e) {
                    return <Observable<CordinateDocumentsDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<CordinateDocumentsDto>><any>_observableThrow(response_);
        }));
    }
  
    protected processGetCodinateDocumentById(response: HttpResponseBase): Observable<CordinateDocumentsDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;
  
        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = CordinateDocumentsDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<CordinateDocumentsDto>(<any>null);
    }
  
}