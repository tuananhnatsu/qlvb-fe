import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { PagedArrivedFolderRequestDto, ArrivedFolderDtoPagedResultDto, ArrivedFolderDto, ArrivedFolderListResultDto } from '@app/models/documents/arrived/folder/arrivedfolder.model';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { CategoryDto } from '@app/models/system-management/category.model';

@Injectable()
export class ArrivedFolderService extends BaseService {
    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/ArrivedFolder`);
    }

    findArrivedFoldersByIds(input: Array<number> = []): Observable<ArrivedFolderListResultDto> {
        let url_ = this.serviceUri + "/FindArrivedFoldersByIds";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.proccessListResultDto(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.proccessListResultDto(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedFolderListResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedFolderListResultDto>><any>_observableThrow(response_);
        }));
    }

    getAllArrivedFolders(input: PagedArrivedFolderRequestDto): Observable<ArrivedFolderDtoPagedResultDto> {
        let url_ = this.serviceUri + "/SearchArrivedFolders";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllArrivedFolders(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllArrivedFolders(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedFolderDtoPagedResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedFolderDtoPagedResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllArrivedFolders(response: HttpResponseBase): Observable<ArrivedFolderDtoPagedResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedFolderDtoPagedResultDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedFolderDtoPagedResultDto>(<any>null);
    }

    protected proccessListResultDto(response: HttpResponseBase): Observable<ArrivedFolderListResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedFolderDtoPagedResultDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedFolderDtoPagedResultDto>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    create(body: ArrivedFolderDto | undefined): Observable<ArrivedFolderDto> {
        let url_ = this.serviceUri + "/Create";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCreate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreate(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedFolderDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedFolderDto>><any>_observableThrow(response_);
        }));
    }

    protected processCreate(response: HttpResponseBase): Observable<ArrivedFolderDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedFolderDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedFolderDto>(<any>null);
    }
    /**
         * @param id (optional) 
         * @return Success
         */
    getArrivedFolderForEdit(id: number | undefined): Observable<ArrivedFolderDto> {
        let url_ = this.serviceUri + "/Get?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGet(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGet(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedFolderDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedFolderDto>><any>_observableThrow(response_);
        }));
    }

    protected processGet(response: HttpResponseBase): Observable<ArrivedFolderDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedFolderDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedFolderDto>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    update(body: ArrivedFolderDto | undefined): Observable<ArrivedFolderDto> {
        let url_ = this.serviceUri + "/Update";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processUpdate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUpdate(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedFolderDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedFolderDto>><any>_observableThrow(response_);
        }));
    }

    protected processUpdate(response: HttpResponseBase): Observable<ArrivedFolderDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedFolderDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedFolderDto>(<any>null);
    }
}