import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { Observable, throwError as _observableThrow, of as _observableOf  } from 'rxjs';
import { ArrivedDocumentDto, ProcessingDocumentGroupDto, ArrivedDocumentUserDto, ConfirmAssignDocumentRequestDto, ProcessDocumentDtoPagedResultDto } from '@app/models/documents/arrived/arriveddocument.model';
import * as _ from 'lodash';
import { Header } from 'primeng/api/shared';
import { OfficeAgentDtoPagedResultDto, PagedOfficeAgentRequestDto } from '../../../models/documents/arrived/office-agent/office-agent.model';
import { ResponseModel } from '@app/models/response.model';
@Injectable()
export class ArrivedDocumentService extends BaseService {
     processingDocumentServiceUrl : string;
  
    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/ArrivedDocument`);
        this.processingDocumentServiceUrl = `${serviceUri}/api/services/app/ProcessingDocumentGroup`;
    }
    /**
     * @param body (optional) 
     * @return Success
     */
    create(body: ArrivedDocumentDto | undefined): Observable<ArrivedDocumentDto> {
        let url_ = this.serviceUri + "/Create";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCreate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreate(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
        }));
    }

    protected processCreate(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedDocumentDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedDocumentDto>(<any>null);
    }

    /**
     * @param body (optional) 
     * @return Success
     */
    update(body: ArrivedDocumentDto | undefined): Observable<ArrivedDocumentDto> {
        let url_ = this.serviceUri + "/Update";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processUpdate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUpdate(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
        }));
    }

    protected processUpdate(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedDocumentDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedDocumentDto>(<any>null);
    }
    /**
     * @param id (optional) 
     * @return Success
     */
    getArrivedDocumentForEdit(id: number | undefined): Observable<ArrivedDocumentDto> {
        let url_ = this.serviceUri + "/GetArrivedDocumentForEdit?";
        if (id === null)
            throw new Error("The parameter 'id' cannot be null.");
        else if (id !== undefined)
            url_ += "Id=" + encodeURIComponent("" + id) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGet(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGet(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
        }));
    }

    protected processGet(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedDocumentDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedDocumentDto>(<any>null);
    }

    filterProcessDocuments(input: ProcessDocumentDtoPagedResultDto): Observable<ProcessDocumentDtoPagedResultDto> {
        let url_ = this.serviceUri + "/FilterProcessDocumentList";
        url_ = url_.replace(/[?&]$/, "");
    
        const requestBody: string = JSON.stringify(input);
    
        let options_: any = {
          body: requestBody,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
          })
        };
    
        return this._http.request("post", url_, options_)
          .pipe(_observableMergeMap((_response: any) => {
            if (_response instanceof HttpResponseBase) {
              try {
                return this.processFilterProcessDocuments(_response);
              } catch (e) {
                return <Observable<ProcessDocumentDtoPagedResultDto>>_observableThrow(e);
              }
            } else {
              return <Observable<ProcessDocumentDtoPagedResultDto>>_observableThrow(_response);
            }
          }));
      }
    
      protected processFilterProcessDocuments(response: HttpResponseBase): Observable<ProcessDocumentDtoPagedResultDto> {
        const status = response.status;
        const responseBlob = response instanceof HttpResponse
          ? response.body
          : (<any>response).error instanceof Blob ? (<any>response).error : undefined;
    
        let _headers: any = {};
        if (response.headers) {
          for (let key of response.headers.keys()) {
            _headers[key] = response.headers.get(key);
          }
        }
        if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = ProcessDocumentDtoPagedResultDto.fromJS(resultData200);
            return _observableOf(result200);
          }));
        } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
        }
    
        return _observableOf<ProcessDocumentDtoPagedResultDto>(<any>null);
      }


       /**
     * @param body (optional) 
     * @return Success
     */
    assignDocumentUser(body: ConfirmAssignDocumentRequestDto | undefined): Observable<ArrivedDocumentDto> {
        let url_ = this.serviceUri + "/AssignDocumentUser";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processAssignDocumentUser(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processAssignDocumentUser(<any>response_);
                } catch (e) {
                    return <Observable<ArrivedDocumentDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ArrivedDocumentDto>><any>_observableThrow(response_);
        }));
    }

    protected processAssignDocumentUser(response: HttpResponseBase): Observable<ArrivedDocumentDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ArrivedDocumentDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ArrivedDocumentDto>(<any>null);
    }

    createOrUpdateProcessingDocumentGroup(body: ProcessingDocumentGroupDto) :Observable<ProcessingDocumentGroupDto>{
        let url_ = this.processingDocumentServiceUrl + "/Create";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCreateOrUpdateProcessingDocumentGroup(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreateOrUpdateProcessingDocumentGroup(<any>response_);
                } catch (e) {
                    return <Observable<ProcessingDocumentGroupDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<ProcessingDocumentGroupDto>><any>_observableThrow(response_);
        }));
      }

      protected processCreateOrUpdateProcessingDocumentGroup(response: HttpResponseBase): Observable<ProcessingDocumentGroupDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ProcessingDocumentGroupDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ProcessingDocumentGroupDto>(<any>null);
    }

    getAllProcessingDocumentGroup(): Observable<ResponseModel> {
        let url_ = this.processingDocumentServiceUrl + "/GetAllProcessingDocumentGroup?";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };
        return this.defaultGet(url_);
    }
    getAllProcessingDocumentDetailGroup(id?:number): Observable<ResponseModel> {
        let url_ = this.processingDocumentServiceUrl + "/GetAllProcessingDocumentDetailGroup?";
        
        let queryString = "";
        if (id){
            queryString += `processingDocumentGroupId=${id}`;
        }
        url_ = (url_+queryString).replace(/[?&]$/, "");
        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };
        return this.defaultGet(url_);
    }

    protected processGetAllProcessingDocumentGroup(response: HttpResponseBase): Observable<ProcessingDocumentGroupDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ProcessingDocumentGroupDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ProcessingDocumentGroupDto>(<any>null);
    }

}