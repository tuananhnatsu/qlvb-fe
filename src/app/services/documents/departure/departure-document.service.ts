import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { Observable, throwError as _observableThrow, of as _observableOf  } from 'rxjs';
import * as _ from 'lodash';
import { Header } from 'primeng/api/shared';
import { ResponseModel } from '@app/models/response.model';
import { DepartureDocumentDtoPagedResultDto, DepartureDocumentPagedRequestDto, DepartureDocumentUserDto, AssignDepartureDocumentUserDto, ReleasedDepartureDocumentDto } from '@app/models/documents/departure/departure-document/departure-document.model';
import { DepartureDocumentForTableDto, ProcessApproveDepartureDocumentDto } from '../../../models/documents/departure/departure-document/departure-document.model';
import { ConfirmReleaseDepartureDocumentComponent } from '@app/views/documents/departure/released/release-document-list/confirm-release-departure-document/confirm-release-departure-document.component';
import { UserDto } from '../../../models/system-management/user.model';
@Injectable()
export class DepartureDocumentService extends BaseService {
  
    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/DepartureDocument`);
    }
    /**
     * @param body (optional) 
     * @return Success
     */
    searchDocuments(input: DepartureDocumentPagedRequestDto): Observable<DepartureDocumentDtoPagedResultDto> {
        let url_ = this.serviceUri + "/SearchDepartureDocument";
        url_ = url_.replace(/[?&]$/, "");
    
        const requestBody: string = JSON.stringify(input);
    
        let options_: any = {
          body: requestBody,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
          })
        };
    
        return this._http.request("post", url_, options_)
          .pipe(_observableMergeMap((_response: any) => {
            if (_response instanceof HttpResponseBase) {
              try {
                return this.processFilterDepartureDocuments(_response);
              } catch (e) {
                return <Observable<DepartureDocumentDtoPagedResultDto>>_observableThrow(e);
              }
            } else {
              return <Observable<DepartureDocumentDtoPagedResultDto>>_observableThrow(_response);
            }
          }));
      }
    
      protected processFilterDepartureDocuments(response: HttpResponseBase): Observable<DepartureDocumentDtoPagedResultDto> {
        const status = response.status;
        const responseBlob = response instanceof HttpResponse
          ? response.body
          : (<any>response).error instanceof Blob ? (<any>response).error : undefined;
    
        let _headers: any = {};
        if (response.headers) {
          for (let key of response.headers.keys()) {
            _headers[key] = response.headers.get(key);
          }
        }
        if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = DepartureDocumentDtoPagedResultDto.fromJS(resultData200);
            return _observableOf(result200);
          }));
        } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
        }
    
        return _observableOf<DepartureDocumentDtoPagedResultDto>(<any>null);
      }
      getUnresolvedDepartureDocumentsStatistic() {
        let url_ = this.serviceUri + "/GetUnresolvedDepartureDocumentStatistic";
        return this.defaultGet(url_);
      }

      getResolvedDepartureDocumentStatistic() {
        let url = this.serviceUri + "/GetResolvedDepartureDocumentStatistic";
        return this.defaultGet(url);
      }
      /**
     * @param body (optional) 
     * @return Success
     */
    create(body: DepartureDocumentForTableDto | undefined): Observable<DepartureDocumentForTableDto> {
      let url_ = this.serviceUri + "/Create";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_: any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processCreate(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processCreate(<any>response_);
              } catch (e) {
                  return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(e);
              }
          } else
              return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(response_);
      }));
  }

  protected processCreate(response: HttpResponseBase): Observable<DepartureDocumentForTableDto> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = DepartureDocumentForTableDto.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<DepartureDocumentForTableDto>(<any>null);
  }

  /**
     * @param body (optional) 
     * @return Success
     */
    update(body: DepartureDocumentForTableDto | undefined): Observable<DepartureDocumentForTableDto> {
      let url_ = this.serviceUri + "/Update";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_: any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };

      return this._http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processUpdate(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processUpdate(<any>response_);
              } catch (e) {
                  return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(e);
              }
          } else
              return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(response_);
      }));
  }

  protected processUpdate(response: HttpResponseBase): Observable<DepartureDocumentForTableDto> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = DepartureDocumentForTableDto.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<DepartureDocumentForTableDto>(<any>null);
  }

      //hanhls
      getReleaseDepartureDocumentStatistic() {
        let url_ = this.serviceUri + "/getReleaseDepartureDocumentStatistic";
        return this.defaultGet(url_);
      }

      searchReleasedDepartureDocument(input: DepartureDocumentPagedRequestDto): Observable<DepartureDocumentDtoPagedResultDto> {
        let url_ = this.serviceUri + "/SearchReleasedDepartureDocument";
        url_ = url_.replace(/[?&]$/, "");
    
        const requestBody: string = JSON.stringify(input);
    
        let options_: any = {
          body: requestBody,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
          })
        };
    
        return this._http.request("post", url_, options_)
          .pipe(_observableMergeMap((_response: any) => {
            if (_response instanceof HttpResponseBase) {
              try {
                return this.processFilterDepartureDocuments(_response);
              } catch (e) {
                return <Observable<DepartureDocumentDtoPagedResultDto>>_observableThrow(e);
              }
            } else {
              return <Observable<DepartureDocumentDtoPagedResultDto>>_observableThrow(_response);
            }
          }));
      }

      assignDepartureDocumentUser(body: AssignDepartureDocumentUserDto | undefined): Observable<DepartureDocumentForTableDto> {
        let url_ = this.serviceUri + "/AssignDepartureDocumentUser";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(body);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processAssignDepartureDocumentUser(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processAssignDepartureDocumentUser(<any>response_);
                } catch (e) {
                    return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<DepartureDocumentForTableDto>><any>_observableThrow(response_);
        }));
       
    }
    protected processAssignDepartureDocumentUser(response: HttpResponseBase): Observable<DepartureDocumentForTableDto> {
      const status = response.status;
      const responseBlob =
          response instanceof HttpResponse ? response.body :
              (<any>response).error instanceof Blob ? (<any>response).error : undefined;

      let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
      if (status === 200) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              let result200: any = null;
              let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
              result200 = DepartureDocumentForTableDto.fromJS(resultData200);
              return _observableOf(result200);
          }));
      } else if (status !== 200 && status !== 204) {
          return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
              return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
          }));
      }
      return _observableOf<DepartureDocumentForTableDto>(<any>null);
    }

    confirmReleaseDeparture(body: ReleasedDepartureDocumentDto | undefined): Observable<string> {
      let url_ = this.serviceUri + "/ConfirmReleaseDeparture";
      url_ = url_.replace(/[?&]$/, "");

      const content_ = JSON.stringify(body);

      let options_: any = {
          body: content_,
          observe: "response",
          responseType: "blob",
          headers: new HttpHeaders({
              "Content-Type": "application/json-patch+json",
              "Accept": "text/plain"
          })
      };
    

      return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
          return this.processConfirmReleaseDeparture(response_);
      })).pipe(_observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
              try {
                  return this.processConfirmReleaseDeparture(<any>response_);
              } catch (e) {
                  return <Observable<string>><any>_observableThrow(e);
              }
          } else
              return <Observable<string>><any>_observableThrow(response_);
      }));
  }
  confirmForcedReleaseDeparture(body: ReleasedDepartureDocumentDto | undefined): Observable<string> {
    let url_ = this.serviceUri + "/ConfirmForcedReleaseDeparture";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
        body: content_,
        observe: "response",
        responseType: "blob",
        headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
        })
    };
    return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processConfirmReleaseDeparture(response_);
    })).pipe(_observableCatch((response_: any) => {
        if (response_ instanceof HttpResponseBase) {
            try {
                return this.processConfirmReleaseDeparture(<any>response_);
            } catch (e) {
                return <Observable<string>><any>_observableThrow(e);
            }
        } else
            return <Observable<string>><any>_observableThrow(response_);
    }));
  }
  protected processConfirmReleaseDeparture(response: HttpResponseBase): Observable<string> {
    const status = response.status;
    const responseBlob =
        response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let result200: any = null;
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            result200 = DepartureDocumentForTableDto.fromJS(resultData200);
            // return _observableOf(result200);
            return _observableOf(resultData200);
        }));
    } else if (status !== 200 && status !== 204) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }));
    }
    return _observableOf<string>(<any>null);
  }

  getNextReleasedNumber(departureDocumentId : number) {
    let url_ = this.serviceUri + "/GetNextReleasedNumber?departureDocumentId="+departureDocumentId;

    return this.defaultGet(url_);
  }
      //end hanhls

     /**
     * @param id (optional) 
     * @return Success
     */
    getAllEmployees() {
      let url_ = this.serviceUri + "/GetAllEmployees";
        return this.defaultGet(url_);
  }

      /**
     * @param id (optional) 
     * @return Success
     */
    getDepartureDocumentById(id: number) {
      let url_ = this.serviceUri + "/GetDepartureDocumentById?id=" + id;
        return this.defaultGet(url_);
  }

  sendRequest(body: ProcessApproveDepartureDocumentDto | undefined): Observable<string>{
    let url_ = this.serviceUri + "/SendRequest";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
        body: content_,
        observe: "response",
        responseType: "blob",
        headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
        })
    };

    return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processSendRequest(response_);
    })).pipe(_observableCatch((response_: any) => {
        if (response_ instanceof HttpResponseBase) {
            try {
                return this.processSendRequest(<any>response_);
            } catch (e) {
                return <Observable<string>><any>_observableThrow(e);
            }
        } else
            return <Observable<string>><any>_observableThrow(response_);
    }));
  }

  protected processSendRequest(response: HttpResponseBase): Observable<string> {
    const status = response.status;
    const responseBlob =
        response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            return _observableOf(resultData200);
        }));
    } else if (status !== 200 && status !== 204) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }));
    }
    return _observableOf<string>(<any>null);
  } 

  approveRequest(body: ProcessApproveDepartureDocumentDto | undefined): Observable<string>{
    let url_ = this.serviceUri + "/ApproveRequest";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
        body: content_,
        observe: "response",
        responseType: "blob",
        headers: new HttpHeaders({
            "Content-Type": "application/json-patch+json",
            "Accept": "text/plain"
        })
    };

    return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processApproveRequest(response_);
    })).pipe(_observableCatch((response_: any) => {
        if (response_ instanceof HttpResponseBase) {
            try {
                return this.processApproveRequest(<any>response_);
            } catch (e) {
                return <Observable<string>><any>_observableThrow(e);
            }
        } else
            return <Observable<string>><any>_observableThrow(response_);
    }));
  }

  protected processApproveRequest(response: HttpResponseBase): Observable<string> {
    const status = response.status;
    const responseBlob =
        response instanceof HttpResponse ? response.body :
            (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
            return _observableOf(resultData200);
        }));
    } else if (status !== 200 && status !== 204) {
        return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
            return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }));
    }
    return _observableOf<string>(<any>null);
}

rejectRequest(body: ProcessApproveDepartureDocumentDto | undefined): Observable<string>{
  let url_ = this.serviceUri + "/RejectRequest";
  url_ = url_.replace(/[?&]$/, "");

  const content_ = JSON.stringify(body);

  let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
          "Content-Type": "application/json-patch+json",
          "Accept": "text/plain"
      })
  };

  return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
    return this.processRejectRequest(response_);
  })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
          try {
              return this.processRejectRequest(<any>response_);
          } catch (e) {
              return <Observable<string>><any>_observableThrow(e);
          }
      } else
          return <Observable<string>><any>_observableThrow(response_);
  }));
}

protected processRejectRequest(response: HttpResponseBase): Observable<string> {
  const status = response.status;
  const responseBlob =
      response instanceof HttpResponse ? response.body :
          (<any>response).error instanceof Blob ? (<any>response).error : undefined;

  let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
  if (status === 200) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
          return _observableOf(resultData200);
      }));
  } else if (status !== 200 && status !== 204) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
          return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
  }
  return _observableOf<string>(<any>null);
}

}