import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from '@angular/core';
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { ResponseModel } from '@app/models/response.model';
import { PagedRequestDto } from '@shared/paged-listing-component-base';

export class PagedRequestDepartureDocumentHistoryDto extends PagedRequestDto{
    keyword: string
    DepartureDocumentId: number
}

@Injectable()
export class DepartureDocumentHistoryService extends BaseService {


  constructor(httpClient: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
    super(httpClient, injector, `${serviceUri}/api/services/app/DepartureDocumentHistory`);
  }

  searchHistoryByDepartureDocumentId(input: PagedRequestDepartureDocumentHistoryDto): Observable<ResponseModel> {
      const url_ = `${this.serviceUri}/SearchHistoryByDepartureDocumentId`
      return this.defaultPost(url_, input)
  }
}