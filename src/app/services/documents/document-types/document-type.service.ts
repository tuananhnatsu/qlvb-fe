import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, Optional, Inject } from "@angular/core";
import { BaseService, _API_BASE_URL } from '@app/services/base.service';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { DocumentTypeListDto } from '@app/models/documents/document-types/document-type.model';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';

@Injectable()
export class DocumentTypeService extends BaseService {

  private _httpClient: HttpClient;

  constructor(
    httpClient: HttpClient, injector: Injector,
    @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
    super(httpClient, injector, `${serviceUri}/api/services/app/DocumentType`);
    this._httpClient = httpClient;
  }

  findDocumentTypesByName(documentTypeName: string): Observable<DocumentTypeListDto> {
    let url_ = this.serviceUri + "/FindDocumentTypesByName";
    url_ = url_.replace(/[?&]$/, "");

    const requestBody: string = JSON.stringify(documentTypeName);

    let options_: any = {
      body: requestBody,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        "Accept": "text/plain"
      })
    };

    return this._httpClient.request("post", url_, options_)
      .pipe(_observableMergeMap((_response: any) => {
        if (_response instanceof HttpResponseBase) {
          try {
            return this.proccessListResultDto(_response);
          } catch (e) {
            return <Observable<DocumentTypeListDto>>_observableThrow(e);
          }
        } else {
          return <Observable<DocumentTypeListDto>>_observableThrow(_response);
        }
      }));
  }

  private proccessListResultDto(response: HttpResponseBase): Observable<DocumentTypeListDto> {

    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = DocumentTypeListDto.fromJS(resultData200);
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<DocumentTypeListDto>(<any>null);
  }

}