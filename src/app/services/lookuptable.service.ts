import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, InjectionToken, Optional, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { BaseService, _API_BASE_URL } from './base.service';
import { Observable, throwError as _observableThrow, of as _observableOf  } from 'rxjs';
import { LookupTableDto } from '@app/models/system-management/lookuptable.model';
import { PagedCommonValueRequestDto, CommonValueDtoPagedResultDto } from '@app/models/common/commonvalue.model';

@Injectable()
export class LookupTableService extends BaseService {
    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/LookupTable`);
    }

    /**
     * @return Success
     */
    getAllCategoryTypeForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetAllCategoryTypeForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllCategoryTypeForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllCategoryTypeForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllCategoryTypeForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }

    /**
     * @return Success
     */
    getAllSecretLevelForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetAllSecretLevelForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllSecretLevelForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllSecretLevelForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllSecretLevelForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }

    /**
     * @return Success
     */
    getAllUrgentLevelForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetAllUrgentLevelForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllUrgentLevelForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllUrgentLevelForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllUrgentLevelForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }

    /**
    * @return Success
    */
    getAllCommonValueByType(input: PagedCommonValueRequestDto): Observable<CommonValueDtoPagedResultDto> {
        let url_ = this.serviceUri + "/SearchAllCommonValueForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(input);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCommonValueByType(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCommonValueByType(<any>response_);
                } catch (e) {
                    return <Observable<CommonValueDtoPagedResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<CommonValueDtoPagedResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processCommonValueByType(response: HttpResponseBase): Observable<CommonValueDtoPagedResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = CommonValueDtoPagedResultDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<CommonValueDtoPagedResultDto>(<any>null);
    }

    /**
    * @return Success
    */
    getAllAllowRoleIdForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetAllAllowRoleIdForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllAllowRoleIdForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllAllowRoleIdForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllAllowRoleIdForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }

    /**
    * @return Success
    */
    getProcessingReasonForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetProcessingReasonForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetProcessingReasonForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetProcessingReasonForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetProcessingReasonForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }

    /**
     * @return Success
     */
    getAllRoleHandlingForLookupTable(): Observable<LookupTableDto[]> {
        let url_ = this.serviceUri + "/GetAllRoleHandlingForLookupTable";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAllRoleHandlingForLookupTable(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAllRoleHandlingForLookupTable(<any>response_);
                } catch (e) {
                    return <Observable<LookupTableDto[]>><any>_observableThrow(e);
                }
            } else
                return <Observable<LookupTableDto[]>><any>_observableThrow(response_);
        }));
    }

    protected processGetAllRoleHandlingForLookupTable(response: HttpResponseBase): Observable<LookupTableDto[]> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                if (Array.isArray(resultData200)) {
                    result200 = [] as any;
                    for (let item of resultData200)
                        result200.push(LookupTableDto.fromJS(item));
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<LookupTableDto[]>(<any>null);
    }
}
