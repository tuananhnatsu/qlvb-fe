import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Injectable, Injector, InjectionToken, Optional, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { BaseService, _API_BASE_URL } from './base.service';
import { Observable, throwError as _observableThrow, of as _observableOf  } from 'rxjs';
import { FileDto, FileDtoPagedResultDto, FileDownloadDto } from '@app/models/documents/file/file.model';

@Injectable()
export class FileService extends BaseService {
    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/File`);
    }
    
    /**
     * @param body (optional) 
     * @return Success
     */
    upload(body: File[] | undefined, folderName: string | undefined): Observable<FileDtoPagedResultDto> {
        let url_ = this.serviceUri + `/Upload?folderName=${folderName}`;
        url_ = url_.replace(/[?&]$/, "");

        let formData = new FormData();
        body.forEach(f => formData.append("File", f));

        let httpHeaders: HttpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', "multipart/form-data");

        const option: any = {
            body: formData,
            observe: "response",
            responseType: "blob",
            httpHeaders: httpHeaders
        }

        return this._http.request('post', url_, option).pipe(_observableMergeMap((response_: any) => {
            return this.processUpload(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processUpload(<any>response_);
                } catch (e) {
                    return <Observable<FileDtoPagedResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<FileDtoPagedResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processUpload(response: HttpResponseBase): Observable<FileDtoPagedResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = FileDtoPagedResultDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<FileDtoPagedResultDto>(<any>null);
    }

    /**
     * @param url (optional) 
     * @return Success
     */
    download(url: string | undefined): Observable<FileDownloadDto> {
        let url_ = this.serviceUri + "/Download?";
        if (!url)
            throw new Error("The parameter 'fileUrl' cannot be null.");
        else if (url)
            url_ += "fileUrl=" + encodeURIComponent("" + url) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "text/plain"
            })
        };

        return this._http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processDownload(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processDownload(<any>response_);
                } catch (e) {
                    return <Observable<FileDownloadDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<FileDownloadDto>><any>_observableThrow(response_);
        }));
    }

    protected processDownload(response: HttpResponseBase): Observable<FileDownloadDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = FileDownloadDto.fromJS(resultData200);
                var binaryString = atob(result200.fileBase64);
                var fileType = result200.fileType;

                var binaryLen = binaryString.length;
                var bytes = new Uint8Array(binaryLen);
                for (var idx = 0; idx < binaryLen; idx++) {
                    var ascii = binaryString.charCodeAt(idx);
                    bytes[idx] = ascii;
                }
                var file = new Blob([bytes], { type: fileType });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(file);
                } else {
                    var fileURL = URL.createObjectURL(file);
                    if (fileType.indexOf('image') !== -1 || fileType.indexOf('pdf') !== -1) {
                        window.open(fileURL);
                    } else {
                        var anchor = document.createElement("a");
                        anchor.download = result200.fileName;
                        anchor.href = fileURL;
                        anchor.click();
                    }
                }
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<FileDownloadDto>(<any>null);
    }
}