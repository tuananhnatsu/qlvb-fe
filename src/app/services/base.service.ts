import { Injector, Injectable, InjectionToken, Inject, Optional } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { ResponseModel } from 'app/models/response.model';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { ApiException } from '@shared/service-proxies/service-proxies';
export const _API_BASE_URL = new InjectionToken<string>('_API_BASE_URL');

export abstract class BaseService {
    _http: HttpClient;
    _injector: Injector;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;
    serviceUri = '';

    readonly RETRY_COUNT: number = 0;
    readonly REPLAY_COUNT: number = 1;
    readonly LIMIT_DEFAULT: number = 1000;

    constructor(
        @Inject(HttpClient) http: HttpClient
        , injector: Injector
        , serviceUri?: string
    ) {
        this._http = http;
        this._injector = injector;
        this.serviceUri = serviceUri ? serviceUri : "";
    }

    get(id: any): Observable<ResponseModel> {
        const apiUrl = `${this.serviceUri}/Get/Id=${id}`;

        return this.defaultGet(apiUrl);
    }

    getAll(fieldSort?: string, pageIndex?: any, pageSize?: any): Observable<ResponseModel>{
        let queryString = "";

        if (fieldSort) queryString += `?Sorting=${fieldSort}`;

        if (pageIndex) queryString += `&SkipCount=${pageIndex}`;

        if (pageSize) queryString += `&MaxResultCount=${pageSize}`;

        const apiUrl = `${this.serviceUri}/GetAll${queryString}`;

        return this.defaultGet(apiUrl)
    }

    post(item: any): Observable<ResponseModel> {
        const apiUrl = `${this.serviceUri}/Create`;

        return this._http.post<ResponseModel>(apiUrl, item)
    }

    put(item: any): Observable<ResponseModel> {
        const apiUrl = `${this.serviceUri}/Update`;

        return this._http.put<ResponseModel>(apiUrl, item)
    }

    delete(id: number): Observable<ResponseModel> {
        const url = `${this.serviceUri}/Delete?Id=${id}`;

        return this._http.delete<ResponseModel>(url)
    }

    defaultGet(apiUrl: string): Observable<ResponseModel> {
        return this._http.get<ResponseModel>(apiUrl)
    }
    defaultPost(apiUrl: string, item: object | any): Observable<ResponseModel> {
        return this._http
            .post<ResponseModel>(apiUrl, item)
    }
    throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
        if (result !== null && result !== undefined)
            return _observableThrow(result);
        else
            return _observableThrow(new ApiException(message, status, response, headers, null));
    }
    
    blobToText(blob: any): Observable<string> {
        return new Observable<string>((observer: any) => {
            if (!blob) {
                observer.next("");
                observer.complete();
            } else {
                let reader = new FileReader();
                reader.onload = event => {
                    observer.next((<any>event.target).result);
                    observer.complete();
                };
                reader.readAsText(blob);
            }
        });
    }
}
