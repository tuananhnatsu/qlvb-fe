import { Injectable, Injector, InjectionToken, Optional, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponseBase, HttpResponse } from '@angular/common/http';
import { BaseService, _API_BASE_URL } from '../base.service';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { ResponseModel } from '@app/models/response.model';
import { UserPagedRequestDto } from '@app/views/system-management/users/users.component';
import { UserDto, UserDtoPagedResultDto } from '@shared/service-proxies/service-proxies';

@Injectable()
export class UserSysService extends BaseService {

    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/User`);
    }
    
    searchUsers(input: UserPagedRequestDto): Observable<ResponseModel> {
        return this._http.post<ResponseModel>(`${this.serviceUri}/SearchUsers`, input)
    }

    updateManyUsers(input: UserDto[]) {
        return this._http.put<ResponseModel>(`${this.serviceUri}/UpdateMany`, input)
    }
    getAllLeadershipApprovedPermission(): Observable<UserDtoPagedResultDto> {
        let url_ = this.serviceUri + "/TakeAllUsersHasLeadershipApprovedPermission";        
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            body: {},
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json-patch+json",
                "Accept": "text/plain"
            })
        };

        return this._http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processLeadershipApprovedPermission(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processLeadershipApprovedPermission(<any>response_);
                } catch (e) {
                    return <Observable<UserDtoPagedResultDto>><any>_observableThrow(e);
                }
            } else
                return <Observable<UserDtoPagedResultDto>><any>_observableThrow(response_);
        }));
    }

    protected processLeadershipApprovedPermission(response: HttpResponseBase): Observable<UserDtoPagedResultDto> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = UserDtoPagedResultDto.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return this.blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return this.throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<UserDtoPagedResultDto>(<any>null);
    }
}

