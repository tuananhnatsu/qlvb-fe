import { Injectable, Injector, InjectionToken, Optional, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService, _API_BASE_URL } from '../base.service';
import { Observable } from 'rxjs';
import { ResponseModel } from '@app/models/response.model';

@Injectable()
export class DepartmentService extends BaseService {

    constructor(http: HttpClient, injector: Injector, @Optional() @Inject(_API_BASE_URL) serviceUri: string) {
        super(http, injector, `${serviceUri}/api/services/app/Department`);
    }
    
    getTreeDepartments(keyword?: string, dependentDepartmentId?: number): Observable<ResponseModel>{
        let queryString = "";
        if (keyword){
            queryString += `?keyword=${keyword}`
        }
        if (dependentDepartmentId){
            queryString += `&dependentDepartmentId=${dependentDepartmentId}`
        }
        const apiUrl = `${this.serviceUri}/GetTreeDepartments${queryString}`
        return this.defaultGet(apiUrl)
    }

    getTreeUserDepartment(keyword?: string, dependentDepartmentId?: number): Observable<ResponseModel>{
        let queryString = "";
        if (keyword){
            queryString += `?keyword=${keyword}`
        }
        if (dependentDepartmentId){
            queryString += `&dependentDepartmentId=${dependentDepartmentId}`
        }
        const apiUrl = `${this.serviceUri}/GetTreeUserDepartment${queryString}`
        return this.defaultGet(apiUrl)
    }

    deleteDepartments(id: number): Observable<ResponseModel>{
        return this._http.delete<ResponseModel>(`${this.serviceUri}/DeleteDepartments?id=${id}`)
    }

    getAllUserByDepartmentId(id: number): Observable<ResponseModel> {
        const _url = `${this.serviceUri}/GetAllUserByDepartmentId?departmentId=${id}`
        return this.defaultGet(_url);
    }
}

