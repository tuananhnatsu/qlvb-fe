import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Containers
// import { DefaultLayoutComponent } from './containers';
import { LoginComponent } from './views/login/login.component';
import { AppRouteGuard } from '@core/auth/auth-route-guard';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'system-management',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AppRouteGuard],
    data: {
      title: 'Trang chủ'
    },
    children: [
      {
        path: 'system-management',
        loadChildren: () => import('./views/system-management/system-management.module').then(m => m.SystemManagementModule)
      },
      {
        path: 'document',
        loadChildren: () => import('./views/documents/document.module').then(m => m.DocumentModule)
      }
    ]
  },
  { path: '**', component: DefaultLayoutComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
