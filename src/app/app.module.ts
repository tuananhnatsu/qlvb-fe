import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Injector, LOCALE_ID } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, PlatformLocation, registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { StartupService } from './core';
import { CoreModule } from './core/core.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { AbpModule } from '@abp/abp.module';
import { SharedModule } from '@shared/shared.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
// import { DefaultLayoutComponent } from './containers';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];
const APP_THEME = [
  LoginComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '../../node_modules/@coreui/angular'

// Import routing module
import { AppRoutingModule } from './app.routing';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SystemManagementModule } from './views/system-management/system-management.module';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginComponent } from './views/login/login.component';
import { AbpHttpInterceptor } from 'abp-ng2-module/dist/src/abpHttpInterceptor';
import { AppConsts } from '@shared/AppConsts';
import { AppSessionService } from '@shared/session/app-session.service';
import { AppPreBootstrap } from './AppPreBootstrap';
import * as _ from 'lodash';
import { API_BASE_URL } from '@shared/service-proxies/service-proxies';
// import { _API_BASE_URL } from './services/system-management/department.service';
import { ServiceModule } from './services/services.module';
import { _API_BASE_URL } from './services/base.service';
import { GestureConfig } from '@angular/material/core';
import { DocumentModule } from './views/documents/document.module';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
export function appInitializerFactory(injector: Injector,
  platformLocation: PlatformLocation,
  startupService: StartupService) {
  return () => {
    startupService.load();

      abp.ui.setBusy();
      return new Promise<boolean>((resolve, reject) => {
          AppConsts.appBaseHref = getBaseHref(platformLocation);
          const appBaseUrl = getDocumentOrigin() + AppConsts.appBaseHref;

          AppPreBootstrap.run(appBaseUrl, () => {
              abp.event.trigger('abp.dynamicScriptsInitialized');
              const appSessionService: AppSessionService = injector.get(AppSessionService);
              appSessionService.init().then(
                  (result) => {
                      abp.ui.clearBusy();

                      if (shouldLoadLocale()) {
                          const angularLocale = convertAbpLocaleToAngularLocale(abp.localization.currentLanguage.name);
                          import(`@angular/common/locales/${angularLocale}.js`)
                              .then(module => {
                                  registerLocaleData(module.default);
                                  resolve(result);
                              }, reject);
                      } else {
                          resolve(result);
                      }
                  },
                  (err) => {
                      abp.ui.clearBusy();
                      reject(err);
                  }
              );
          });
      });
  };
}

export function convertAbpLocaleToAngularLocale(locale: string): string {
  if (!AppConsts.localeMappings) {
      return locale;
  }

  const localeMapings = _.filter(AppConsts.localeMappings, { from: locale });
  if (localeMapings && localeMapings.length) {
      return localeMapings[0]['to'];
  }

  return locale;
}

export function shouldLoadLocale(): boolean {
  return abp.localization.currentLanguage.name && abp.localization.currentLanguage.name !== 'en-US';
}

export function getRemoteServiceBaseUrl(): string {
  return AppConsts.remoteServiceBaseUrl;
}

export function getCurrentLanguage(): string {
  if (abp.localization.currentLanguage.name) {
      return abp.localization.currentLanguage.name;
  }
  

  // todo: Waiting for https://github.com/angular/angular/issues/31465 to be fixed.
  return 'vi';
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    CoreModule,
    SystemManagementModule,
    DocumentModule,
    ServiceProxyModule,
    ServiceModule,
    SharedModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_THEME
  ],
  providers: [
    { provide: _API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
    {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
    },
    { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
    { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [Injector, PlatformLocation, StartupService],
      multi: true
  },
  {
      provide: LOCALE_ID,
      useFactory: getCurrentLanguage
  },
  { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

export function getBaseHref(platformLocation: PlatformLocation): string {
  const baseUrl = platformLocation.getBaseHrefFromDOM();
  if (baseUrl) {
      return baseUrl;
  }

  return '/';
}

function getDocumentOrigin() {
  if (!document.location.origin) {
      const port = document.location.port ? ':' + document.location.port : '';
      return document.location.protocol + '//' + document.location.hostname + port;
  }

  return document.location.origin;
}