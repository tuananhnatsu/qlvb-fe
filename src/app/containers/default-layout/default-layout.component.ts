import { Injector, OnInit, Component } from '@angular/core';
import { MenuService } from '../../core';
import { AppAuthService } from '@core/auth/app-auth.service';
import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import { AppSessionService } from '@shared/session/app-session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent extends AppComponentBase implements OnInit {
  minimized = false;
  isOpenRow = false;
  fullName: string = "";
  public allMenuItems = [...this.menuService.getAll()];
  public navItems = [];
  permission: PermissionCheckerService;
  constructor(injector: Injector,
    private menuService: MenuService,
    private authService: AppAuthService,
    private sessionService: AppSessionService
  ) {
    super(injector)
    this.permission = injector.get(PermissionCheckerService);
  }
  ngOnInit(): void {
    this.navItems = this.setInitialPermissionsStatus(this.allMenuItems);
    if (this.sessionService.user) {
      this.fullName = `${this.sessionService.user.name} ${this.sessionService.user.surname}`
    }
  }
  setInitialPermissionsStatus(menus: any[]): any[] {
    var newMenus = [];
    _.map(menus, item => {
      var granted = this.isGranted(item);
      if (granted) {
        var newItem = {
          name: item.name,
          permissionName: item.permissionName,
          url: item.url,
          icon: item.icon,
          children: null
        }
        if (item.children && item.children.length > 0) {
          var newChilds = this.setInitialPermissionsStatus(item.children);
          if (newChilds && newChilds.length > 0) newItem.children = newChilds;
        }
        newMenus.push(newItem);
      }
    });
    return newMenus;
  }
  isGranted(item: any): boolean {
    var granted = this.permission.isGranted(item.permissionName);
    if (!granted && item.children && item.children.length > 0) {
      _.map(item.children, child => {
        granted = this.isGranted(child);
        if (granted) return true;
      });
    }
    return granted;
  }
  toggleMinimize(e) {
    this.minimized = e;
  }
  _toggleMinimize() {
    this.minimized = !this.minimized;
  }
  logout(): void {
    this.authService.logout(true);
  }
}
