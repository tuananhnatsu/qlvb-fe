export class FileDto implements IFileDto {
    id: number;
    fileName: string | undefined;
    fileUrl: string | undefined;
    relatedObjectId: number | undefined;
    relatedObjectType: number | undefined;    
    constructor(data?: IFileDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.fileName = data["fileName"];
            this.fileUrl = data["fileUrl"];
            this.relatedObjectId = data["relatedObjectId"];
            this.relatedObjectType = data["relatedObjectType"];
        }
    }

    static fromJS(data: any): FileDto {
        data = typeof data === 'object' ? data : {};
        let result = new FileDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["fileName"] = this.fileName;
        data["fileUrl"] = this.fileUrl;
        data["relatedObjectId"]= this.relatedObjectId;
        data["relatedObjectType"]= this.relatedObjectType;
        return data;
    }

    clone(): FileDto {
        const json = this.toJSON();
        let result = new FileDto();
        result.init(json);
        return result;
    }
}
export interface IFileDto {
    id: number;
    fileName: string | undefined;
    fileUrl: string | undefined;
    relatedObjectId: number | undefined;
    relatedObjectType: number | undefined;    
}
export class FileDtoPagedResultDto implements IFileDtoPagedResultDto {
    totalCount: number;
    items: FileDto[] | undefined;

    constructor(data?: IFileDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(FileDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): FileDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new FileDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): FileDtoPagedResultDto {
        const json = this.toJSON();
        let result = new FileDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface IFileDtoPagedResultDto {
    totalCount: number;
    items: FileDto[] | undefined;
}
export class FileDownloadDto implements IFileDownloadDto {
    fileBase64: string;
    fileType: string | undefined;
    fileName: string | undefined;

    constructor(data?: IFileDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.fileBase64 = data["fileBase64"];
            this.fileType = data["fileType"];
            this.fileName = data["fileName"];
        }
    }

    static fromJS(data: any): FileDownloadDto {
        data = typeof data === 'object' ? data : {};
        let result = new FileDownloadDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["fileBase64"] = this.fileBase64;
        data["fileType"] = this.fileType;
        data["fileName"] = this.fileName;
        return data;
    }

    clone(): FileDownloadDto {
        const json = this.toJSON();
        let result = new FileDownloadDto();
        result.init(json);
        return result;
    }
}

export interface IFileDownloadDto {
    fileBase64: string;
    fileType: string | undefined;
}