import { PagedRequestDto } from "@shared/paged-listing-component-base";
import { FileDto } from "../../file/file.model";

export enum DepartureDocumentStatusEnum{
  Texting=0,
  Sent,
  Waiting,
  Confirmed,
  Processed,
  Cancel,
  WaitToRelease,
  Release
}

export class DepartureDocumentForTableDto
  implements IDepartureDocumentForTableDto {
  id: number;
  shortDescription: string;
  documentFieldId: number;
  departmentId: number;
  departmentName: string;
  documentTypeId: number;    
  departureFolderId: number;
  year: number;
  createdByName: string;
  contentRequest: string;
  numberOfPages: number;
  storedPlace: string;
  approvedById: number;
  urgentLevel: number;
  secretLevel: number;
  description: string;
  creationTime: moment.Moment | undefined;
  sentDate: moment.Moment | undefined;
  appliedDate: moment.Moment | undefined;
  expiredDate: moment.Moment | undefined;
  requestDate: moment.Moment | undefined;
  urgentLevelName: string;
  secretLevelName: string;
  status: number;
  createdById: number;
  files: FileDto[] | undefined = [];
  attachedFiles: FileDto[] | undefined = [];
  documentTypeName: string | undefined;
  documentFieldName: string | undefined;
  approvedByName: string | undefined;

  constructor(data?: IDepartureDocumentForTableDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.id = data["id"];
      this.documentFieldId = data["documentFieldId"];
      this.documentTypeId = data["documentTypeId"];
      this.departmentId = data["departmentId"];
      this.departureFolderId = data["departureFolderId"];
      this.year = data["year"];
      this.numberOfPages = data["numberOfPages"];
      this.storedPlace = data["storedPlace"];
      this.approvedById = data["approvedById"];
      this.urgentLevel = data["urgentLevel"];
      this.secretLevel = data["secretLevel"];
      this.description = data["description"];
      this.shortDescription = data["shortDescription"];
      this.creationTime = data["creationTime"];
      this.sentDate = data["sentDate"];
      this.appliedDate = data["appliedDate"];
      this.urgentLevelName = data["urgentLevelName"];
      this.secretLevelName = data["secretLevelName"];
      this.attachedFiles = data["attachedFiles"];
      this.createdById = data["createdById"];
      this.files = data["files"];
      this.documentFieldName = data["documentFieldName"];
      this.documentTypeName = data["documentTypeName"];
      this.requestDate = data["requestDate"];
      this.departmentName = data["departmentName"];
      this.contentRequest = data["contentRequest"];
      this.expiredDate = data["expiredDate"];
      this.status = data["status"];
      this.approvedByName = data["approvedByName"];
      if (Array.isArray(data["attachedFiles"])) {
        this.files = [] as any;
        for (let item of data["attachedFiles"]) this.files.push(FileDto.fromJS(item));
      }      
    }
  }

  static fromJS(data: any): DepartureDocumentForTableDto {
    data = typeof data === "object" ? data : {};
    let result = new DepartureDocumentForTableDto();
    result.init(data);

    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["documentFieldId"] = this.documentFieldId;
    data["documentTypeId"] = this.documentTypeId;
    data["departmentId"] = this.departmentId;
    data["departureFolderId"] = this.departureFolderId;
    data["year"] = this.year;
    data["numberOfPages"] = this.numberOfPages;
    data["storedPlace"] = this.storedPlace;
    data["approvedById"] = this.approvedById;
    data["urgentLevel"] = this.urgentLevel;
    data["secretLevel"] = this.secretLevel;
    data["description"] = this.description;
    data["creationTime"] = this.creationTime;
    data["sentDate"] = this.sentDate;
    data["appliedDate"] = this.appliedDate;
    data["shortDescription"] = this.shortDescription;
    data["urgentLevelName"] = this.urgentLevelName;
    data["secretLevelName"] = this.secretLevelName;
    data["files"] = this.files;
    data["attachedFiles"] = this.attachedFiles;
    data["createdById"] = this.createdById;
    data["documentTypeName"] = this.documentTypeName;
    data["documentFieldName"] = this.documentFieldName;
    data["requestDate"] = this.requestDate;
    data["departmentName"] = this.departmentName;
    data["contentRequest"] = this.contentRequest;
    data["expiredDate"] = this.expiredDate;
    data["status"] = this.status;
    data["approvedByName"] = this.approvedByName;
    if (Array.isArray(data["attachedFiles"])) {
      this.files = [] as any;
      for (let item of data["attachedFiles"]) this.files.push(FileDto.fromJS(item));
    }
    return data;
  }

  clone(): DepartureDocumentForTableDto {
    const json = this.toJSON();
    let result = new DepartureDocumentForTableDto();
    result.init(json);

    return result;
  }
}

export interface IDepartureDocumentForTableDto {
  id: number;
  shortDescription: string;
  documentFieldId: number;
  documentTypeId: number;
  departmentId: number;
  departureFolderId: number;
  year: number;
  createdByName: string;
  numberOfPages: number;
  storedPlace: string;
  approvedById: number;
  urgentLevel: number;
  secretLevel: number;
  description: string;
  creationTime: moment.Moment | undefined;
  sentDate: moment.Moment | undefined;
  appliedDate: moment.Moment | undefined;
  requestDate: moment.Moment | undefined;
  expiredDate: moment.Moment | undefined;
  urgentLevelName: string;
  secretLevelName: string;
  status: number;
  createdById: number;
  documentTypeName: string | undefined;
  documentFieldName: string | undefined;
  departmentName: string;
  contentRequest: string;
  approvedByName: string;
}
export class DepartureDocumentDtoPagedResultDto implements IDepartureDocumentDtoPagedResultDto {
  
    totalCount: number;
    items: DepartureDocumentForTableDto[] | undefined;
  
    constructor(data?: IDepartureDocumentDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  
    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"]) {
                  this.items.push(DepartureDocumentForTableDto.fromJS(item));
                }
            }
        }
    }
  
    static fromJS(data: any): DepartureDocumentDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new DepartureDocumentDtoPagedResultDto();
        result.init(data);
  
        return result;
    }
  
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : { };
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }
  
    clone(): DepartureDocumentDtoPagedResultDto {
        const json = this.toJSON();
        let result = new DepartureDocumentDtoPagedResultDto();
        result.init(json);
        
        return result;
    }
  }
  export interface IDepartureDocumentDtoPagedResultDto {
  
    totalCount: number;
    items: DepartureDocumentForTableDto[] | undefined;
  
  }
  export class DepartureDocumentPagedRequestDto extends PagedRequestDto{
    keyword: string = ""
    documentTypeId: number
    documentFieldId: number
    status: number
    year: number[] = []
    fromDate: moment.Moment | undefined;
    toDate: moment.Moment | undefined;
    isProcessed: boolean = false
  }
  
  export class AssignDepartureDocumentUserDto {
    departureDocumentUserDto: DepartureDocumentUserDto;
    content?: string|"";
}

  export class DepartureDocumentUserDto implements IDepartureDocumentUserDto {
    id:number;
    userId: number;
    nonExpired?: boolean | false;
    expiredDate? : moment.Moment;
    statusId?: number;
    departureDocumentId?: number;
    userGroupId?:number;
    userFullName:string;

    constructor(data?: IDepartureDocumentUserDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    static fromJS(data: any): DepartureDocumentUserDto {
        data = typeof data === 'object' ? data : {};
        let result = new DepartureDocumentUserDto();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.userId = data["userId"];
            this.nonExpired = data["nonExpired"];
            this.expiredDate = data["expiredDate"];
            this.statusId = data["statusId"];
            this.departureDocumentId = data["departureDocumentId"];
            this.userGroupId = data["userGroupId"];
            this.userFullName = data["userFullName"];
        }
    }
  }

  export interface IDepartureDocumentUserDto {
    userId: number;
    nonExpired?: boolean | false;
    expiredDate? : moment.Moment;
    statusId?: number;
    departureDocumentId?: number;
    userGroupId?:number;
    userFullName:string;
  }

  export class ReleasedDepartureDocumentDto implements IReleasedDepartureDocumentDto{
    releasedSymbol?: string|"";
    releasedNumber?: string|"";
    releasedDate : moment.Moment;
    releasedReceiveOrg?: string|"";
    releasedSignedUser?: string|"";
    releasedNumOrg?:string|"";
    departureDocumentId:number;
    symbolValue: number;
    constructor(data?: IReleasedDepartureDocumentDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
    }
    static fromJS(data: any): DepartureDocumentUserDto {
      data = typeof data === 'object' ? data : {};
      let result = new DepartureDocumentUserDto();
      result.init(data);
      return result;
  }

  init(data?: any) {
      if (data) {
          this.releasedSymbol = data["releasedSymbol"];
          this.symbolValue = data["symbolValue"];
          this.releasedNumber = data["releasedNumber"];
          this.releasedDate = data["releasedDate"];
          this.releasedReceiveOrg = data["releasedReceiveOrg"];
          this.releasedSignedUser = data["releasedSignedUser"];
          this.releasedNumOrg = data["releasedNumOrg"];
          this.departureDocumentId = data["departureDocumentId"];

      }
  }
  }
  export interface IReleasedDepartureDocumentDto {
    releasedSymbol?: string|"";
    releasedNumber?: string|"";
    releasedDate? : moment.Moment|undefined;
    releasedReceiveOrg?: string|"";
    releasedSignedUser?: string|"";
    releasedNumOrg?:string|"";
    departureDocumentId?:number;
    symbolValue?: number;
  }

  export class ProcessApproveDepartureDocumentDto {
    approvedById: number;
    departmentApproved: number;
    isExpried: boolean;
    deadlineDate: moment.Moment;
    content: string;
    status: number;
    departureDocumentId: number;
    contentRejected: string;
    contentApproved: string;
    nextPersonProcess: number;

    constructor(data?: IProcessApproveDepartureDocumentDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
    }
    static fromJS(data: any): ProcessApproveDepartureDocumentDto {
      data = typeof data === 'object' ? data : {};
      let result = new ProcessApproveDepartureDocumentDto();
      result.init(data);
      return result;
  }

  init(data?: any) {
      if (data) {
          this.departureDocumentId = data["departureDocumentId"];
          this.approvedById = data["approvedById"];
          this.departmentApproved = data["departmentApproved"];
          this.isExpried = data["isExpried"];
          this.deadlineDate = data["deadlineDate"];
          this.content = data["content"];
          this.contentApproved = data["contentApproved"];
          this.contentRejected = data["contentRejected"];
          this.nextPersonProcess = data["nextPersonProcess"];
      }
  }
  }

  export interface IProcessApproveDepartureDocumentDto {
    departureDocumentId: number;
    departmentApproved: number;
    approvedById: number;
    isExpried: boolean;
    deadlineDate: moment.Moment;
    content: string;
    contentRejected: string;
    contentApproved: string;
    nextPersonProcess: number;
  }