import * as moment from 'moment';
import { ArrivedFolderDto } from './folder/arrivedfolder.model';
import { IFileDto, FileDto } from '../file/file.model';
import { PagedRequestDto } from '../../../shared/paged-listing-component-base';

export class ArrivedDocumentDto implements IArrivedDocumentDto {
    id: number;
    shortDescription: string | undefined;
    documentFieldId: number | undefined;
    documentTypeId: number | undefined;
    publishedDate: moment.Moment | undefined;
    arrivedDate: moment.Moment | undefined;
    year: number | undefined;
    arrivedFolderId: number | undefined;
    arrivedNumber: number | undefined;
    documentNumber: string | undefined;
    signedByName: string | undefined;
    numberOfPublish: number | undefined;
    numberOfPages: number | undefined;
    storedPlace: string | undefined;
    publishedOrganization: string | undefined;
    approvedById: number | undefined;
    urgentLevel: number | undefined;
    secretLevel: number | undefined;
    description: string | undefined;
    relatedToArrivedDocumentId: number | undefined;
    relatedArrivedDocuments: ArrivedDocumentDto[] | undefined;
    files: FileDto[]  | undefined = [];
    attachedFiles: FileDto[]
    status: number | undefined = 0;
    deadLineDate: moment.Moment | undefined;
    sentDate: moment.Moment | undefined;
    processingStatus: number | undefined;
    allowRoleId: number | undefined;
    documentTypeName: string | undefined;
    documentFieldName: string | undefined;
    secretLevelName: string | undefined;
    urgentLevelName: string | undefined;
    approvedByName: string | undefined;
    totalHandlded: number | undefined;
    totalProcessing: number | undefined;
    totalUnHandle: number | undefined;
    filesCordinate: FileDto[]  | undefined = [];
    handlingDocuments: HandlingDocumentsDto[] | undefined;
    cordinateDocuments: CordinateDocumentsDto[] | undefined;

    constructor(data?: IArrivedDocumentDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.shortDescription = data["shortDescription"];
            this.documentFieldId = data["documentFieldId"];
            this.documentTypeId = data["documentTypeId"];
            this.documentNumber = data["documentNumber"];
            this.publishedDate = data["publishedDate"] ? new Date(data["publishedDate"]): <any>undefined;
            this.arrivedDate = data["arrivedDate"] ? new Date(data["arrivedDate"]) : <any>undefined;
            this.year = data["year"];
            this.arrivedFolderId = data["arrivedFolderId"];
            this.arrivedNumber = data["arrivedNumber"];
            this.signedByName = data["signedByName"];
            this.numberOfPublish = data["numberOfPublish"];
            this.numberOfPages = data["numberOfPages"];
            this.storedPlace = data["storedPlace"];
            this.publishedOrganization = data["publishedOrganization"];
            this.approvedById = data["approvedById"];
            this.urgentLevel = data["urgentLevel"];
            this.secretLevel = data["secretLevel"];
            this.description = data["description"];
            this.relatedToArrivedDocumentId = data["relatedToArrivedDocumentId"];
            this.attachedFiles = data["attachedFiles"];
            this.status = data["status"];
            this.processingStatus = data["processingStatus"];
            this.deadLineDate = data["deadLineDate"];
            this.allowRoleId = data["allowRoleId"];
            this.documentFieldName = data["documentFieldName"];
            this.documentTypeName = data["documentTypeName"];
            this.secretLevelName = data["secretLevelName"];
            this.urgentLevelName = data["urgentLevelName"];
            this.approvedByName = data["approvedByName"];
            this.totalHandlded = data["totalHandlded"];
            this.totalProcessing = data["totalProcessing"];
            this.totalUnHandle = data["totalUnHandle"];
            this.sentDate = data["sentDate"];
            this.cordinateDocuments = data["cordinateDocuments"];
            this.handlingDocuments = data["handlingDocuments"];
            if (Array.isArray(data["relatedArrivedDocuments"])) {
                this.relatedArrivedDocuments = [] as any;
                for (let item of data["relatedArrivedDocuments"])
                    this.relatedArrivedDocuments.push(ArrivedDocumentDto.fromJS(item));
            }
            if (Array.isArray(data["files"])) {
                this.files = [] as any;
                for (let item of data["files"])
                    this.files.push(FileDto.fromJS(item));
            }
            if (Array.isArray(data["filesCordinate"])) {
                this.filesCordinate = [] as any;
                for (let item of data["filesCordinate"])
                    this.filesCordinate.push(FileDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ArrivedDocumentDto {
        data = typeof data === 'object' ? data : {};
        let result = new ArrivedDocumentDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["shortDescription"] = this.shortDescription;
        data["documentFieldId"] = this.documentFieldId;
        data["documentTypeId"] = this.documentTypeId;
        data["documentNumber"] = this.documentNumber;
        data["publishedDate"] = this.publishedDate ? moment(this.publishedDate).add(1, "day").toISOString() : <any>undefined;
        data["arrivedDate"] = this.arrivedDate ? moment(this.arrivedDate).add(1, "day").toISOString() : <any>undefined;
        data["year"] = this.year;
        data["arrivedFolderId"] = this.arrivedFolderId;
        data["arrivedNumber"] = this.arrivedNumber;
        data["signedByName"] = this.signedByName;
        data["numberOfPublish"] = this.numberOfPublish;
        data["numberOfPages"] = this.numberOfPages;
        data["storedPlace"] = this.storedPlace;
        data["publishedOrganization"] = this.publishedOrganization;
        data["approvedById"] = this.approvedById;
        data["urgentLevel"] = this.urgentLevel;
        data["secretLevel"] = this.secretLevel;
        data["description"] = this.description;
        data["relatedToArrivedDocumentId"] = this.relatedToArrivedDocumentId;
        data["attachedFiles"] = this.attachedFiles;
        data["status"] = this.status;
        data["processingStatus"] = this.processingStatus;
        data["deadLineDate"] = this.deadLineDate;
        data["allowRoleId"] = this.allowRoleId;
        data["documentFieldName"] = this.documentFieldName;
        data["documentTypeName"] = this.documentTypeName;
        data["secretLevelName"] = this.secretLevelName;
        data["urgentLevelName"] = this.urgentLevelName;
        data["approvedByName"] = this.approvedByName;
        data["totalUnHandle"] = this.totalUnHandle;
        data["totalProcessing"] = this.totalProcessing;
        data["totalHandlded"] = this.totalHandlded;
        data["cordinateDocuments"] = this.cordinateDocuments;
        data["handlingDocuments"] = this.handlingDocuments;
        data["sentDate"] = this.sentDate;
        
        if (Array.isArray(this.relatedArrivedDocuments)) {
            data["relatedArrivedDocuments"] = [];
            for (let item of this.relatedArrivedDocuments)
                data["relatedArrivedDocuments"].push(item.toJSON());
        }
        if (Array.isArray(this.files)) {
            data["files"] = [];
            for (let item of this.files)
                data["files"].push(item.toJSON());
        }
        if (Array.isArray(data["filesCordinate"])) {
            this.filesCordinate = [] as any;
            for (let item of data["filesCordinate"])
                this.filesCordinate.push(FileDto.fromJS(item));
        }
        return data;
    }

    clone(): ArrivedDocumentDto {
        const json = this.toJSON();
        let result = new ArrivedDocumentDto();
        result.init(json);
        return result;
    }
}

export interface IArrivedDocumentDto {
    id: number;
    shortDescription: string | undefined;
    documentFieldId: number | undefined;
    documentTypeId: number | undefined;
    publishedDate: moment.Moment | undefined;
    arrivedDate: moment.Moment | undefined;
    year: number | undefined;
    arrivedFolderId: number | undefined;
    arrivedNumber: number | undefined;
    documentNumber: string | undefined;
    signedByName: string | undefined;
    numberOfPublish: number | undefined;
    numberOfPages: number | undefined;
    storedPlace: string | undefined;
    publishedOrganization: string | undefined;
    approvedById: number | undefined;
    urgentLevel: number | undefined;
    secretLevel: number | undefined;
    description: string | undefined;
    relatedToArrivedDocumentId: number | undefined;
    files: FileDto[]  | undefined ;
    attachedFiles: FileDto[]
    status: number | undefined;
    deadLineDate: moment.Moment | undefined;
    sentDate: moment.Moment | undefined;
    processingStatus: number | undefined;
    allowRoleId: number | undefined;
    documentTypeName: string | undefined;
    documentFieldName: string | undefined;
    secretLevelName: string | undefined;
    urgentLevelName: string | undefined;
    approvedByName: string | undefined;
    totalHandlded: number | undefined;
    totalProcessing: number | undefined;
    totalUnHandle: number | undefined;
    relatedArrivedDocuments: ArrivedDocumentDto[] | undefined;
    handlingDocuments: HandlingDocumentsDto[] | undefined;
    cordinateDocuments: CordinateDocumentsDto[] | undefined;
}



export class PagedProcessDocumentRequestDto extends PagedRequestDto {
    shortDescription: string;
    documentNumber: string;
    roleHandling: number;
    urgentLevel: number;
    documentType: number;
    documentField: number;
    endDate: string;
    startDate: string;
    expiredDateFrom: string;
    expiredDateTo: string;
    years: any[];
    status: number;
    isOutOfDate: number | null | undefined;
}
  
  export interface IProcessDocumentDtoPagedResultDto {
  
    totalCount: number;
    items: ArrivedDocumentDto[] | undefined;
    totalHandlded: number;
    totalProcessing: number;
    totalUnHandle: number;
  }
export class ProcessDocumentDtoPagedResultDto implements IProcessDocumentDtoPagedResultDto {
  
    totalCount: number;
    items: ArrivedDocumentDto[] | undefined;
    totalHandlded: number;
    totalProcessing: number;
    totalUnHandle: number;

    constructor(data?: IProcessDocumentDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
  
    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            this.totalHandlded = data["totalHandlded"];
            this.totalProcessing = data["totalProcessing"];
            this.totalUnHandle = data["totalUnHandle"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"]) {
                  this.items.push(ArrivedDocumentDto.fromJS(item));
                }
            }
        }
    }
  
    static fromJS(data: any): ProcessDocumentDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new ProcessDocumentDtoPagedResultDto();
        result.init(data);
  
        return result;
    }
  
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : { };
        data["totalCount"] = this.totalCount;
        data["totalUnHandle"] = this.totalUnHandle;
        data["totalProcessing"] = this.totalProcessing;
        data["totalHandlded"] = this.totalHandlded;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }
  
    clone(): ProcessDocumentDtoPagedResultDto {
        const json = this.toJSON();
        let result = new ProcessDocumentDtoPagedResultDto();
        result.init(json);
        
        return result;
    }
  }
  export class ProcessDocumentView extends ArrivedDocumentDto {
    secretLevelName: string;
    urgentLevelName: string;
    allowRoleName: string;
  
    constructor(arrivedDocumentDto: ArrivedDocumentDto) {
      super(arrivedDocumentDto);
    }
  }

export class ProcessingDocumentGroupDto{
    id:number|undefined;
    name: string;
    users: ProcessingDocumentGroupDetailDto[]|undefined;

    
    static fromJS(data: any): ProcessingDocumentGroupDto {
        data = typeof data === 'object' ? data : {};
        let result = new ProcessingDocumentGroupDto();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
            if (Array.isArray(data["users"])) {
                this.users = [] as any;
                for (let item of data["users"])
                    this.users.push(ProcessingDocumentGroupDetailDto.fromJS(item));
            }
        }
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["id"] = this.id;
        if (Array.isArray(this.users)) {
            data["users"] = [];
            for (let item of this.users)
                data["users"].push(item.toJSON());
        }
        return data;
    }
}
export class ProcessingDocumentGroupDetailDto implements IProcessingDocumentGroupDetailDto{
    id: number;
    userId:number;
    username:string;
    departmentName:string;
    roleName:string;
    constructor(data?: IProcessingDocumentGroupDetailDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    static fromJS(data: any): ProcessingDocumentGroupDetailDto {
        data = typeof data === 'object' ? data : {};
        let result = new ProcessingDocumentGroupDetailDto();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.userId = data["userId"];
            this.username = data["username"];
            this.departmentName = data["departmentName"];
            this.roleName = data["roleName"];
        }
    }
    toJSON(data?: any){
        data = typeof data === 'object' ? data : {};
        data["userId"] = this.userId;
        data["id"] = this.id;
        data["departmentName"] =this.departmentName ;
        data["roleName"] =this.roleName ;
        return data;
    }

}
export interface IProcessingDocumentGroupDetailDto{
    id: number|undefined;
    userId:number|undefined;
    username:string|undefined;
    departmentName:string|undefined;
    roleName:string|undefined;
}


export class ArrivedDocumentUserDto implements IIArrivedDocumentUserDto {
    id:number;
    userId: number;
    allowRoleId?: number;
    processingReasonId?: number;
    nonExpired?: boolean | false;
    expiredDate? : moment.Moment;
    statusId?: number;
    arrivedDocumentId?: number;
    userGroupId?:number;
    userFullName:string;

    constructor(data?: IIArrivedDocumentUserDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    static fromJS(data: any): ArrivedDocumentUserDto {
        data = typeof data === 'object' ? data : {};
        let result = new ArrivedDocumentUserDto();
        result.init(data);
        return result;
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.userId = data["userId"];
            this.allowRoleId = data["allowRoleId"];
            this.processingReasonId = data["processingReasonId"];
            this.nonExpired = data["nonExpired"];
            this.expiredDate = data["expiredDate"];
            this.statusId = data["statusId"];
            this.arrivedDocumentId = data["arrivedDocumentId"];
            this.userGroupId = data["userGroupId"];
            this.userFullName = data["userFullName"];
        }
    }
  }

  export interface IIArrivedDocumentUserDto {
    userId: number;
    allowRoleId?: number;
    processingReasonId?: number;
    nonExpired?: boolean | false;
    expiredDate? : moment.Moment;
    statusId?: number;
    arrivedDocumentId?: number;
    userGroupId?:number;
    userFullName:string;
  }
export class ConfirmAssignDocumentRequestDto{
    content?: string|"";
    status: number| false;
    users? : ArrivedDocumentUserDto[]|undefined;
}

export class CordinateDocumentsDto {
    sendedById: number;
    content: string;
    sendDate: moment.Moment;
    attachedFiles: FileDto[]
    files: FileDto[]  | undefined = [];
    arrivedDocumentId: number;
    constructor(data?: ICordinateDocumentsDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.content = data["content"];
            this.arrivedDocumentId = data["arrivedDocumentId"];
            this.sendDate = data["sendDate"];
            this.sendedById = data["sendedById"];
            if (Array.isArray(data["files"])) {
                this.files = [] as any;
                for (let item of data["files"])
                    this.files.push(FileDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): CordinateDocumentsDto {
        data = typeof data === 'object' ? data : {};
        let result = new CordinateDocumentsDto();
        result.init(data);
        return result;
    }
    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};

        data["content"] = this.content;
        data["arrivedDocumentId"] = this.arrivedDocumentId;
        data["sendDate"] = this.sendDate;
        data["sendedById"] = this.sendedById;
        data["files"] = this.files;

        if (Array.isArray(data["files"])) {
            this.files = [] as any;
            for (let item of data["files"])
                this.files.push(FileDto.fromJS(item));
        }
        return data;
    }
    clone(): CordinateDocumentsDto {
        const json = this.toJSON();
        let result = new CordinateDocumentsDto();
        result.init(json);
        return result;
    }
}
  
export interface ICordinateDocumentsDto {
  sendedById: number;
  content: string;
  sendDate: moment.Moment;
  attachedFiles: FileDto[]
  files: FileDto[]  | undefined;
  arrivedDocumentId: number;
}

export class HandlingDocumentsDto {
  handlingBy: number;
  content: string;
  handlingDate: moment.Moment;
  arrivedDocumentId: number;
}
  
export interface IHandlingDocumentsDto {
  handlingBy: number;
  content: string;
  handlingDate: moment.Moment;
  arrivedDocumentId: number;
}