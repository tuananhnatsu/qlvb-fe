import { PagedRequestDto } from '@shared/paged-listing-component-base';
export class ArrivedFolderDto implements IArrivedFolderDto {    
    id: number;
    name: string | undefined;
    code: string | undefined;
    currentNumber: string | undefined;
    year: number | undefined;
    priority: number | undefined;
    status: number | undefined;
    statusName:string | undefined;
    constructor(data?: IArrivedFolderDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];            
            this.code = data["code"];
            this.currentNumber = data["currentNumber"];
            this.year = data["year"];            
            this.priority = data["priority"];
            this.status = data["status"];
        }
    }

    static fromJS(data: any): ArrivedFolderDto {
        data = typeof data === 'object' ? data : {};
        let result = new ArrivedFolderDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;        
        data["code"] = this.code;
        data["currentNumber"] = this.currentNumber;
        data["year"] = this.year;        
        data["priority"] = this.priority;
        data["status"] = this.status;
        return data;
    }

    clone(): ArrivedFolderDto {
        const json = this.toJSON();
        let result = new ArrivedFolderDto();
        result.init(json);
        return result;
    }
}

export interface IArrivedFolderDto {
    id: number;
    name: string | undefined;
    code: string | undefined;
    currentNumber: string | undefined;
    year: number | undefined;
    priority: number | undefined;
    status: number | undefined;
}
export class PagedArrivedFolderRequestDto extends PagedRequestDto {
    keyword: string;
    year: number;  
    status: number | null | undefined;  
}

export class ArrivedFolderDtoPagedResultDto implements IArrivedFolderDtoPagedResultDto {
    totalCount: number;
    items: ArrivedFolderDto[] | undefined;

    constructor(data?: IArrivedFolderDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(ArrivedFolderDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ArrivedFolderDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new ArrivedFolderDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): ArrivedFolderDtoPagedResultDto {
        const json = this.toJSON();
        let result = new ArrivedFolderDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface IArrivedFolderDtoPagedResultDto {
    totalCount: number;
    items: ArrivedFolderDto[] | undefined;
}

export interface IArrivedFolderListResultDto {
    items: ArrivedFolderDto[];
}

export class ArrivedFolderListResultDto implements IArrivedFolderListResultDto {
    
    items: ArrivedFolderDto[];

    constructor(data?: IArrivedFolderListResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(ArrivedFolderDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ArrivedFolderListResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new ArrivedFolderListResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): ArrivedFolderListResultDto {
        const json = this.toJSON();
        let result = new ArrivedFolderListResultDto();
        result.init(json);
        return result;
    }
}