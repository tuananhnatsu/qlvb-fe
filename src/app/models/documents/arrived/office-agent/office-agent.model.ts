import { PagedRequestDto } from '@shared/paged-listing-component-base';

export class OfficeAgentDto implements IOfficeAgentDto {
  
  id: number;  
  shortDescription: string;
  documentFieldId: number;
  documentTypeId: number;
  publishedDate: Date | undefined;
  arrivedDate: Date | undefined;
  year: number;
  arrivedFolderId: number | undefined;
  arrivedFolderName: string;
  arrivedNumber: number;
  documentNumber: string;
  signedByName: string;
  numberOfPublish: number;
  numberOfPages: number;
  storedPlace: string;
  publishedOrganizationId: number;
  approvedById: number;
  urgentLevel: number;
  secretLevel: number;
  description: string;
  relatedToArrivedDocumentId: number;
  creationTime: moment.Moment | undefined;
  sentDate: moment.Moment | undefined;
  publishedOrganization: string | undefined;
  urgentLevelName: string;
  secretLevelName: string;
  approvedName: string | undefined;


  constructor(data?: IOfficeAgentDto) {
    if (data) {
        for (var property in data) {
            if (data.hasOwnProperty(property))
                (<any>this)[property] = (<any>data)[property];
        }
    }
  }

  init(data?: any) {
    if (data) {
        this.id = data["id"];
        this.documentFieldId = data["documentFieldId"];
        this.documentTypeId = data["documentTypeId"];
        this.publishedDate = data["publishedDate"];
        this.arrivedDate = data["arrivedDate"];
        this.year = data["year"];
        this.arrivedFolderId = data["arrivedFolderId"];
        this.arrivedFolderName = data["arrivedFolderName"];
        this.arrivedNumber = data["arrivedNumber"];
        this.documentNumber = data["documentNumber"];
        this.signedByName = data["signedByName"];
        this.numberOfPublish = data["numberOfPublish"];
        this.numberOfPages = data["numberOfPages"];
        this.storedPlace = data["storedPlace"];
        this.publishedOrganizationId = data["publishedOrganizationId"];
        this.approvedById = data["approvedById"];
        this.urgentLevel = data["urgentLevel"];
        this.secretLevel = data["secretLevel"];
        this.description = data["description"];
        this.shortDescription = data["shortDescription"];
        this.relatedToArrivedDocumentId = data["relatedToArrivedDocumentId"];
        this.publishedOrganization = data["publishedOrganization"];
        this.creationTime = data["creationTime"];
        this.sentDate = data["sentDate"];
        this.approvedName = data["approvedName"];
        this.urgentLevelName = data["urgentLevelName"];
        this.secretLevelName = data["secretLevelName"];
    }
  }

  static fromJS(data: any): OfficeAgentDto {
    data = typeof data === 'object' ? data : { };
    let result = new OfficeAgentDto();
    result.init(data);

    return result;
  }

  toJSON(data?: any) {
    data["id"] = this.id;
    data["documentFieldId"] = this.documentFieldId;
    data["documentTypeId"] = this.documentTypeId;
    data["publishedDate"] = this.publishedDate;
    data["arrivedDate"] = this.arrivedDate;
    data["year"] = this.year;
    data["arrivedFolderId"] = this.arrivedFolderId;
    data["arrivedFolderName"] = this.arrivedFolderName;
    data["arrivedNumber"] = this.arrivedNumber;
    data["documentNumber"] = this.documentNumber;
    data["signedByName"] = this.signedByName;
    data["numberOfPublish"] = this.numberOfPublish;
    data["numberOfPages"] = this.numberOfPages;
    data["storedPlace"] = this.storedPlace;
    data["publishedOrganizationId"] = this.publishedOrganizationId;
    data["approvedById"] = this.approvedById;
    data["urgentLevel"] = this.urgentLevel;
    data["secretLevel"] = this.secretLevel;
    data["description"] = this.description;
    data["shortDescription"] = this.shortDescription;
    data["relatedToArrivedDocumentId"] = this.relatedToArrivedDocumentId;
    data["urgentLevelName"] = this.urgentLevelName;
    data["secretLevelName"] = this.secretLevelName;
    data["sentDate"] = this.sentDate;
    data["approvedName"] = this.approvedName;
  }

  clone(): OfficeAgentDto {
    const json = this.toJSON();
    let result = new OfficeAgentDto();
    result.init(json);

    return result;
  }
}

export interface IOfficeAgentDto {

  id: number;
  shortDescription: string | undefined;
  documentFieldId: number;
  documentTypeId: number;
  publishedDate: Date | undefined;
  arrivedDate: Date | undefined;
  year: number;
  arrivedFolderId: number | undefined;
  arrivedFolderName: string | undefined;
  arrivedNumber: number | undefined;
  documentNumber: string | undefined;
  signedByName: string | undefined;
  numberOfPublish: number | undefined;
  numberOfPages: number | undefined;
  storedPlace: string | undefined;
  publishedOrganizationId: number | undefined;
  publishedOrganization: string | undefined;
  approvedById: number | undefined;
  urgentLevel: number | undefined;
  secretLevel: number | undefined;
  description: string | undefined;
  relatedToArrivedDocumentId: number | undefined;
  creationTime: moment.Moment | undefined;
  urgentLevelName: string;
  secretLevelName: string;

}

export class PagedOfficeAgentRequestDto extends PagedRequestDto {
  
  keyword: string;
  year: number;    

}

export interface IOfficeAgentDtoPagedResultDto {

  totalCount: number;
  items: OfficeAgentDto[] | undefined;

}

export class OfficeAgentDtoPagedResultDto implements IOfficeAgentDtoPagedResultDto {
  
  totalCount: number;
  items: OfficeAgentDto[] | undefined;

  constructor(data?: IOfficeAgentDtoPagedResultDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.totalCount = data["totalCount"];
          if (Array.isArray(data["items"])) {
              this.items = [] as any;
              for (let item of data["items"]) {
                this.items.push(OfficeAgentDto.fromJS(item));
              }
          }
      }
  }

  static fromJS(data: any): OfficeAgentDtoPagedResultDto {
      data = typeof data === 'object' ? data : {};
      let result = new OfficeAgentDtoPagedResultDto();
      result.init(data);

      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : { };
      data["totalCount"] = this.totalCount;
      if (Array.isArray(this.items)) {
          data["items"] = [];
          for (let item of this.items)
              data["items"].push(item.toJSON());
      }
      return data;
  }

  clone(): OfficeAgentDtoPagedResultDto {
      const json = this.toJSON();
      let result = new OfficeAgentDtoPagedResultDto();
      result.init(json);
      
      return result;
  }
}

export class OfficeAgentView extends OfficeAgentDto {
  
  urgentLevelName: string;
  secretLevelName: string;

  constructor(officeAgentDto: OfficeAgentDto) {
    super(officeAgentDto);
  }
}