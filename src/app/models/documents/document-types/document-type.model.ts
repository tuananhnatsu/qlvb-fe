export interface IDocumentTypeDto {

  id: number;
  documentTypeName: string;
  documentTypeCode: string;

}

export class DocumentTypeDto implements IDocumentTypeDto {

  id: number;
  documentTypeName: string;
  documentTypeCode: string;

  constructor(data?: IDocumentTypeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.id = data["id"];
      this.documentTypeName = data["documentTypeName"];
      this.documentTypeCode = data["documentTypeCode"];
    }
  }

  static fromJS(data: any): DocumentTypeDto {
    data = typeof data === 'object' ? data : {};
    let result = new DocumentTypeDto();
    result.init(data);

    return result;
  }

  toJSON(data?: any) {
    data["id"] = this.id;
    data["documentTypeName"] = this.documentTypeName;
    data["documentTypeCode"] = this.documentTypeCode;
  }

  clone(): DocumentTypeDto {
    const json = this.toJSON();
    let result = new DocumentTypeDto();
    result.init(json);

    return result;
  }

}

export interface IDocumentTypeListDto {

  items: DocumentTypeDto[];

}

export class DocumentTypeListDto implements IDocumentTypeListDto {

  items: DocumentTypeDto[];

  constructor(data?: IDocumentTypeListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (Array.isArray(data["items"])) {
        this.items = [] as any;
        for (let item of data["items"]) {
          this.items.push(DocumentTypeDto.fromJS(item));
        }
      }
    }
  }

  static fromJS(data: any): DocumentTypeListDto {
    data = typeof data === 'object' ? data : {};
    let result = new DocumentTypeListDto();
    result.init(data);

    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (Array.isArray(this.items)) {
      data["items"] = [];
      for (let item of this.items) {
        data["items"].push(item.toJSON());
      }
    }

    return data;
  }

  clone(): DocumentTypeListDto {
    const json = this.toJSON();
    let result = new DocumentTypeListDto();
    result.init(json);

    return result;
  }

}