export class LookupTableDto implements ILookupTableDto {
    id: number | undefined;
    displayName: string | undefined;

    constructor(data?: ILookupTableDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.displayName = data["displayName"];
        }
    }

    static fromJS(data: any): LookupTableDto {
        data = typeof data === 'object' ? data : {};
        let result = new LookupTableDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["displayName"] = this.displayName;
        return data;
    }
}

export interface ILookupTableDto {
    id: number | undefined;
    displayName: string | undefined;
}