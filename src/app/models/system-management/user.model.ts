
export class UserDto implements IUserDto {    
    name: string | undefined;
    userName: string | undefined;
    surname: string | undefined;
    fullName: string | undefined;    
    positionName: string | undefined;    
    departmentName: string | undefined;    
    id: number;
    constructor(data?: IUserDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.name = data["name"];
            this.userName = data["userName"];
            this.surname = data["surname"];
            this.fullName = data["fullName"];
            this.positionName = data["positionName"];
            this.departmentName = data["departmentName"];
            this.id = data["id"];
        }
    }

    static fromJS(data: any): UserDto {
        data = typeof data === 'object' ? data : {};
        let result = new UserDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["userName"] = this.userName;
        data["surname"] = this.surname;
        data["fullName"] = this.fullName;
        data["departmentName"] = this.departmentName;
        data["positionName"] = this.positionName;
        data["id"] = this.id;
        return data;
    }

    clone(): UserDto {
        const json = this.toJSON();
        let result = new UserDto();
        result.init(json);
        return result;
    }
}
export interface IUserDto {
    name: string | undefined;
    userName: string | undefined;
    surname: string | undefined;
    fullName: string | undefined;    
    positionName: string | undefined;    
    departmentName: string | undefined;    
    id: number;    
}
export class UserDtoPagedResultDto implements IUserDtoPagedResultDto {
    totalCount: number;
    items: UserDto[] | undefined;

    constructor(data?: IUserDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(UserDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): UserDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new UserDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): UserDtoPagedResultDto {
        const json = this.toJSON();
        let result = new UserDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface IUserDtoPagedResultDto {
    totalCount: number;
    items: UserDto[] | undefined;
}