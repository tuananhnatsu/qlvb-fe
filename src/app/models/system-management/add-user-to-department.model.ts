export class AddUserToDepartmentModel {
    user: string
    role: string
    userId: number | string
}