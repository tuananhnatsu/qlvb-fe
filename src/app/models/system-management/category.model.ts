import { PagedRequestDto } from '@shared/paged-listing-component-base';

export class CategoryDto implements ICategoryDto {    
    name: string | undefined;
    displayName: string | undefined;
    code: string | undefined;
    description: string | undefined;
    grantedPermissions: string[] | undefined;
    id: number;
    type: number | undefined;
    typeName: string | undefined;
    creationTime: moment.Moment | undefined;
    constructor(data?: ICategoryDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.name = data["name"];
            this.displayName = data["displayName"];
            this.code = data["code"];
            this.description = data["description"];
            this.id = data["id"];
            this.type = data["type"];
            this.creationTime = data["creationTime"];
        }
    }

    static fromJS(data: any): CategoryDto {
        data = typeof data === 'object' ? data : {};
        let result = new CategoryDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["code"] = this.code;
        data["description"] = this.description;
        data["id"] = this.id;
        data["type"] = this.type;
        data["creationTime"] = this.creationTime;
        return data;
    }

    clone(): CategoryDto {
        const json = this.toJSON();
        let result = new CategoryDto();
        result.init(json);
        return result;
    }
}

export interface ICategoryDto {
    name: string | undefined;
    displayName: string | undefined;
    code: string | undefined;
    description: string | undefined;    
    id: number;
    type: number | undefined;
    typeName: string | undefined;
}

export class CategoryDtoPagedResultDto implements ICategoryDtoPagedResultDto {
    totalCount: number;
    items: CategoryDto[] | undefined;

    constructor(data?: ICategoryDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(CategoryDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): CategoryDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new CategoryDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): CategoryDtoPagedResultDto {
        const json = this.toJSON();
        let result = new CategoryDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface ICategoryDtoPagedResultDto {
    totalCount: number;
    items: CategoryDto[] | undefined;
}

export class PagedCategoriesRequestDto extends PagedRequestDto {
    keyword: string;
    type: number[];
}