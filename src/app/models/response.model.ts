export class ResponseModel {
    result: any;
    targetUrl: string | null;
    success: boolean;
    error: any | null;
    unAuthorizedRequest: boolean;
    __abp: boolean;
}

export interface ListDto {
    totalCount: number;
    items: Array<object> | Array<any>
}