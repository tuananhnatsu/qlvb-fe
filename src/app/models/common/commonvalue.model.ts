import { PagedRequestDto } from '@shared/paged-listing-component-base';

export class CommonValueDto implements ICommonValueDto {    
    id: number;    
    value: string | undefined;
    type: number | undefined;
    
    constructor(data?: ICommonValueDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.type = data["type"];            
            this.value = data["value"];
        }
    }

    static fromJS(data: any): CommonValueDto {
        data = typeof data === 'object' ? data : {};
        let result = new CommonValueDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["value"] = this.value;        
        data["type"] = this.type;
        return data;
    }

    clone(): CommonValueDto {
        const json = this.toJSON();
        let result = new CommonValueDto();
        result.init(json);
        return result;
    }
}

export interface ICommonValueDto {
    id: number;
    value: string | undefined;
    type: number | undefined;
}

export class PagedCommonValueRequestDto extends PagedRequestDto {
    keyword: string;
    type: number;    
}
export class CommonValueDtoPagedResultDto implements ICommonValueDtoPagedResultDto {
    totalCount: number;
    items: CommonValueDto[] | undefined;

    constructor(data?: ICommonValueDtoPagedResultDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.totalCount = data["totalCount"];
            if (Array.isArray(data["items"])) {
                this.items = [] as any;
                for (let item of data["items"])
                    this.items.push(CommonValueDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): CommonValueDtoPagedResultDto {
        data = typeof data === 'object' ? data : {};
        let result = new CommonValueDtoPagedResultDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["totalCount"] = this.totalCount;
        if (Array.isArray(this.items)) {
            data["items"] = [];
            for (let item of this.items)
                data["items"].push(item.toJSON());
        }
        return data;
    }

    clone(): CommonValueDtoPagedResultDto {
        const json = this.toJSON();
        let result = new CommonValueDtoPagedResultDto();
        result.init(json);
        return result;
    }
}

export interface ICommonValueDtoPagedResultDto {
    totalCount: number;
    items: CommonValueDto[] | undefined;
}